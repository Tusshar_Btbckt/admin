import { Injectable } from "@angular/core";
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { utf8Encode } from "@angular/compiler/src/util";
import { AuthenticationService } from "../pages/Authentication/services/authentication.service";
/**
 * Prefixes all requests not starting with `http[s]` with `environment.serverUrl`.
 */
@Injectable({
  providedIn: "root",
})
export class ApiHeaderInterceptor implements HttpInterceptor {
  constructor(private authenticationService: AuthenticationService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!request.url.includes("assets/")) {
      var localData = localStorage.getItem("_cred_dt");
      if (localData) {
        let currentUser = JSON.parse(
          this.authenticationService.DecryptData(
            localStorage.getItem("_cred_dt")
          )
        );
        let token = currentUser.access_token;
        if (token) {
          request = request.clone({
            setHeaders: {
              Authorization: `Bearer ${token}`,
              responseType: "JSON",
              ContentSecurityPolicy: "default-src 'self'",
              CacheControl:
                "no-cache, no-store, must-revalidate, post- check=0, pre-check=0",
              Pragma: "no-cache",
              Expires: "0",
            },
            url: request.url,
          });
        } else {
          request = request.clone({ url: request.url });
        }
      }
    }
    return next.handle(request);
  }
}
