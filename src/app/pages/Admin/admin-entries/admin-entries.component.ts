import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { AdminEntries,AdminEntries_Models,AdminEntries_Resp_Models,AdminEntriesRes_test_Models,
  AdminEntries_Common_Models} from '../../../pages/_models/admin/admin-entries.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { AdminEntriesService } from '../../../pages/_services/admin/admin-entries.service';
import { NbDialogService } from '@nebular/theme';
import { ImageDialogComponent } from '../_dialog/image-dialog/image-dialog.component';

import { LazyLoadEvent } from 'primeng/api';

import Swal from 'sweetalert2/dist/sweetalert2.js';

import { AuthenticationService } from '../../Authentication/services/authentication.service';

// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-admin-entries',
  templateUrl: './admin-entries.component.html',
  styleUrls: ['./admin-entries.component.scss']
})
export class AdminEntriesComponent implements OnInit {
  adminentriesForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  public XMasterList: any[];
  public Xlink_List: any[];
  products = [];
  IsWait:boolean;
  imageSrc: string = "./assets/images/image_placeholder.jpg";
  IsImg1_btn_Show = true;
  IsImg2_btn_Show = true;
  imageSrc2:string= "./assets/images/image_placeholder.jpg";

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;
  
  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;
  
  constructor(
    private AdmmEnts_Service: AdminEntriesService,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private authenticationService:AuthenticationService 
  ) { }

  config: NbToastrConfig;
  currentUserData:any = {}

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.get_XMaster_List();
    
    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
    
  }

  getFormControl(): void {
    
    this.adminentriesForm = this.formBuilder.group({
      XMaster: ['', Validators.required],
      XLink: [''],
      XCode: ['', Validators.required],
      XName: ['', Validators.required],
      Ordering: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Description: ['', Validators.required],
      Image1_Source:  [''],
      Image1_Name:  [''],
      Image2_Source:  [''],
      Image2_Name:  [''],
      InActive:[false]
    });

    
  }

  get_XMaster_List(): void {

    const objModel = { StrQuery:"Sp_AdminEntriesMst 'Get_XMaster_List','','','','','',1,1", };

    this.AdmmEnts_Service.getDropdownList(objModel).subscribe((resp: any) => {
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        this.XMasterList = resp.row;
      }
      else
      {
        this.XMasterList = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  Change_XMaster(param): void {
    //console.log(ox.value);
    this.Xlink_List = [];
    this.adminentriesForm.patchValue({ XLink: null });
    //let selected_Xmaster = ox.value;
    //const objModel1 = { StrQuery:"Sp_AdminEntriesMst 'Get_XLink_List','"+selected_Xmaster+"','','','','',1,1", };

    const objModel = { StrQuery:`Sp_AdminEntriesMst 'Get_XLink_List','${param}','','','','',${this.currentUserData.UserId},${this.currentUserData.Company}`, };

    this.AdmmEnts_Service.getDropdownList(objModel).subscribe((resp: any) => {
      //console.log(resp);
      debugger;
      let _result = resp.success;
      if(_result== "1")
      {
        this.Xlink_List = resp.row;
      }
      else
      {
        this.Xlink_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  async Change_XMaster_forEdit(param) {
    this.Xlink_List = [];
    this.adminentriesForm.patchValue({ XLink: null });
    //let selected_Xmaster = ox.value;
    const objModel = { StrQuery:`Sp_AdminEntriesMst 'Get_XLink_List','${param}','','','','',${this.currentUserData.UserId},${this.currentUserData.Company}`, };

    //const objModel1 = { StrQuery:"Sp_Common_Mst 'Get_XLink_List','"+selected_Xmaster+"','','','','',1,1", };
    await this.AdmmEnts_Service.getDropdownList(objModel).subscribe((resp: any) => {
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        this.Xlink_List = resp.row;
      }
      else
      {
        this.Xlink_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

   // convenience getter for easy access to form fields
   get f() { return this.adminentriesForm.controls; }

   
   onSubmit(Name){
    
    debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.adminentriesForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }
    debugger;

    

    const objModel: AdminEntries = {
      XMaster:this.f.XMaster.value,
      XCode:this.f.XCode.value,
      XName:this.f.XName.value,
      XLink:this.f.XLink.value,
      Ordering:this.f.Ordering.value,
      Description:this.f.Description.value,
      Base64String_Image1:this.f.Image1_Source.value,
      Image1_Name:this.f.Image1_Name.value,
      Base64String_Image2:this.f.Image2_Source.value,
      Image2_Name:this.f.Image2_Name.value,
      IpAddress:"::1",
      IsActive:this.f.InActive.value == false ? true:false,
      IsDelete:this.f.InActive.value == true ? true:false,
      Company:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
    }

    
    this.AdmmEnts_Service.Save_AdminEntries(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      if(_result== "2")
      {
        this.IsWait = false;
        this.adminentriesForm.reset();
        this.adminentriesForm.setErrors(null);
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.onFileClear("1");
        this.onFileClear("2");
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.adminentriesForm.reset();
        this.adminentriesForm.setErrors(null);
        this.IsNew_Entry = true;
        this.onFileClear("1");
        this.onFileClear("2");
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  }; 
  
  Btn_Reset(){
    this.IsUpdate = false;
    this.onFileClear("1");
    this.onFileClear("2");
  };

  onFileChange(event) {
    const reader = new FileReader();
    debugger;
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.adminentriesForm.patchValue({
        Image1_Name: event.target.files[0].name
      });
      reader.readAsDataURL(file);
    
      reader.onload = () => {

        this.IsImg1_btn_Show = false;
        
        var get_Base64String = reader.result as string;
        var strImage = get_Base64String.replace(/^data:image\/[a-z]+;base64,/, "");
        this.imageSrc = reader.result as string;
     
        this.adminentriesForm.patchValue({
          Image1_Source: strImage
        });
   
      };
   
    }
  }

  onFileChange2(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.adminentriesForm.patchValue({
        Image2_Name: event.target.files[0].name
      });
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.IsImg2_btn_Show = false;
        var get_Base64String = reader.result as string;
        var strImage = get_Base64String.replace(/^data:image\/[a-z]+;base64,/, "");
        this.imageSrc2 = reader.result as string;
     
        this.adminentriesForm.patchValue({
          Image2_Source: strImage
        });
   
      };
   
    }
  }

  onFileClear(parm):void{

    if(parm == "1"){
      this.imageSrc ="./assets/images/image_placeholder.jpg";
      this.IsImg1_btn_Show = true;
      // this.adminentriesForm.patchValue({
      //   Image1_Source: ""
      // });

      this.adminentriesForm.controls['Image1_Source'].setValue("");
      this.adminentriesForm.controls['Image1_Name'].setValue("");

    }else{
      this.imageSrc2 ="./assets/images/image_placeholder.jpg";
      this.IsImg2_btn_Show = true;
      // this.adminentriesForm.patchValue({
      //   Image2_Source: ""
      // });

      this.adminentriesForm.controls['Image2_Source'].setValue("");
      this.adminentriesForm.controls['Image2_Name'].setValue("");
    }

  }

  Show_Success_msg(msg):void{
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg):void{
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg):void{
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };
  
  openDialog(parm) {
    if(parm == "1"){

      this.dialogService.open(ImageDialogComponent, {
        context: {
          title: 'Image Preview',
          imageSrc : this.imageSrc
        },
      });

    }else{
      this.dialogService.open(ImageDialogComponent, {
        context: {
          title: 'Image Preview',
          imageSrc :this.imageSrc2
        },
      });
    }
    
  }

  selectTab(ev) {
    debugger;
      if(ev.tabId == "tb_lstVw"){
   
       this.tb_IsActive = false;
       
        if(this.IsNew_Entry){
         // fetch new entry
         this.tble_loading = true;
         this.get_ListView_List("0","10","");
  
         setTimeout(() => {
           if (this.datasource) {
               this.Prod_ListView = this.datasource.slice(0, (0 + 10));
               this.tble_loading = false;
               this.IsNew_Entry= false;
           }
         }, 1000);
        }
      }
   
     }


  get_ListView_List(first,rows,globalFilter): void {
    debugger;

    globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
    
     const objModel: AdminEntries_Common_Models = {
      Type:"Get_ListView",
      Value:first,
      Value1:globalFilter != "" ? (first + rows):rows,
      Value2:globalFilter,
      Value3:"",
      Value4:"",
      UserId:"1",
      Company:"1"
    }

    this.AdmmEnts_Service.get_ListView(objModel).subscribe((resp: any) => {
      
      let _result = resp.ResponseCode;
      if(_result== "2")
      {
        this.datasource = resp.List;
        //For List-View
        this.totalRecords =  resp.List[0].Row_Count;

        this.Prod_ListView = globalFilter != "" ? this.datasource :this.datasource.slice(first, (first + rows));
        this.tble_loading = false;
        
        
      }
      else if(_result== "-4")
      {
        this.datasource = [];
        this.totalRecords =  0;
        this.tble_loading = false;
      }
      else
      {
        this.datasource = [];
        this.totalRecords =  0;
        this.tble_loading = false;
      }

    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  loadCustomers(event: LazyLoadEvent) {
    debugger;
    this.tble_loading = true;

    if(event.globalFilter!= null){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(this.datasource.length == 0){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(event.first > 500){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    
    if(this.datasource.length > 0)
    {
      this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      this.tble_loading = false;
    }
    


  }

  
  async editProduct(parm){
    const str = `Sp_AdminEntriesMst 'Get_AdminEntries_Detail_ById',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.AdmmEnts_Service.getData_JSON(objModel).subscribe((resp: any) => {
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        let objResp = resp.row[0];
        this.IsUpdate = true;
        this.Change_XMaster_forEdit(objResp.XMaster);

        setTimeout(() => {

          debugger;
          if(objResp.Image1 == "Default.jpg"){
            this.IsImg1_btn_Show = true;
          }
          else{
            this.IsImg1_btn_Show = false;
            this.imageSrc = `http://api.icorrect.in/Upload/AdminEntries/1/${objResp.Image1}`;
          }
  
          if(objResp.Image2 == "Default.jpg"){
            this.IsImg2_btn_Show = true;
          }
          else{
            this.IsImg2_btn_Show = false;
            this.imageSrc2 = `http://api.icorrect.in/Upload/AdminEntries/1/${objResp.Image2}`;
          }

          // this.submitted = true;

          this.adminentriesForm.controls['XMaster'].setValue(objResp.XMaster);
          this.adminentriesForm.controls['XLink'].setValue(objResp.XLink);
          this.adminentriesForm.controls['XCode'].setValue(objResp.XCode);
          this.adminentriesForm.controls['XName'].setValue(objResp.XName);
          this.adminentriesForm.controls['Ordering'].setValue(1);
          this.adminentriesForm.controls['Description'].setValue(objResp.Description);
          //this.adminentriesForm.controls['Image1_Source'].setValue("");
          this.adminentriesForm.controls['Image1_Name'].setValue(objResp.Image1);
          //this.adminentriesForm.controls['Image2_Source'].setValue("");
          this.adminentriesForm.controls['Image2_Name'].setValue(objResp.Image2);
          this.adminentriesForm.controls['InActive'].setValue(objResp.IsInActive);
  
          
          
        }, 1000);

        
        this.tb_IsActive = true;
        
      }
      else
      {
        this.XMasterList = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    

    
  };

  

  deleteProduct_Confirm(parm) {

    Swal.fire({
      title: 'Confirm',
      text:  'Are you sure you want to delete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {

        this.deleteProduct(parm);

        // Swal.fire(
        //   'Deleted!',
        //   'Your imaginary file has been deleted.',
        //   'success'
        // )
      // For more information about handling dismissals please visit
      // https://sweetalert2.github.io/#handling-dismissals
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        // Swal.fire(
        //   'Cancelled',
        //   'Your imaginary file is safe :)',
        //   'error'
        // )
      }
    })


  }

  deleteProduct(parm) {
    debugger
    const objModel: any = {
      Id:parm,
      IpAddress:"::1",
      Company:1,
      CreatedBy:1,
      Command:"Delete"
    }

    this.AdmmEnts_Service.Save_AdminEntries(objModel).subscribe((resp: any) => {
      let _result = resp.ResponseCode;
      if(_result== "4")
      {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.Id !== parm);
        this.datasource = this.datasource.filter(val => val.Id !== parm);
        this.showToast('success','Successful','Entry Deleted.');
      }
      else if(_result== "6")
      {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.Id !== parm);
        this.datasource = this.datasource.filter(val => val.Id !== parm);
        this.showToast('success','Successful','Entry Deleted.');
      }
      else
      {
        this.showToast('error','Error','Entry Not Deleted.');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    
  }


  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';
    
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }
    


  

}
