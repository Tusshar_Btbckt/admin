import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminEntriesComponent } from './admin-entries/admin-entries.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { AdminComponent } from './admin.component';
import { CountryComponent } from './country/country.component';
import { StateComponent } from './state/state.component';
import { CityComponent } from './city/city.component';




const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'admin-entries',
        component: AdminEntriesComponent,
      },
      {
        path: 'admin-user',
        component: AdminUserComponent,
      },
      {
        path: 'country',
        component: CountryComponent,
      },
      {
        path: 'state',
        component: StateComponent,
      },
      {
        path: 'city',
        component: CityComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
