import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { AdminUser } from '../../../pages/_models/admin/admin-user.model';
import { AppResponse } from '../../../pages/_models/app-response.model';
import { AdminUserService } from '../../../pages/_services/admin/admin-user.service';

import { LazyLoadEvent } from 'primeng/api';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.scss']
})

export class AdminUserComponent implements OnInit {
  AdminUserForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;
  returnUrl: string;
  public HotelIdList: any[];
  public UserTypeIdList: any[];
  products = [];
  IsWait: boolean;

  IsSucess = false;
  IsWarning = false;
  IsDanger = false;
  IsSucessMsg: string;
  IsWarningMsg: string;
  IsDangerMsg: string;

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private AdminUser_Service: AdminUserService,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    private authenticationService:AuthenticationService 
  ) { }

  config: NbToastrConfig;
  currentUserData:any = {}

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.get_HotelId_List();
    this.get_UserTypeId_List();

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  getFormControl(): void {
    this.AdminUserForm = this.formBuilder.group({
      UserId: [''],
      HotelId: ['', Validators.required],
      UserFName: ['', Validators.required],
      UserLName: ['', Validators.required],
      UserName: ['', Validators.required],
      Password: ['', Validators.required],
      Mobile: ['', Validators.required],
      Email: ['', Validators.required],
      UserTypeId: ['', Validators.required],
      InActive: [false]
    });
  }

  get_HotelId_List(): void {

    const objModel = { StrQuery: "Sp_Get_AdminUser 'Get_Hotel_List','','','','','',1,1", };

    this.AdminUser_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.HotelIdList = resp.row;
      }
      else {
        this.HotelIdList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_UserTypeId_List(): void {

    const objModel = { StrQuery: "Sp_Get_AdminUser 'Get_Role_List','','','','','',1,1", };

    this.AdminUser_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.UserTypeIdList = resp.row;
      }
      else {
        this.UserTypeIdList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  // convenience getter for easy access to form fields
  get f() { return this.AdminUserForm.controls; }

  onSubmit(Name) {
debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.AdminUserForm.invalid) {
      this.IsWait = false;
      return false;
    }

    const objModel: AdminUser = {
      UserId: Name == "Update" ? this.f.UserId.value : 0,
      HotelId: this.f.HotelId.value,
      UserFName: this.f.UserFName.value,
      UserLName: this.f.UserLName.value,
      UserName: this.f.UserName.value,
      Password: this.f.Password.value,
      Mobile: this.f.Mobile.value,
      Email: this.f.Email.value,
      UserTypeId: this.f.UserTypeId.value,
      IpAddress: "::1",
      IsActive: this.f.InActive.value == false ? true : false,
      IsDelete: this.f.InActive.value == true ? true : false,
      Company:this.currentUserData.Company,
      CreatedBy: this.currentUserData.UserId,
      Command: Name,

    }
    debugger;
    this.AdminUser_Service.Save_AdminUser(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.ResponseCode;
      if (_result == "2") {
        this.IsWait = false;
        this.AdminUserForm.reset();
        this.AdminUserForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
      }
      else if (_result == "4") {
        this.IsWait = false;
        this.AdminUserForm.reset();
        this.AdminUserForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else if (_result == "-4") {
        this.IsWait = false;
        this.Show_Error_msg('User Name all ready exits...Please try different user name!!!');
      }
      else if (_result == "-6") {
        this.IsWait = false;
        this.Show_Error_msg('Server side validation error...Please try again!!!');
      }
      else if (_result == "-8") {
        this.IsWait = false;
        this.Show_Error_msg('Command Not Passed...Please try again!!!');
      }
      else {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },
      (error: AppResponse) => {
        debugger;
        this.IsWait = false;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          //this.error = error.message;
          this.Show_Error_msg('Something went wrong...Please try again later!!!');
      });
  };

  Show_Success_msg(msg): void {
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg): void {
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg): void {
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  Btn_Reset() {
    this.IsUpdate = false;
  };

  selectTab(ev) {

    if (ev.tabId == "tb_lstVw") {

      this.tb_IsActive = false;

      if (this.IsNew_Entry) {
        // fetch new entry
        this.tble_loading = true;
        this.get_ListView_List("0", "10", "");
        setTimeout(() => {
          if (this.datasource) {
            this.Prod_ListView = this.datasource.slice(0, (0 + 10));
            this.tble_loading = false;
            this.IsNew_Entry = false;
          }
        }, 1000);
      }
    }

  }

  get_ListView_List(first, rows, globalFilter): void {

    globalFilter = globalFilter == null ? '' : globalFilter;
    const objModel = { StrQuery: "Sp_Get_AdminUser 'Get_ListView','" + first + "','" + rows + "','" + globalFilter + "','','',1,1", };

    this.AdminUser_Service.get_ListView(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.datasource = resp.row;
        //For List-View
        this.totalRecords = resp.row.length;
      }
      else {
        this.datasource = [];
        this.totalRecords = 0;
      }

    },
      (error: AppResponse) => {
        debugger;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  loadCustomers(event: LazyLoadEvent) {

    this.tble_loading = true;

    if (event.globalFilter != null) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (this.datasource.length == 0) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (event.first > 500) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }

    setTimeout(() => {
      if (this.datasource) {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
    }, 2000);

  }

  async editProduct(parm) {
    const str = `Sp_Get_AdminUser 'Get_AdminUser_Detail_ByUserId',${parm},'','','','',1,1`;
    const objModel = {
      StrQuery: str,
    };

    await this.AdminUser_Service.getData_JSON(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if (_result == "1") {
        debugger;
        let objResp = resp.row[0];
        this.IsUpdate = true;

        setTimeout(() => {

          this.AdminUserForm.controls['UserId'].setValue(objResp.UserId);
          this.AdminUserForm.controls['HotelId'].setValue(objResp.HotelId);
          this.AdminUserForm.controls['UserFName'].setValue(objResp.UserFName);
          this.AdminUserForm.controls['UserLName'].setValue(objResp.UserLName);
          this.AdminUserForm.controls['UserName'].setValue(objResp.UserName);
          this.AdminUserForm.controls['Password'].setValue(objResp.Password);
          this.AdminUserForm.controls['Mobile'].setValue(objResp.Mobile);
          this.AdminUserForm.controls['Email'].setValue(objResp.Email);
          this.AdminUserForm.controls['UserTypeId'].setValue(objResp.UserTypeId);
          this.AdminUserForm.controls['InActive'].setValue(objResp.IsInActive);

        }, 1000);

        this.tb_IsActive = true;

      }
      else {
        this.HotelIdList = [];
        this.UserTypeIdList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });

  };

  deleteProduct_Confirm(parm) {

    Swal.fire({
      title: 'Confirm',
      text: 'Are you sure you want to delete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {

        this.deleteProduct(parm);

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })

  }

  deleteProduct(parm) {

    const objModel: any = {
      UserId: parm,
      IpAddress: "::1",
      Company: 1,
      CreatedBy: 1,
      Command: "Delete"
    }

    this.AdminUser_Service.Save_AdminUser(objModel).subscribe((resp: any) => {
      let _result = resp.ResponseCode;
      if (_result == "6") {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.UserId !== parm);
        this.datasource = this.datasource.filter(val => val.UserId !== parm);
        this.showToast('success', 'Successful', 'Entry Deleted.');
      }
      else {
        this.showToast('error', 'Error', 'Entry Not Deleted.');
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });


  }


  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}