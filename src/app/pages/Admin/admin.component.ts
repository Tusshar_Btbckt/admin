import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-admin',
  template:`
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class AdminComponent {
}


// export class AdminComponent implements OnInit {
//   constructor() { }
//   ngOnInit(): void {
//   }
// }
