import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
  NbTabsetModule,
  NbBadgeModule
} from '@nebular/theme';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from '../admin/admin.component';
import { AdminEntriesComponent } from './admin-entries/admin-entries.component';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ImageDialogComponent } from './_dialog/image-dialog/image-dialog.component';
import { AdminUserComponent } from './admin-user/admin-user.component';
import { CountryComponent } from './country/country.component';
import { StateComponent } from './state/state.component';
import { CityComponent } from './city/city.component';

//For Table
import {TableModule} from 'primeng/table';

import {BadgeModule} from 'primeng/badge';

@NgModule({
  declarations: [
    AdminComponent,
    AdminEntriesComponent,
    ImageDialogComponent,
    AdminUserComponent,
    CountryComponent,
    StateComponent,
    CityComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule,
    TableModule,
    NbTabsetModule,
    NbBadgeModule,
    BadgeModule
  ]
})
export class AdminModule { }
