import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { City } from '../../../pages/_models/admin/city.model';
import { AppResponse } from '../../../pages/_models/app-response.model';
import { CityService } from '../../../pages/_services/admin/city.service';

import { LazyLoadEvent } from 'primeng/api';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})

export class CityComponent implements OnInit {
  CityForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;
  returnUrl: string;
  public StateList: any[];
  products = [];
  IsWait: boolean;

  IsSucess = false;
  IsWarning = false;
  IsDanger = false;
  IsSucessMsg: string;
  IsWarningMsg: string;
  IsDangerMsg: string;

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private City_Service: CityService,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    private authenticationService:AuthenticationService 
  ) { }

  config: NbToastrConfig;
  currentUserData:any = {}

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.get_State_List();

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  getFormControl(): void {
    this.CityForm = this.formBuilder.group({
      CityId: [''],
      State: ['', Validators.required],
      City: ['', Validators.required],
      InActive: [false]
    });
  }

  get_State_List(): void {

    const objModel = { StrQuery: "Sp_Get_City 'Get_State_List','','','','','',1,1", };

    this.City_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.StateList = resp.row;
      }
      else {
        this.StateList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  // convenience getter for easy access to form fields
  get f() { return this.CityForm.controls; }

  onSubmit(Name) {
debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.CityForm.invalid) {
      this.IsWait = false;
      return false;
    }

    const objModel: City = {
      CityId: Name == "Update" ? this.f.CityId.value : 0,
      State: this.f.State.value,
      City: this.f.City.value,
      IpAddress: "::1",
      IsActive: this.f.InActive.value == false ? true : false,
      IsDelete: this.f.InActive.value == true ? true : false,
      Company: this.currentUserData.Company,
      CreatedBy: this.currentUserData.UserId,
      Command: Name

    }
    debugger;
    this.City_Service.Save_City(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.ResponseCode;
      if (_result == "2") {
        this.IsWait = false;
        this.CityForm.reset();
        this.CityForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
      }
      else if (_result == "4") {
        this.IsWait = false;
        this.CityForm.reset();
        this.CityForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else if (_result == "-4") {
        this.IsWait = false;
        this.Show_Error_msg('City Name all ready exits...!!!');
      }
      else if (_result == "-6") {
        this.IsWait = false;
        this.Show_Error_msg('Server side validation error...Please try again!!!');
      }
      else if (_result == "-8") {
        this.IsWait = false;
        this.Show_Error_msg('Command Not Passed...Please try again!!!');
      }
      else {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },
      (error: AppResponse) => {
        debugger;
        this.IsWait = false;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          //this.error = error.message;
          this.Show_Error_msg('Something went wrong...Please try again later!!!');
      });
  };

  Show_Success_msg(msg): void {
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg): void {
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg): void {
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  Btn_Reset() {
    this.IsUpdate = false;
  };

  selectTab(ev) {

    if (ev.tabId == "tb_lstVw") {

      this.tb_IsActive = false;

      if (this.IsNew_Entry) {
        // fetch new entry
        this.tble_loading = true;
        this.get_ListView_List("0", "10", "");
        setTimeout(() => {
          if (this.datasource) {
            this.Prod_ListView = this.datasource.slice(0, (0 + 10));
            this.tble_loading = false;
            this.IsNew_Entry = false;
          }
        }, 1000);
      }
    }

  }

  get_ListView_List(first, rows, globalFilter): void {

    globalFilter = globalFilter == null ? '' : globalFilter;
    const objModel = { StrQuery: "Sp_Get_City 'Get_ListView','" + first + "','" + rows + "','" + globalFilter + "','','',1,1", };

    this.City_Service.get_ListView(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.datasource = resp.row;
        //For List-View
        this.totalRecords = resp.row.length;
      }
      else {
        this.datasource = [];
        this.totalRecords = 0;
      }

    },
      (error: AppResponse) => {
        debugger;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  loadCustomers(event: LazyLoadEvent) {

    this.tble_loading = true;

    if (event.globalFilter != null) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (this.datasource.length == 0) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (event.first > 500) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }

    setTimeout(() => {
      if (this.datasource) {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
    }, 2000);

  }

  async editProduct(parm) {
    const str = `Sp_Get_City 'Get_City_Detail_ById',${parm},'','','','',1,1`;
    const objModel = {
      StrQuery: str,
    };

    await this.City_Service.getData_JSON(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if (_result == "1") {
        debugger;
        let objResp = resp.row[0];
        this.IsUpdate = true;

        setTimeout(() => {

          this.CityForm.controls['CityId'].setValue(objResp.CityId);
          this.CityForm.controls['State'].setValue(objResp.State);
          this.CityForm.controls['City'].setValue(objResp.City);
          this.CityForm.controls['InActive'].setValue(objResp.IsInActive);

        }, 1000);

        this.tb_IsActive = true;

      }
      else {
        this.StateList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });

  };

  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}