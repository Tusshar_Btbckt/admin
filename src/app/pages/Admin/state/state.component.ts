import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective } from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { State } from '../../../pages/_models/admin/state.model';
import { AppResponse } from '../../../pages/_models/app-response.model';
import { StateService } from '../../../pages/_services/admin/state.service';

import { LazyLoadEvent } from 'primeng/api';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss']
})

export class StateComponent implements OnInit {
  StateForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;
  returnUrl: string;
  public CountryList: any[];
  products = [];
  IsWait: boolean;

  IsSucess = false;
  IsWarning = false;
  IsDanger = false;
  IsSucessMsg: string;
  IsWarningMsg: string;
  IsDangerMsg: string;

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private State_Service: StateService,
    private formBuilder: FormBuilder,
    private toastrService: NbToastrService,
    private authenticationService:AuthenticationService 
  ) { }

  config: NbToastrConfig;
  currentUserData:any = {}
  
  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.get_Country_List();

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  getFormControl(): void {
    this.StateForm = this.formBuilder.group({
      StateId: [''],
      Country: ['', Validators.required],
      State: ['', Validators.required],
      InActive: [false]
    });
  }

  get_Country_List(): void {

    const objModel = { StrQuery: "Sp_Get_State 'Get_Country_List','','','','','',1,1", };

    this.State_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.CountryList = resp.row;
      }
      else {
        this.CountryList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  // convenience getter for easy access to form fields
  get f() { return this.StateForm.controls; }

  onSubmit(Name) {

    this.IsWait = true;
    // stop here if form is invalid
    if (this.StateForm.invalid) {
      this.IsWait = false;
      return false;
    }

    const objModel: State = {
      StateId: Name == "Update" ? this.f.StateId.value : 0,
      Country: this.f.Country.value,
      State: this.f.State.value,
      IpAddress: "::1",
      IsActive: this.f.InActive.value == false ? true : false,
      IsDelete: this.f.InActive.value == true ? true : false,
      Company: this.currentUserData.Company,
      CreatedBy: this.currentUserData.UserId,
      Command: Name
    }
    debugger;
    this.State_Service.Save_State(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.ResponseCode;
      if (_result == "2") {
        this.IsWait = false;
        this.StateForm.reset();
        this.StateForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
      }
      else if (_result == "4") {
        this.IsWait = false;
        this.StateForm.reset();
        this.StateForm.setErrors(null);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else if (_result == "-4") {
        this.IsWait = false;
        this.Show_Error_msg('State Name all ready exits...!!!');
      }
      else if (_result == "-6") {
        this.IsWait = false;
        this.Show_Error_msg('Server side validation error...Please try again!!!');
      }
      else if (_result == "-8") {
        this.IsWait = false;
        this.Show_Error_msg('Command Not Passed...Please try again!!!');
      }
      else {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },
      (error: AppResponse) => {
        debugger;
        this.IsWait = false;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          //this.error = error.message;
          this.Show_Error_msg('Something went wrong...Please try again later!!!');
      });
  };

  Show_Success_msg(msg): void {
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg): void {
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg): void {
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  Btn_Reset() {
    this.IsUpdate = false;
  };

  selectTab(ev) {

    if (ev.tabId == "tb_lstVw") {

      this.tb_IsActive = false;

      if (this.IsNew_Entry) {
        // fetch new entry
        this.tble_loading = true;
        this.get_ListView_List("0", "10", "");
        setTimeout(() => {
          if (this.datasource) {
            this.Prod_ListView = this.datasource.slice(0, (0 + 10));
            this.tble_loading = false;
            this.IsNew_Entry = false;
          }
        }, 1000);
      }
    }

  }

  get_ListView_List(first, rows, globalFilter): void {

    globalFilter = globalFilter == null ? '' : globalFilter;
    const objModel = { StrQuery: "Sp_Get_State 'Get_ListView','" + first + "','" + rows + "','" + globalFilter + "','','',1,1", };

    this.State_Service.get_ListView(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.datasource = resp.row;
        //For List-View
        this.totalRecords = resp.row.length;
      }
      else {
        this.datasource = [];
        this.totalRecords = 0;
      }

    },
      (error: AppResponse) => {
        debugger;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  loadCustomers(event: LazyLoadEvent) {

    this.tble_loading = true;

    if (event.globalFilter != null) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (this.datasource.length == 0) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }
    else if (event.first > 500) {
      this.get_ListView_List(event.first, event.rows, event.globalFilter);
    }

    setTimeout(() => {
      if (this.datasource) {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
    }, 2000);

  }

  async editProduct(parm) {
    const str = `Sp_Get_State 'Get_State_Detail_ById',${parm},'','','','',1,1`;
    const objModel = {
      StrQuery: str,
    };

    await this.State_Service.getData_JSON(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if (_result == "1") {
        debugger;
        let objResp = resp.row[0];
        this.IsUpdate = true;

        setTimeout(() => {

          this.StateForm.controls['StateId'].setValue(objResp.StateId);
          this.StateForm.controls['Country'].setValue(objResp.Country);
          this.StateForm.controls['State'].setValue(objResp.State);
          this.StateForm.controls['InActive'].setValue(objResp.IsInActive);

        }, 1000);

        this.tb_IsActive = true;

      }
      else {
        this.CountryList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });

  };

  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

}