import { Component, OnDestroy, OnInit } from "@angular/core";
import { takeUntil, takeWhile } from "rxjs/operators";
import { forkJoin, Subject } from "rxjs";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AuthenticationService } from "../../services/authentication.service";
import { Router } from "@angular/router";

@Component({
  selector: "login",
  styleUrls: ["./login.component.scss"],
  templateUrl: "./login.component.html",
})
export class LoginComponent implements OnDestroy, OnInit {
  loginForm: FormGroup;
  contacts: any[];
  recent: any[];
  private destroy$ = new Subject<void>();
  ngOnInit() {
  let currentUser = localStorage.getItem("_cred_dt")
    if (currentUser) {
        this.router.navigate(['/pages']);
      }
    this.createLoginForm();
  }
  constructor(
    private formBuilder: FormBuilder,private router: Router,
    private authenticationService: AuthenticationService
  ) {}
  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ["", [Validators.required]],
      password: [
        null,
        Validators.compose([
          Validators.required,
          Validators.pattern(
            /^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)(?=.*[$@$!%*#?&])/
          ),
          Validators.minLength(6),
        ]),
      ],
    });
  }

  login() {
    debugger;
    // if (this.loginForm.invalid) {
    //   this.loginForm.markAllAsTouched();
    //   return;
    // }
    const loginPost: any = {
      UserName: this.loginForm.value.email,
      Password: this.loginForm.value.password,
      grant_type: "password",
    };

    this.authenticationService
      .login(loginPost)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res: any) => {
        console.log(res);
        if (res.row != null) {
          let arr = []
          res.row.MenuList.forEach(element => {
            arr.push(element.Code)
          });
          console.log(arr.toString())
          var encryptData = this.authenticationService.EncryptData(res.row);
          localStorage.setItem("_cred_dt", encryptData);
          this.router.navigate(['/pages']);
        }
      });
  }
  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
