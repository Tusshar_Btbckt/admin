import { Injectable } from "@angular/core";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import {
  AddStock,
  AS_Resp_Models,
  AS_Common_Models,
} from "../../_models/Inventory/add-stock.model";
import { environment } from "../../../../environments/environment";
import { Observable } from "rxjs";
import * as CryptoJS from "crypto-js";

const httpOptions = {
  headers: new HttpHeaders({
    "Content-Type": "application/json; charset=utf-8",
    "Access-Control-Allow-Origin": "https://icorrect.in",
  }),
};

@Injectable({
  providedIn: "root",
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient) {}

  public login(logindata: any): Observable<any> {
    return this.httpClient.post<any>(
      `${environment.webapiUrl}/api/SvnElvn/Login/`,
      logindata,
      httpOptions
    );
  }

  EncryptData = (data: any) => {
    try {
      const encryptedText = CryptoJS.AES.encrypt(
        JSON.stringify(data),
        "b5d04a565541bdc90f0a2d9ff41d6831a9f041415d019248e7812fa435b7a46c39e3ca6fd329507a2f8faa3e993bf88b6e64dd5b7b8a56e1678558b86229ea29"
      ).toString();
      return encryptedText;
    } catch (ex) {}
  };

  DecryptData = (data: string) => {
    try {
      const decryptedText = CryptoJS.AES.decrypt(
        data,
        "b5d04a565541bdc90f0a2d9ff41d6831a9f041415d019248e7812fa435b7a46c39e3ca6fd329507a2f8faa3e993bf88b6e64dd5b7b8a56e1678558b86229ea29"
      ).toString(CryptoJS.enc.Utf8);
      return decryptedText;
    } catch (ex) {}
  };

  getPermission(key: string) {
    var localData = localStorage.getItem("_cred_dt");
    if (localData) {
      let currentUser = JSON.parse(
        this.DecryptData(localStorage.getItem("_cred_dt"))
      );
      let menuList = currentUser.MenuList;
      let isExist = menuList.filter((w) => w.Code == key);
      if (isExist.length > 0) {
        return isExist[0].IsPermission == 1 ? true : false;
      } else {
        console.log("Permission Key Not Found");
        return false;
      }
    }
    return false;
  }
  getUserData() {
    var localData = localStorage.getItem("_cred_dt");
    if (localData) {
      let currentUser = JSON.parse(
        this.DecryptData(localStorage.getItem("_cred_dt"))
      );
      return currentUser;
    }
    return null;
  }
}
