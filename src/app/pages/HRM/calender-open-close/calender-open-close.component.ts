import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Calender {
  XCode: string;
  XName: string;
}

interface Period {
  XCode: string;
  XName: string;
}

interface Action {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-calender-open-close',
  templateUrl: './calender-open-close.component.html',
  styleUrls: ['./calender-open-close.component.scss']
})

export class CalenderOpenCloseComponent implements OnInit {

  CalenderOpenCloseForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Calenders: Calender[] = [
    {XCode: '1', XName:'2021'}
  ]; 

  Periods: Period[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '1', XName:'Feb-2021'}
  ]; 

  Actions: Action[] = [
    {XCode: '1', XName:'Open'},
    {XCode: '1', XName:'Close'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.CalenderOpenCloseForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Calender: ['', Validators.required],
      Period: ['', Validators.required],
      FromDate: ['', Validators.required],
      ToDate: ['', Validators.required],
      Action: ['', Validators.required],
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.CalenderOpenCloseForm.controls; }

}