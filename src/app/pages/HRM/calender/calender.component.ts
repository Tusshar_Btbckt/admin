import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Calender {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.scss']
})

export class CalenderComponent implements OnInit {

  CalenderForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Calenders: Calender[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '2', XName:'Feb-2021'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.CalenderForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Name: ['', Validators.required],
      Calender: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.CalenderForm.controls; }

}