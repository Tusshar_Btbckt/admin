import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Department {
  XCode: string;
  XName: string;
}

interface Year {
  XCode: string;
  XName: string;
}

interface Month {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-daily-attendance',
  templateUrl: './daily-attendance.component.html',
  styleUrls: ['./daily-attendance.component.scss']
})

export class DailyAttendanceComponent implements OnInit {

  DailyAttendanceForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Departments: Department[] = [
    {XCode: '1', XName:'Head office'}
  ]; 

  Years: Year[] = [
    {XCode: '1', XName:'2021'}
  ]; 

  Months: Month[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '1', XName:'Feb-2021'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.DailyAttendanceForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Department: ['', Validators.required],
      Year: ['', Validators.required],
      Month: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.DailyAttendanceForm.controls; }

}