import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Gender {
  XCode: string;
  XName: string;
}

interface MaritalStatus {
  XCode: string;
  XName: string;
}

interface Nationality {
  XCode: string;
  XName: string;
}

interface Department {
  XCode: string;
  XName: string;
}

interface JobPost {
  XCode: string;
  XName: string;
}

interface SalaryPaidOn {
  XCode: string;
  XName: string;
}

interface Status {
  XCode: string;
  XName: string;
}

interface EmployeeType {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-employee-profile',
  templateUrl: './employee-profile.component.html',
  styleUrls: ['./employee-profile.component.scss']
})

export class EmployeeProfileComponent implements OnInit {

  EmployeeProfileForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Genders: Gender[] = [
    {XCode: '1', XName:'1'}
  ]; 

  MaritalStatuss: MaritalStatus[] = [
    {XCode: '1', XName:'1'}
  ]; 

  Nationalitys: Nationality[] = [
    {XCode: '1', XName:'1'}
  ]; 

  Departments: Department[] = [
    {XCode: '1', XName:'1'}
  ]; 

  JobPosts: JobPost[] = [
    {XCode: '1', XName:'1'}
  ]; 

  SalaryPaidOns: SalaryPaidOn[] = [
    {XCode: '1', XName:'1'}
  ]; 

  Statuss: Status[] = [
    {XCode: '1', XName:'1'}
  ]; 

  EmployeeTypes: EmployeeType[] = [
    {XCode: '1', XName:'1'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.EmployeeProfileForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Name: ['', Validators.required],
      Mobile: ['', Validators.required],
      Email: ['', Validators.required],
      DOB: ['', Validators.required],
      Gender: ['', Validators.required],
      MaritalStatus: ['', Validators.required],
      Qualification: ['', Validators.required],
      Nationality: ['', Validators.required],
      Address: ['', Validators.required],
      DOJ: ['', Validators.required],
      Department: ['', Validators.required],
      JobPost: ['', Validators.required],
      SalaryPaidOn: ['', Validators.required],
      Status: ['', Validators.required],
      EmployeeType: ['', Validators.required],
      EmpId: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.EmployeeProfileForm.controls; }

}