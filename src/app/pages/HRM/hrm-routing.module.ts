import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalenderComponent } from './calender/calender.component';
import { CalenderOpenCloseComponent } from './calender-open-close/calender-open-close.component';
import { DailyAttendanceComponent } from './daily-attendance/daily-attendance.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { MonthlyAttendanceComponent } from './monthly-attendance/monthly-attendance.component';
import { PayrollCalenderComponent } from './payroll-calender/payroll-calender.component';
import { SalaryComponent } from './salary/salary.component';
import { SalaryCalculationComponent } from './salary-calculation/salary-calculation.component';
import { HRMComponent } from './hrm.component';

const routes: Routes = [
  {
    path: '',
    component: HRMComponent,
    children: [
      {
        path: 'calender',
        component: CalenderComponent,
      },
      {
        path: 'calender-open-close',
        component: CalenderOpenCloseComponent,
      },
      {
        path: 'payroll-calender',
        component: PayrollCalenderComponent,
      },
      {
        path: 'employee-profile',
        component: EmployeeProfileComponent,
      },
      {
        path: 'salary',
        component: SalaryComponent,
      },
      {
        path: 'salary-calculation',
        component: SalaryCalculationComponent,
      },
      {
        path: 'daily-attendance',
        component: DailyAttendanceComponent,
      },
      {
        path: 'monthly-attendance',
        component: MonthlyAttendanceComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HRMRoutingModule { }