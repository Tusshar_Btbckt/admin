import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
} from '@nebular/theme';

import { HRMRoutingModule } from './hrm-routing.module';
import { HRMComponent } from '../hrm/hrm.component';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { ImageDialogComponent } from './_dialog/image-dialog/image-dialog.component';

import { CalenderComponent } from './calender/calender.component';
import { CalenderOpenCloseComponent } from './calender-open-close/calender-open-close.component';
import { DailyAttendanceComponent } from './daily-attendance/daily-attendance.component';
import { EmployeeProfileComponent } from './employee-profile/employee-profile.component';
import { MonthlyAttendanceComponent } from './monthly-attendance/monthly-attendance.component';
import { PayrollCalenderComponent } from './payroll-calender/payroll-calender.component';
import { SalaryComponent } from './salary/salary.component';
import { SalaryCalculationComponent } from './salary-calculation/salary-calculation.component';


@NgModule({
  declarations: [
    HRMComponent,
    CalenderComponent,
    CalenderOpenCloseComponent,
    DailyAttendanceComponent,
    EmployeeProfileComponent,
    MonthlyAttendanceComponent,
    PayrollCalenderComponent,
    SalaryComponent,
    SalaryCalculationComponent
  ],
  imports: [
    CommonModule,
    HRMRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule
  ]
})
export class HRMModule { }
