import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Department {
  XCode: string;
  XName: string;
}

interface Calender {
  XCode: string;
  XName: string;
}

interface Period {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-monthly-attendance',
  templateUrl: './monthly-attendance.component.html',
  styleUrls: ['./monthly-attendance.component.scss']
})

export class MonthlyAttendanceComponent implements OnInit {

  MonthlyAttendanceForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Departments: Department[] = [
    {XCode: '1', XName:'Head office'}
  ]; 

  Calenders: Calender[] = [
    {XCode: '1', XName:'2021'}
  ]; 

  Periods: Period[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '1', XName:'Feb-2021'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.MonthlyAttendanceForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Department: ['', Validators.required],
      Calender: ['', Validators.required],
      Period: ['', Validators.required],
      WorkingDays: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.MonthlyAttendanceForm.controls; }

}