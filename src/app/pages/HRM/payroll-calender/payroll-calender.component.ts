import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface Calender {
  XCode: string;
  XName: string;
}

interface CalenderType {
  XCode: string;
  XName: string;
}

interface PeriodType {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-payroll-calender',
  templateUrl: './payroll-calender.component.html',
  styleUrls: ['./payroll-calender.component.scss']
})

export class PayrollCalenderComponent implements OnInit {

  PayrollCalenderForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Calenders: Calender[] = [
    {XCode: '1', XName:'Financial'}
  ]; 

  CalenderTypes: CalenderType[] = [
    {XCode: '1', XName:'2021'}
  ]; 

  PeriodTypes: PeriodType[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '2', XName:'Feb-2021'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.PayrollCalenderForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Calender: ['', Validators.required],
      CalenderType: ['', Validators.required],
      PeriodType: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.PayrollCalenderForm.controls; }

}