import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface CalenderType {
  XCode: string;
  XName: string;
}

interface Month {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-salary-calculation',
  templateUrl: './salary-calculation.component.html',
  styleUrls: ['./salary-calculation.component.scss']
})

export class SalaryCalculationComponent implements OnInit {

  SalaryCalculationForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  CalenderTypes: CalenderType[] = [
    {XCode: '1', XName:'2021'}
  ]; 

  Months: Month[] = [
    {XCode: '1', XName:'Jan-2021'},
    {XCode: '2', XName:'Feb-2021'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.SalaryCalculationForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      CalenderType: ['', Validators.required],
      Month: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.SalaryCalculationForm.controls; }

}