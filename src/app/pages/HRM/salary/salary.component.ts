import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';

interface EmployeeType {
  XCode: string;
  XName: string;
}

interface SalaryPaidOn {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-salary',
  templateUrl: './salary.component.html',
  styleUrls: ['./salary.component.scss']
})

export class SalaryComponent implements OnInit {

  SalaryForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  EmployeeTypes: EmployeeType[] = [
    {XCode: '1', XName:'Permanent'},
    {XCode: '2', XName:'Free-lancer'}
  ]; 

  SalaryPaidOns: SalaryPaidOn[] = [
    {XCode: '1', XName:'Monthly'},
    {XCode: '2', XName:'Hourly'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.SalaryForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      EmployeeType: ['', Validators.required],
      SalaryPaidOn: ['', Validators.required],
      EmployeeName: ['', Validators.required],
      MonthlySalary: ['', Validators.required]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.SalaryForm.controls; }

}