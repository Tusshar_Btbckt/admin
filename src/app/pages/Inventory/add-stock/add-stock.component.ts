import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { AddStock,AS_Resp_Models,AS_Common_Models,AS_Dropdown_Models} from '../../../pages/_models/Inventory/add-stock.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { AddStockService } from '../../../pages/_services/Inventory/add-stock.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { NbDateService } from '@nebular/theme';

// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

import { AuthenticationService } from '../../Authentication/services/authentication.service';

@Component({
  selector: 'ngx-add-stock',
  templateUrl: './add-stock.component.html',
  styleUrls: ['./add-stock.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})

export class AddStockComponent implements OnInit {

  AddStockForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  // Master
  public ItemName_List: AS_Dropdown_Models[];
  public PackUnit_List: AS_Dropdown_Models[];
  public Kitchen_List: AS_Dropdown_Models[];
  
  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor( 
    private formBuilder: FormBuilder,
    private AddStockService: AddStockService,
    private toastrService: NbToastrService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     private authenticationService:AuthenticationService,
     protected dateService: NbDateService<Date> 
    ) { }

    currentUserData:any = {}
    
    ngOnInit(): void {
     this.currentUserData =  this.authenticationService.getUserData();
      this.getFormControl();
      this.get_ItemName_List();
      this.get_PackUnit_List();
      this.get_Kitchen_List();

      //For List-View
      this.totalRecords = 0;
      this.tble_loading = true;
      this.datasource = [];
    }

  getFormControl(): void {
    this.AddStockForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      ItemName: ['', Validators.required],
      PackUnit: ['', Validators.required],
      Qty: ['', Validators.required],
      Kitchen: ['', Validators.required],
      InActive:[false]
    });
  }

  get_ItemName_List(): void {

    const objModel = { StrQuery:"Sp_Get_AddStock 'Get_ItemName_List','','','','','',1,1", };

    this.AddStockService.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.ItemName_List = resp.row;
      }
      else
      {
        this.ItemName_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_PackUnit_List(): void {

    const objModel = { StrQuery:"Sp_Get_AddStock 'Get_PackUnit_List','','','','','',1,1", };

    this.AddStockService.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.PackUnit_List = resp.row;
      }
      else
      {
        this.PackUnit_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Kitchen_List(): void {

    const objModel = { StrQuery:"Sp_Get_AddStock 'Get_Kitchen_List','','','','','',1,1", };

    this.AddStockService.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Kitchen_List = resp.row;
      }
      else
      {
        this.Kitchen_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

   // convenience getter for easy access to form fields
   get f() { return this.AddStockForm.controls; }

   onSubmit(Name){
    
    debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.AddStockForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }

    debugger;
    const objModel: AddStock = {
      SrNo: Name == "Update" ? this.f.SrNo.value : "0",
      Date:this.f.Date.value,
      ItemName:this.f.ItemName.value,
      PackUnit:this.f.PackUnit.value,
      Qty:this.f.Qty.value,
      Kitchen:this.f.Kitchen.value,
      IpAddress:"::1",
      //IsActive:this.f.InActive.value == false ? true:false,
      //IsDelete:this.f.InActive.value == true ? true:false,
      IsActive:true,
      IsDelete:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
  }


  debugger;
    this.AddStockService.Save_AddStock(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.AddStockForm.reset();
        this.AddStockForm.setErrors(null);
        this.IsNew_Entry = true;
        setTimeout(() => {      
          this.AddStockForm.controls['Date'].setValue(this.dateService.today());
          this.AddStockForm.controls['InActive'].setValue(false);
        }, 500);
        this.Show_Success_msg('Data Save Successfully!!!');
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.AddStockForm.reset();
        this.AddStockForm.setErrors(null);
        this.IsNew_Entry = true;
        setTimeout(() => {      
          this.AddStockForm.controls['Date'].setValue(this.dateService.today());
          this.AddStockForm.controls['InActive'].setValue(false);
        }, 500);
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else if (_result == "-6") {
        this.IsWait = false;
        this.Show_Error_msg('Server side validation error...Please try again!!!');
      }
      else if (_result == "-8") {
        this.IsWait = false;
        this.Show_Error_msg('Command Not Passed...Please try again!!!');
      }
      else if (_result == "-14") {
        this.IsWait = false;
        this.Show_Warning_msg('Stock Not Available');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  };


  Btn_Reset(){
    this.IsUpdate = false;
    this.AddStockForm.reset();
    this.AddStockForm.setErrors(null);

    setTimeout(() => {      
      this.AddStockForm.controls['Date'].setValue(this.dateService.today());
      this.AddStockForm.controls['InActive'].setValue(false);
    }, 500);
  };

  // Notification msg
Show_Success_msg(msg):void{
  this.IsSucessMsg = msg;
  this.IsSucess = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsSucess = false;
    this.IsSucessMsg = "";
  }, 3000);
};

Show_Warning_msg(msg):void{
  this.IsWarningMsg = msg;
  this.IsWarning = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsWarning = false;
    this.IsWarningMsg = "";
  }, 3000);
};

Show_Error_msg(msg):void{
  this.IsDangerMsg = msg;
  this.IsDanger = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsDanger = false;
    this.IsDangerMsg = "";
  }, 3000);
};


   // List View
  selectTab(ev) {
    debugger;
      if(ev.tabId == "tb_lstVw"){
   
       this.tb_IsActive = false;
       
        if(this.IsNew_Entry){
         // fetch new entry
         this.tble_loading = true;
         this.get_ListView_List("0","10","");
  
         setTimeout(() => {
           if (this.datasource) {
               this.Prod_ListView = this.datasource.slice(0, (0 + 10));
               this.tble_loading = false;
               this.IsNew_Entry= false;
           }
         }, 1000);
        }
      }
   
     }
  
     get_ListView_List(first,rows,globalFilter): void {
      debugger;
  
      globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
       const objModel = { 
         StrQuery:`Sp_Get_AddStock 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
       };
       
      this.AddStockService.getData_JSON(objModel).subscribe((resp: any) => {
        //debugger;
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          debugger;
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
  
          this.Prod_ListView = this.datasource.slice(first, (first + rows));
          this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
        }
      },   
      (error: AppResponse) => {
        debugger;
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
    };
  
    loadCustomers(event: LazyLoadEvent) {
      debugger;
      this.tble_loading = true;
  
      if(event.globalFilter!= null){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(this.datasource.length == 0){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(event.first > 500){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      
      if(this.datasource.length > 0)
      {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
  
    }

     // Edit
  async editAddStock(parm){
    const str = `Sp_Get_AddStock 'Get_AddStock_Details_BySrNo',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.AddStockService.getData_JSON(objModel).subscribe((resp: any) => {
        debugger;
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        let objResp = resp.row;
        this.IsUpdate = true;
        
        setTimeout(() => {
        this.AddStockForm.controls['SrNo'].setValue(objResp[0].SrNo);
        this.AddStockForm.controls['Date'].setValue(objResp[0].Date);
        this.AddStockForm.controls['ItemName'].setValue(objResp[0].ItemName);
        this.AddStockForm.controls['PackUnit'].setValue(objResp[0].PackUnit);
        this.AddStockForm.controls['Qty'].setValue(objResp[0].Qty);
        this.AddStockForm.controls['Kitchen'].setValue(objResp[0].Kitchen);
        this.AddStockForm.controls['InActive'].setValue(objResp[0].IsInActive);
        }, 1000);
       
        this.tb_IsActive = true;

      }
      else
      {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
    
  };

  deleteAddStock_Confirm(parm) {

    Swal.fire({
      title: 'Confirm',
      text: 'Are you sure you want to delete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {

        this.deleteAddStock(parm);

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })


  }

  deleteAddStock(parm) {
    debugger
    const objModel: any = {
      SrNo: parm,
      IpAddress: "::1",
      Company: 1,
      CreatedBy: 1,
      Command: "Delete"
    }

    this.AddStockService.Save_AddStock(objModel).subscribe((resp: any) => {
      let _result = resp.ResponseCode;
      if (_result == "6") {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.SrNo !== parm);
        this.datasource = this.datasource.filter(val => val.SrNo !== parm);
        this.showToast('success', 'Successful', 'Entry Deleted.');
      }
      else {
        this.showToast('error', 'Error', 'Entry Not Deleted.');
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });


  }

  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }


}