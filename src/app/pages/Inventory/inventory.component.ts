import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-inventory',
  template: `
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})

export class InventoryComponent {
}

// export class InventoryComponent implements OnInit {
//   constructor() { }
//   ngOnInit(): void {
//   }
// }
