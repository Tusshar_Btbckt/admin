import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
  NbTabsetModule
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { InventoryRoutingModule } from './inventory-routing.module';
import { InventoryComponent } from '../inventory/inventory.component';
import { AddStockComponent } from './add-stock/add-stock.component';

import {BadgeModule} from 'primeng/badge';
//For Table
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';

import {AutoCompleteModule} from 'primeng/autocomplete';

import {DropdownModule} from 'primeng/dropdown';
import { StockTransferComponent } from './stock-transfer/stock-transfer.component';


@NgModule({
  declarations: [
    InventoryComponent,
    AddStockComponent,
    StockTransferComponent
  ],
  imports: [
    CommonModule,
    InventoryRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule,
    TableModule,
    NbTabsetModule,
    BadgeModule,
    DialogModule,
    ConfirmDialogModule,
    ToolbarModule,
    ButtonModule,
    AutoCompleteModule,
    DropdownModule
  ]
})
export class InventoryModule { }
