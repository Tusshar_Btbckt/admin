import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  StockTransfer,ST_Resp_Models,ST_Common_Models,ST_Dropdown_Models,Product,Item
} from '../../../pages/_models/Inventory/stock-transfer.model';

import { AppResponse} from '../../../pages/_models/app-response.model';
import { StockTransferService } from '../../../pages/_services/Inventory/stock-transfer.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
import { NbDateService } from '@nebular/theme';
// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-stock-transfer',
  templateUrl: './stock-transfer.component.html',
  styleUrls: ['./stock-transfer.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})
export class StockTransferComponent implements OnInit {

  StockTransferForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  //products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  TrxForm: FormGroup;
  submitted_trx = false;

  // Master
  public PackQty_List: ST_Dropdown_Models[];
  public FromKitchen_List: ST_Dropdown_Models[];
  public ToKitchen_List: ST_Dropdown_Models[];

  // Trx
  productDialog: boolean;
  products: Product[];
  product: any;
  selectedProducts: Product[];
  //submitted: boolean;
  statuses: any[];
  Item:Item;

  text: string;
  selectedItem: any;
  filteredItems: any[];
  filteredItems_PackUnit: any[];
  items: any[];

   //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor( 
    private formBuilder: FormBuilder,
    private StockTransfer_Service: StockTransferService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private currencyPipe : CurrencyPipe,
    private toastrService: NbToastrService,
    private authenticationService:AuthenticationService,
    protected dateService: NbDateService<Date>  
    ) { }

  config: NbToastrConfig;
  currentUserData:any = {}

  // convenience getter for easy access to form fields
  get f() { return this.StockTransferForm.controls; }
  get t() { return this.TrxForm.controls; }

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.getTrx_FormControl();
    this.get_PackQty_List();
    this.get_Item_List();
    this.get_From_Kitchen_List_List();
    this.get_To_Kitchen_List_List();

    this.product = {};
    this.products = [];

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];

  }

  getFormControl(): void {
    this.StockTransferForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      FromKitchen: ['', Validators.required],
      ToKitchen: ['', Validators.required],
      InActive:[false]
    });
  }

  getTrx_FormControl(): void {
    //const regexPattern = "/^-?\d*[.,]?\d{0,2}$/";
    
    this.TrxForm = this.formBuilder.group({
      Id: [''],
      Sr: [''],
      Item: ['',Validators.required],
      Qty:  ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      PackQty: ['',Validators.required],
      //InActive:[false]
    });
  }

  get_PackQty_List(): void {

    const objModel = { StrQuery:"Sp_StockTransferMst 'Get_PackQty_List','','','','','',1,1", };

    this.StockTransfer_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.PackQty_List = resp.row;
      }
      else
      {
        this.PackQty_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_From_Kitchen_List_List(): void {

    const objModel = { StrQuery:"Sp_StockTransferMst 'Get_Kitchen_List','','','','','',1,1", };

    this.StockTransfer_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.FromKitchen_List = resp.row;
      }
      else
      {
        this.FromKitchen_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_To_Kitchen_List_List(): void {

    const objModel = { StrQuery:"Sp_StockTransferMst 'Get_Kitchen_List','','','','','',1,1", };

    this.StockTransfer_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.ToKitchen_List = resp.row;
      }
      else
      {
        this.ToKitchen_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  
  onSubmit(Name){
    
    debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.StockTransferForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }
    debugger;

    
    var asdasd = this.f.InActive.value;
    debugger;
    const objModel: StockTransfer = {
      SrNo:this.f.SrNo.value,
      Date:this.f.Date.value,
      From_Kitchen:this.f.FromKitchen.value,
      To_Kitchen:this.f.ToKitchen.value,
      objlist:this.products.map(element => ({ 
        Sr:element.Sr,
        ItemCode:element.Item.value,
        PackUnit:element.PackQty.value,
        Qty:element.Qty
      })),
      IpAddress:"::1",
      //IsActive:this.f.InActive.value == false ? true:false,
      //IsDelete:this.f.InActive.value == true ? true:false,
      IsActive:true,
      IsDelete:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name

  }


  debugger;
    this.StockTransfer_Service.Save_StockTransferMst(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.StockTransferForm.reset();
        this.StockTransferForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.StockTransferForm.reset();
        this.StockTransferForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  };

  Btn_Reset(){
    this.IsUpdate = false;
    this.StockTransferForm.reset();
    this.StockTransferForm.setErrors(null);
    
    this.StockTransferForm.reset();
    this.StockTransferForm.setErrors(null);
    this.product = {};
    this.products = [];
    
    setTimeout(() => {      
      this.StockTransferForm.controls['Date'].setValue(this.dateService.today());
    }, 500);

  };


  // Notification msg
Show_Success_msg(msg):void{
  this.IsSucessMsg = msg;
  this.IsSucess = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsSucess = false;
    this.IsSucessMsg = "";
  }, 3000);
};

Show_Warning_msg(msg):void{
  this.IsWarningMsg = msg;
  this.IsWarning = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsWarning = false;
    this.IsWarningMsg = "";
  }, 3000);
};

Show_Error_msg(msg):void{
  this.IsDangerMsg = msg;
  this.IsDanger = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsDanger = false;
    this.IsDangerMsg = "";
  }, 3000);
};

// List View
selectTab(ev) {
  debugger;
    if(ev.tabId == "tb_lstVw"){
 
     this.tb_IsActive = false;
     
      if(this.IsNew_Entry){
       // fetch new entry
       this.tble_loading = true;
       this.get_ListView_List("0","10","");

       setTimeout(() => {
         if (this.datasource) {
             this.Prod_ListView = this.datasource.slice(0, (0 + 10));
             this.tble_loading = false;
             this.IsNew_Entry= false;
         }
       }, 1000);
      }
    }
 
   }

   get_ListView_List(first,rows,globalFilter): void {
    debugger;

    globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
     const objModel = { 
       StrQuery:`Sp_StockTransferMst 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
     };
     
    this.StockTransfer_Service.getData_JSON(objModel).subscribe((resp: any) => {
      //debugger;
      //console.log(resp.row);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.datasource = resp.row;
        //For List-View
        this.totalRecords =  resp.row[0].Row_Count;

        this.Prod_ListView = this.datasource.slice(first, (first + rows));
        this.tble_loading = false;

      }
      else
      {
        this.datasource = [];
        this.totalRecords =  0;
        this.tble_loading = false;
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  loadCustomers(event: LazyLoadEvent) {
    debugger;
    this.tble_loading = true;

    if(event.globalFilter!= null){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(this.datasource.length == 0){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(event.first > 500){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    
    if(this.datasource.length > 0)
    {
      this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      this.tble_loading = false;
    }
    // setTimeout(() => {
    //     if (this.datasource) {
    //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
    //         this.tble_loading = false;
    //     }
    // }, 2000);


  }

  deleteProduct_Confirm(parm) {

    Swal.fire({
      title: 'Confirm',
      text: 'Are you sure you want to delete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {

        this.deleteProduct_ListView(parm);

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })


  }

  deleteProduct_ListView(parm) {
    debugger
    const objModel: any = {
      SrNo: parm,
      IpAddress: "::1",
      objlist:[],
      CompanyId: 1,
      CreatedBy: 1,
      Command: "Delete"
    }

    this.StockTransfer_Service.getData_JSON(objModel).subscribe((resp: any) => {
      let _result = resp.ResponseCode;
      if (_result == "6") {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.SrNo !== parm);
        this.datasource = this.datasource.filter(val => val.SrNo !== parm);
        this.showToast('success', 'Successful', 'Entry Deleted.');
      }
      else {
        this.showToast('error', 'Error', 'Entry Not Deleted.');
      }
    },
      (error: AppResponse) => {

        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });


  }


// Edit
async editRecipe(parm){

  const str = `Sp_StockTransferMst 'Get_StockTransfer_Master_Details_BySrNo',${parm},'','','','',1,1`;
  const objModel = { 
    StrQuery:str, 
  };

    this.StockTransfer_Service.getData_JSON(objModel).subscribe((resp: any) => {
    debugger;
    //console.log(resp);
    let _result = resp.success;
    if (_result == "1") {
      let objResp = resp.row;
      this.IsUpdate = true;

      //setTimeout(() => {
        this.StockTransferForm.controls['SrNo'].setValue(objResp[0].SrNo);
        this.StockTransferForm.controls['Date'].setValue(objResp[0].Date);
        this.StockTransferForm.controls['FromKitchen'].setValue(objResp[0].From_Kitchen);
        this.StockTransferForm.controls['ToKitchen'].setValue(objResp[0].To_Kitchen);
        this.StockTransferForm.controls['InActive'].setValue(objResp[0].InActive == 1 ? true : false);
      //}, 1000);

      // Trx
      const str = `Sp_StockTransferMst 'Get_StockTransfer_Trx_Details_BySrNo',${parm},'','','','',1,1`;
      const objModel = { 
        StrQuery:str, 
      };

      this.StockTransfer_Service.getData_JSON(objModel).subscribe((resp_Trx: any) => {

        let _result_trx = resp_Trx.success;
        if(_result_trx== "1")
        {
          let objResp_trx = []
          objResp_trx = resp_Trx.row;

          this.products = [];

          for(let i = 0; i < objResp_trx.length; i++) {
              let item = objResp_trx[i];

              const Item: Item = {
                label: item.ItemName,
                value: item.ItemCode
              }

              const Recipe_Dropdown_Models: ST_Dropdown_Models = {
                label: item.PackUnitName,
                value: item.PackUnit
              }

              const objModel: Product = {
                Id: item.Sr,
                Sr: item.Sr,
                Item: Item,
                Qty: item.Qty,
                PackQty: Recipe_Dropdown_Models
              }

              this.products.push(objModel);
              
          }
          
          
      
            //objModel.Id = this.createId();
            //this.product.image = 'product-placeholder.svg';
            

        }
        else
        {
          this.submitted = false;
          this.Show_Error_msg('Something went wrong...Please try again later!!!');
        }
      },   
      (error: AppResponse) => {
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });


      this.tb_IsActive = true;

    }

    else {
      this.submitted = false;
      this.Show_Error_msg('Something went wrong...Please try again later!!!');
    }
  },
    (error: AppResponse) => {
      if (error.status === 400)
        this.error = "Error 400...Bad Request!!!";

      else
        this.error = error.message;
    });

  

  
};


//Trx
filterItems(event) {
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  let filtered : any[] = [];
  let query = event.query;

  for(let i = 0; i < this.items.length; i++) {
      let item = this.items[i];
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(item);
      }
  }
debugger;
  this.filteredItems = filtered;
}

filterItems_PackUnit(event) {
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  let filtered : any[] = [];
  let query = event.query;

  for(let i = 0; i < this.PackQty_List.length; i++) {
      let item = this.PackQty_List[i];
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(item);
      }
  }
debugger;
  this.filteredItems_PackUnit = filtered;
}

openNew() {
  //this.product = {};
  this.submitted = false;
  this.productDialog = true;
  this.get_PackQty_List();
}

deleteSelectedProducts() {
  this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.products = this.products.filter(val => !this.selectedProducts.includes(val));
          this.selectedProducts = null;
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
  });
}
  

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  get_Item_List(): void {

    const objModel = { StrQuery:"Sp_StockTransferMst 'Get_Item_List','','','','','',1,1", };

    this.StockTransfer_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.items = resp.row;
      }
      else
      {
        this.items = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  hideDialog() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    //setTimeout(() => this.formGroupDirective.resetForm(), 200);
    this.submitted_trx = false;
    this.productDialog = false;
  }

  saveProduct() {
    debugger;
    this.submitted_trx = true; 

    const objModel = { StrQuery:`Sp_StockTransferMst 'Get_Verify_Qty','${this.t.Item.value.value}','${this.t.Qty.value}','${this.t.PackQty.value.value}','${this.f.FromKitchen.value}','',1,1`, };
    debugger;
    this.StockTransfer_Service.getDropdownList(objModel).subscribe((resp: any) => {
    let _result = resp.success;
    if(_result== "1")
    {
      debugger;
      let VDone = resp.row[0].VDone;
      if(VDone == -4)
      {
        let Qty_AsPer_UserPackUnit = resp.row[0].Qty_AsPer_UserPackUnit;
        let PackUnit = resp.row[0].PackUnit;         
        this.Get_Verify_Qty_Alert(Qty_AsPer_UserPackUnit,PackUnit);
        this.productDialog = false;
      }
      else
      {
        if (this.t.Id.value) {
  
          const objModel: Product = {
            Id: this.t.Id.value,
            Sr: this.t.Sr.value,
            Item:this.t.Item.value,
            Qty: this.t.Qty.value,
            PackQty: this.t.PackQty.value,
          }
      
          this.products[this.findIndexById(this.t.Id.value)] = objModel;
          //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
        }
        else {
      
          const objModel: Product = {
            Id: this.t.Id.value,
            Sr: this.products.length + 1,
            Item:this.t.Item.value,
            Qty: this.t.Qty.value,
            PackQty: this.t.PackQty.value
          }
      
            objModel.Id = this.createId();
            //this.product.image = 'product-placeholder.svg';
            this.products.push(objModel);
            //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
        }
      
        this.TrxForm.reset();
        this.TrxForm.setErrors(null);
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.submitted_trx = false;
      
        this.products = [...this.products];
        this.productDialog = false;
        //this.product = {};

      }

    }
    else
    {
      this.productDialog = false;
    }
    },   
    (error: AppResponse) => {
      if(error.status === 400)
            this.error = "Error 400...Bad Request!!!";
        else
            this.error = error.message;
    });
    
  
    
    
  }
  
  Get_Verify_Qty_Alert(Qty_AsPer_UserPackUnit,PackUnit) {
    debugger;
    this.confirmationService.confirm({
        message: `${Qty_AsPer_UserPackUnit} ${PackUnit} Available only!!!`,
        header: 'Qty not available',
        icon: 'pi pi-exclamation-triangle',
        rejectVisible : false,
        acceptLabel : 'Ok'
        // accept: () => {
        //     this.products = this.products.filter(val => val.Id !== product.Id);
        //     debugger;
        //     this.product = {}
        //     //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
        // }
    });
  }
  
  findIndexById(Id: string): number {
    let index = -1;
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].Id === Id) {
            index = i;
            break;
        }
    }
  
    return index;
  }

  
createId(): string {
  let id = '';
  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for ( var i = 0; i < 5; i++ ) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return id;
}

editProduct(product: Product) {
  debugger;


  this.TrxForm.controls['Id'].setValue(product.Id);
  this.TrxForm.controls['Sr'].setValue(product.Sr);
  this.TrxForm.controls['Item'].setValue(product.Item);
  this.TrxForm.controls['Qty'].setValue(product.Qty);

  this.TrxForm.controls['PackQty'].setValue(product.PackQty);

  
  this.productDialog = true;
 
}

deleteProduct(product: Product) {
  debugger;
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete ' + product.Item.label + '?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.products = this.products.filter(val => val.Id !== product.Id);
                debugger;
                this.product = {};

                //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
            }
        });
}

//
private showToast(type: string, title: string, body: string) {
  const config = {
    status: type,
    destroyByClick: true,
    duration: 3000,
    hasIcon: true,
    position: NbGlobalPhysicalPosition.TOP_RIGHT,
    preventDuplicates: false,
  };
  const titleContent = title ? `${title}` : '';

  this.toastrService.show(
    body,
    `${titleContent}`,
    config);
}



}
