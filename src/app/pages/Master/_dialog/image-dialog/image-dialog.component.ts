import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'ngx-image-dialog',
  templateUrl: './image-dialog.component.html',
  styleUrls: ['./image-dialog.component.scss']
})
export class ImageDialogComponent {

  @Input() title: string;
  @Input() imageSrc: string;
  
  constructor(protected ref: NbDialogRef<ImageDialogComponent>) {}

  dismiss() {
    this.ref.close();
  }

}