import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { NbDialogService } from '@nebular/theme';
import { ImageDialogComponent } from '../_dialog/image-dialog/image-dialog.component';

interface Company {
  XCode: string;
  XName: string;
}

interface Country {
  XCode: string;
  XName: string;
}

interface State {
  XCode: string;
  XName: string;
}

interface City {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-hotel',
  templateUrl: './hotel.component.html',
  styleUrls: ['./hotel.component.scss']
})

export class HotelComponent implements OnInit {

  HotelForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;
  imageSrc: string = "./assets/images/image_placeholder.jpg";
  IsImg1_btn_Show = true;
  IsImg2_btn_Show = true;
  imageSrc2:string= "./assets/images/image_placeholder.jpg";
  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Companys: Company[] = [
    {XCode: '1', XName:'Test 1'},
    {XCode: '2', XName:'Test 2'},
    {XCode: '3', XName:'Test 3'},
  ]; 

  Countrys: Country[] = [
    {XCode: 'IN', XName:'India'},
    {XCode: 'US', XName:'USA'}
  ]; 

  States: State[] = [
    {XCode: 'MH', XName:'Maharashtra'},
    {XCode: 'DL', XName:'Delhi'}
  ]; 

  Citys: City[] = [
    {XCode: 'Pune', XName:'Pune'},
    {XCode: 'Mumbai', XName:'Mumbai'}
  ]; 

  constructor( 
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService
    ) { }

    ngOnInit(): void {
      this.getFormControl();
    }

  getFormControl(): void {
    this.HotelForm = this.formBuilder.group({
      Company: ['', Validators.required],
      HotelName: ['', Validators.required],
      Mobile: ['', Validators.required],
      Email: ['', Validators.required],
      Address: ['', Validators.required],
      Country: ['', Validators.required],
      State: ['', Validators.required],
      City: ['', Validators.required],
      Lat:  ['', Validators.required],
      Long: ['', Validators.required],
      Image1_Source:  [''],
      Image1_Name:  [''],
      Image2_Source:  [''],
      Image2_Name:  [''],
      InActive:[false]
    });
  }

   // convenience getter for easy access to form fields
   get f() { return this.HotelForm.controls; }

   onFileChange(event) {
    const reader = new FileReader();
    debugger;
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.HotelForm.patchValue({
        Image1_Name: event.target.files[0].name
      });
      reader.readAsDataURL(file);
    
      reader.onload = () => {

        this.IsImg1_btn_Show = false;
        
        var get_Base64String = reader.result as string;
        var strImage = get_Base64String.replace(/^data:image\/[a-z]+;base64,/, "");
        this.imageSrc = reader.result as string;
     
        this.HotelForm.patchValue({
          Image1_Source: strImage
        });
   
      };
   
    }
  }

  onFileChange2(event) {
    const reader = new FileReader();
    
    if(event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.HotelForm.patchValue({
        Image2_Name: event.target.files[0].name
      });
      reader.readAsDataURL(file);
    
      reader.onload = () => {
        this.IsImg2_btn_Show = false;
        var get_Base64String = reader.result as string;
        var strImage = get_Base64String.replace(/^data:image\/[a-z]+;base64,/, "");
        this.imageSrc2 = reader.result as string;
     
        this.HotelForm.patchValue({
          Image2_Source: strImage
        });
   
      };
   
    }
  }

  openDialog(parm) {
    if(parm == "1"){

      this.dialogService.open(ImageDialogComponent, {
        context: {
          title: 'Image Preview',
          imageSrc : this.imageSrc
        },
      });

    }else{
      this.dialogService.open(ImageDialogComponent, {
        context: {
          title: 'Image Preview',
          imageSrc :this.imageSrc2
        },
      });
    }
    
  }

  onFileClear(parm):void{

    if(parm == "1"){
      this.imageSrc ="./assets/images/image_placeholder.jpg";
      this.IsImg1_btn_Show = true;
      this.HotelForm.patchValue({
        Image1_Source: ""
      });
    }else{
      this.imageSrc2 ="./assets/images/image_placeholder.jpg";
      this.IsImg2_btn_Show = true;
      this.HotelForm.patchValue({
        Image2_Source: ""
      });
    }

  }

}
