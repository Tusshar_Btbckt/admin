import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormGroupDirective,ValidatorFn } from '@angular/forms';
import { ItemCard, ConversionTrx, ConversionUnit } from '../../../pages/_models/Master/item-card.model';
import { AppResponse } from '../../../pages/_models/app-response.model';
import { ItemCardService } from '../../../pages/_services/Master/item-card.service';
import { NbDialogService } from '@nebular/theme';
import { ImageDialogComponent } from '../_dialog/image-dialog/image-dialog.component';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-admin-entries',
  templateUrl: './item-card.component.html',
  styleUrls: ['./item-card.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService, ConfirmationService]
})


//Custome Validation
// function createCellPhoneValidator(user: FooUser): ValidatorFn {
//   return user.role === x ? Validators.required : Validators.nullValidator;
// }


  
export class ItemCardComponent implements OnInit {
  ItemCardForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;
  returnUrl: string;
  public ItemTypeList: any[];
  public MarksList: any[];
  public CategoryList: any[];
  public RecipeList: any[];
  public PackUnitList: any[];
  public CuisinesList: any[];
  products = [];
  IsWait: boolean;
  imageSrc: string = "./assets/images/image_placeholder.jpg";
  IsImg1_btn_Show = true;

  IsSucess = false;
  IsWarning = false;
  IsDanger = false;
  IsSucessMsg: string;
  IsWarningMsg: string;
  IsDangerMsg: string;

  // Trx
  productDialog: boolean;
  Products: ConversionTrx[];
  product: any;
  selectedProducts: ConversionTrx[];
  //statuses: any[];
  Item: ConversionUnit;

  //text: string;
  //selectedItem: any;
  filteredItems: any[];
  items: any[];

  TrxForm: FormGroup;
  submitted_trx = false;

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  // 
  IsShown_RawMaterial: boolean = false;
  IsShown_SemiGoods: boolean = false;
  IsShown_FinishGoods: boolean = false;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private ItemCard_Service: ItemCardService,
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private authenticationService:AuthenticationService 
  ) { }

  config: NbToastrConfig;
  currentUserData:any = {}
  
  // convenience getter for easy access to form fields
  get f() { return this.ItemCardForm.controls; }
  get t() { return this.TrxForm.controls; }

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.getTrx_FormControl();
    this.get_ItemType_List();
    this.get_Marks_List();
    this.get_Category_List();
    this.get_Recipe_List();
    this.get_PackUnit_List();
    this.get_ConversionUnit_List();
    this.get_Cuisines_List();

    this.product = {};
    this.products = [];

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  getFormControl(): void {

    this.ItemCardForm = this.formBuilder.group({
      SrNo: [''],
      Name: ['', Validators.required],
      Description: ['', Validators.required],
      ItemType: ['', Validators.required],
      Marks: ['', Validators.required],
      Category: ['', Validators.required],
      Ordering: ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Price: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Image1_Source: [''],
      Image1_Name: [''],
      //Recipe: ['', Validators.required],
      Recipe: [''],
      RecipeQty: [''],
      PackUnit: ['', Validators.required],
      Cuisines: ['', Validators.required],
      InActive: [true]
    });

    this.formControlValueChanged_ItemCardForm();

    
  }

  // formControlValueChanged_Recipe() {
  //   const Recipe = this.ItemCardForm.get('Recipe');
  //   this.ItemCardForm.get('ItemType').valueChanges.subscribe(
  //   (mode: string) => {
  //       console.log(mode);
  //       if (mode === '01ITRM') {
  //         //Recipe.get('Recipe').clearValidators();
  //         Recipe.clearValidators();
  //       }
  //       else if (mode === '01ITSM') {
  //         //Recipe.get('Recipe').setValidators(Validators.required);
  //         Recipe.setValidators([Validators.required]);
  //       }
  //       else if (mode === '01ITFG') {
  //         //Recipe.get('Recipe').setValidators(Validators.required);
  //         Recipe.setValidators([Validators.required]);
  //       }
  //       Recipe.updateValueAndValidity();
  //   });
  // }

  formControlValueChanged_ItemCardForm() {

    const Name = this.ItemCardForm.get('Name');
    const Description = this.ItemCardForm.get('Description');
    const PackUnit = this.ItemCardForm.get('PackUnit');
    const Price = this.ItemCardForm.get('Price');
    const Cuisines = this.ItemCardForm.get('Cuisines');

    const Marks = this.ItemCardForm.get('Marks');
    const Category = this.ItemCardForm.get('Category');
    const Ordering = this.ItemCardForm.get('Ordering');
    const Recipe = this.ItemCardForm.get('Recipe');
    const RecipeQty = this.ItemCardForm.get('RecipeQty');

    this.ItemCardForm.get('ItemType').valueChanges.subscribe(
    (mode: string) => {
        
      Name.setValidators([Validators.required]);
      Description.setValidators([Validators.required]);
      PackUnit.setValidators([Validators.required]);
      Price.setValidators([Validators.required]);
      Cuisines.setValidators([Validators.required]);

      switch (mode) {
        case "01ITRM":
            // Raw Meterial
            Marks.clearValidators();
            Category.clearValidators();
            Ordering.clearValidators();
            Recipe.clearValidators();
            RecipeQty.clearValidators();


            break;
        case "01ITSM":
            // Semi Goods

            //Compulsory
            Marks.setValidators([Validators.required]);
            Recipe.setValidators([Validators.required]);
            RecipeQty.setValidators([Validators.required]);

            //Not-Compulsory
            Category.clearValidators();
            Ordering.clearValidators();
            Price.clearValidators();

            break;
        case "01ITFG":
            // Finish Goods

            //Compulsory
            Marks.setValidators([Validators.required]);
            Category.setValidators([Validators.required]);
            Ordering.setValidators([Validators.required]);
            Recipe.setValidators([Validators.required]);
            RecipeQty.setValidators([Validators.required]);

            break;
        default: 
            // 
            break;
     }

    Name.updateValueAndValidity();
    Description.updateValueAndValidity();
    PackUnit.updateValueAndValidity();
    Price.updateValueAndValidity();
    Cuisines.updateValueAndValidity();

    Marks.updateValueAndValidity();
    Category.updateValueAndValidity();
    Ordering.updateValueAndValidity();
    Recipe.updateValueAndValidity();
    RecipeQty.updateValueAndValidity();   
        
        
    });

  };

  // public Recipe_Validator(ItemType: any): any {
  //   console.log(ItemType);
   
  // }
  // public readonly Recipe_Validator: ValidatorFn = c => {
  //   debugger;
  //   return this.f.ItemType.value === "01ITRM" ? Validators.required(c) : Validators.nullValidator(c);
  // }

  // public Recipe_Validator(forms: ItemCard): ValidatorFn {
  //   debugger;
  //   console.log(forms.ItemType);
  //   return forms.ItemType === "01ITRM" ? Validators.nullValidator : Validators.required;
  // }

  

 

  getTrx_FormControl(): void {
    this.TrxForm = this.formBuilder.group({
      Id: [''],
      Sr: [''],
      ConversionUnit: ['', Validators.required],
      ConversionFactor: ['', Validators.required]
    });
  }

  get_ItemType_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_ItemType_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.ItemTypeList = resp.row;
      }
      else {
        this.ItemTypeList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_Marks_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_Marks_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.MarksList = resp.row;
      }
      else {
        this.MarksList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_Category_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_Category_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.CategoryList = resp.row;
      }
      else {
        this.CategoryList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_Recipe_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_Recipe_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.RecipeList = resp.row;
      }
      else {
        this.RecipeList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_PackUnit_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_PackUnit_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.PackUnitList = resp.row;
      }
      else {
        this.PackUnitList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_ConversionUnit_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_ConversionUnit_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.success;
      if (_result == "1") {
        this.items = resp.row;
      }
      else {
        this.items = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  get_Cuisines_List(): void {

    const objModel = { StrQuery: "Sp_Get_ItemCard 'Get_Cuisines_List','','','','','',1,1", };

    this.ItemCard_Service.getDropdownList(objModel).subscribe((resp: any) => {

      let _result = resp.success;
      if (_result == "1") {
        this.CuisinesList = resp.row;
      }
      else {
        this.CuisinesList = [];
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });
  };

  onSubmit(Name) {

    debugger;
    this.IsWait = true;
    if (this.ItemCardForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }    

    const objModel: ItemCard = {
      SrNo: Name == "Update" ? this.f.SrNo.value : "0",
      Name: this.f.Name.value,
      Description: this.f.Description.value,
      ItemType: this.f.ItemType.value,
      Marks: this.f.Marks.value,
      Category: this.f.Category.value,
      Ordering: this.f.Ordering.value == "" ? 0:this.f.Ordering.value,
      Price: this.f.Price.value == "" ? 0:this.f.Price.value,
      Recipe: this.f.Recipe.value,
      RecipeQty: this.f.RecipeQty.value == "" ? 0:this.f.RecipeQty.value,
      PackUnit: this.f.PackUnit.value,
      Cuisines: this.f.Cuisines.value,
      Base64String_Image1: this.f.Image1_Source.value,
      Image1_Name: this.f.Image1_Name.value,
      IpAddress: "::1",
      IsActive: this.f.InActive.value == true ? true : false,
      IsDelete: this.f.InActive.value == false ? true : false,
      objlist: this.products.map(element => ({
        Sr: element.Sr,
        ConversionUnit: element.ConversionUnit.value,
        ConversionFactor: element.ConversionFactor
      })),
      Company: this.currentUserData.Company,
      CreatedBy: this.currentUserData.UserId,
      Command: Name
    }

    this.ItemCard_Service.Save_ItemCard(objModel).subscribe((resp: any) => {

      let _result = resp.ResponseCode;
      if (_result == "2") {
        this.IsWait = false;
        this.ItemCardForm.reset();
        this.ItemCardForm.setErrors(null);
        this.TrxForm.reset();
        this.TrxForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.onFileClear("1");
        this.ItemCardForm.controls['InActive'].setValue(true);
        this.Show_Success_msg('Data Save Successfully!!!');
      }
      else if (_result == "4") {
        this.IsWait = false;
        this.ItemCardForm.reset();
        this.ItemCardForm.setErrors(null);
        this.TrxForm.reset();
        this.TrxForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        this.onFileClear("1");
        this.ItemCardForm.controls['InActive'].setValue(true);
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else if (_result == "-6") {
        this.IsWait = false;
        this.Show_Error_msg('Server side validation error...Please try again!!!');
      }
      else if (_result == "-8") {
        this.IsWait = false;
        this.Show_Error_msg('Command Not Passed...Please try again!!!');
      }
      else if (_result == "-10") {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },
      (error: AppResponse) => {
        this.IsWait = false;
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          //this.error = error.message;
          this.Show_Error_msg('Something went wrong...Please try again later!!!');
      });
  };


  Btn_Reset() {
    this.IsUpdate = false;
    this.onFileClear("1");
    this.ItemCardForm.reset();
    this.ItemCardForm.setErrors(null);

    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    this.product = {};
    this.products = [];
  };

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      this.ItemCardForm.patchValue({
        Image1_Name: event.target.files[0].name
      });
      reader.readAsDataURL(file);

      reader.onload = () => {

        this.IsImg1_btn_Show = false;

        var get_Base64String = reader.result as string;
        var strImage = get_Base64String.replace(/^data:image\/[a-z]+;base64,/, "");
        this.imageSrc = reader.result as string;

        this.ItemCardForm.patchValue({
          Image1_Source: strImage
        });

      };

    }
  }

  onFileClear(parm): void {

    if (parm == "1") {
      this.imageSrc = "./assets/images/image_placeholder.jpg";
      this.IsImg1_btn_Show = true;

      this.ItemCardForm.controls['Image1_Source'].setValue("");
      this.ItemCardForm.controls['Image1_Name'].setValue("");
    }
  }

  Show_Success_msg(msg): void {
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg): void {
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg): void {
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(() => {                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  openDialog(parm) {
    if (parm == "1") {

      this.dialogService.open(ImageDialogComponent, {
        context: {
          title: 'Image Preview',
          imageSrc: this.imageSrc
        },
      });

    }

  }

  selectTab(ev) {
  debugger;
    if(ev.tabId == "tb_lstVw"){
 
     this.tb_IsActive = false;
     
      if(this.IsNew_Entry){
       // fetch new entry
       this.tble_loading = true;
       this.get_ListView_List("0","10","");

       setTimeout(() => {
         if (this.datasource) {
             this.Prod_ListView = this.datasource.slice(0, (0 + 10));
             this.tble_loading = false;
             this.IsNew_Entry= false;
         }
       }, 1000);
      }
    }
 
   }

  get_ListView_List(first,rows,globalFilter): void {
    debugger;

    globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
     const objModel = { 
       StrQuery:`Sp_Get_ItemCard 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
     };
     
    this.ItemCard_Service.getData_JSON(objModel).subscribe((resp: any) => {
      //debugger;
      //console.log(resp.row);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.datasource = resp.row;
        //For List-View
        this.totalRecords =  resp.row[0].Row_Count;

        this.Prod_ListView = this.datasource.slice(first, (first + rows));
        this.tble_loading = false;

      }
      else
      {
        this.datasource = [];
        this.totalRecords =  0;
        this.tble_loading = false;
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };


  loadCustomers(event: LazyLoadEvent) {
    debugger;
    this.tble_loading = true;

    if(event.globalFilter!= null){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(this.datasource.length == 0){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(event.first > 500){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    
    if(this.datasource.length > 0)
    {
      this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      this.tble_loading = false;
    }
    // setTimeout(() => {
    //     if (this.datasource) {
    //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
    //         this.tble_loading = false;
    //     }
    // }, 2000);


  }

  async editProduct(parm) {
    const str = `Sp_Get_ItemCard 'Get_ItemCard_Detail_BySrNo',${parm},'','','','',1,1`;
    const objModel = {
      StrQuery: str,
    };

    await this.ItemCard_Service.getData_JSON(objModel).subscribe((resp: any) => {
      debugger;

      let _result = resp.success;
      if (_result == "1") {
        let objResp = resp.row;
        this.IsUpdate = true;

        setTimeout(() => {

          debugger;
          if (objResp.Image1 == "Default.jpg") {
            this.IsImg1_btn_Show = true;
          }
          else {
            this.IsImg1_btn_Show = false;
            this.imageSrc = `http://api.icorrect.in/Upload/ItemCard/1/${objResp[0].Image1}`;
          }

          // this.submitted = true;

          this.ItemCardForm.controls['SrNo'].setValue(objResp[0].SrNo);
          this.ItemCardForm.controls['Name'].setValue(objResp[0].Name);
          this.ItemCardForm.controls['Description'].setValue(objResp[0].Description);
          this.ItemCardForm.controls['ItemType'].setValue(objResp[0].ItemType);
          this.ItemCardForm.controls['Marks'].setValue(objResp[0].Marks);
          this.ItemCardForm.controls['Category'].setValue(objResp[0].Category);
          this.ItemCardForm.controls['Ordering'].setValue(objResp[0].Ordering);
          this.ItemCardForm.controls['Price'].setValue(objResp[0].Price);
          this.ItemCardForm.controls['Recipe'].setValue(objResp[0].Recipe);
          this.ItemCardForm.controls['RecipeQty'].setValue(objResp[0].RecipeQty);
          this.ItemCardForm.controls['PackUnit'].setValue(objResp[0].PackUnit);
          this.ItemCardForm.controls['Cuisines'].setValue(objResp[0].Cuisines);
          this.ItemCardForm.controls['Image1_Name'].setValue(objResp[0].Image1);
          this.ItemCardForm.controls['InActive'].setValue(objResp[0].IsInActive);

        }, 1000);

        // Trx
        const str = `Sp_Get_ItemCard 'Get_ItemCard_Trx_Detail_BySrNo',${parm},'','','','',1,1`;
        const objModel = {
          StrQuery: str,
        };

        this.ItemCard_Service.getData_JSON(objModel).subscribe((resp_Trx: any) => {

          let _result_trx = resp_Trx.success;
          if (_result_trx == "1") {
            let objResp_trx = []
            objResp_trx = resp_Trx.row;

            this.products = [];

            for (let i = 0; i < objResp_trx.length; i++) {
              let item = objResp_trx[i];

              const Item: ConversionUnit = {
                label: item.ConversionUnitName,
                value: item.ConversionUnitCode
              }

              const objModel: ConversionTrx = {
                Id: item.Sr,
                Sr: item.Sr,
                ConversionUnit: Item,
                ConversionFactor: item.ConversionFactor
              }

              this.products.push(objModel);

            }

          }
          else {
            this.submitted = false;
            this.Show_Error_msg('Something went wrong...Please try again later!!!');
          }
        },
          (error: AppResponse) => {
            if (error.status === 400)
              this.error = "Error 400...Bad Request!!!";
            else
              this.error = error.message;
          });

        this.tb_IsActive = true;

      }
      else {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });




  };

  deleteProduct_Confirm(parm) {

    Swal.fire({
      title: 'Confirm',
      text: 'Are you sure you want to delete?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {

        this.deleteProduct(parm);

      } else if (result.dismiss === Swal.DismissReason.cancel) {

      }
    })


  }

  deleteProduct(parm) {
    debugger
    const objModel: any = {
      SrNo: parm,
      IpAddress: "::1",
      Company: 1,
      CreatedBy: 1,
      Command: "Delete"
    }

    this.ItemCard_Service.Save_ItemCard(objModel).subscribe((resp: any) => {
      let _result = resp.ResponseCode;
      if (_result == "6") {
        this.Prod_ListView = this.Prod_ListView.filter(val => val.SrNo !== parm);
        this.datasource = this.datasource.filter(val => val.SrNo !== parm);
        this.showToast('success', 'Successful', 'Entry Deleted.');
      }
      else {
        this.showToast('error', 'Error', 'Entry Not Deleted.');
      }
    },
      (error: AppResponse) => {
        if (error.status === 400)
          this.error = "Error 400...Bad Request!!!";
        else
          this.error = error.message;
      });


  }

  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';

    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  //Trx

  openNew() {
    this.submitted = false;
    this.productDialog = true;
  }

  deleteSelectedProducts() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.products = this.products.filter(val => !this.selectedProducts.includes(val));
        this.selectedProducts = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
      }
    });
  }


  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  hideDialog() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    this.submitted_trx = false;
    this.productDialog = false;
  }

  filterItems(event) {
    let filtered: any[] = [];
    let query = event.query;

    for (let i = 0; i < this.items.length; i++) {
      let item = this.items[i];
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(item);
      }
    }
    this.filteredItems = filtered;
  }

  saveProduct() {
    debugger;
    this.submitted_trx = true;

    if (this.t.Id.value) {

      const objModel: ConversionTrx = {
        Id: this.t.Id.value,
        Sr: this.t.Sr.value,
        ConversionUnit: this.t.ConversionUnit.value,
        ConversionFactor: this.t.ConversionFactor.value
      }

      this.products[this.findIndexById(this.t.Id.value)] = objModel;
    }
    else {

      const objModel: ConversionTrx = {
        Id: this.t.Id.value,
        Sr: this.products.length + 1,
        ConversionUnit: this.t.ConversionUnit.value,
        ConversionFactor: this.t.ConversionFactor.value
      }

      objModel.Id = this.createId();
      this.products.push(objModel);
    }

    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    this.submitted_trx = false;

    this.products = [...this.products];
    this.productDialog = false;
  }

  findIndexById(Id: string): number {
    let index = -1;
    for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].Id === Id) {
        index = i;
        break;
      }
    }

    return index;
  }

  createId(): string {
    let id = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (var i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }

  edit_Cnvrsn_Popup(product: ConversionTrx) {

    this.TrxForm.controls['Id'].setValue(product.Id);
    this.TrxForm.controls['Sr'].setValue(product.Sr);
    this.TrxForm.controls['ConversionUnit'].setValue(product.ConversionUnit);
    this.TrxForm.controls['ConversionFactor'].setValue(product.ConversionFactor);

    this.productDialog = true;

  }

  delete_Cnvrsn_Popup(product: ConversionTrx) {
    debugger;
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + product.ConversionUnit.label + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.products = this.products.filter(val => val.Id !== product.Id);
        debugger;
        this.product = {};
      }
    });
  }

}


