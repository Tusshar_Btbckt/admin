import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MasterComponent } from './master.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { VendorComponent } from './vendor/vendor.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CustomerComponent } from './customer/customer.component';
import { HotelComponent } from './hotel/hotel.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RoomComponent } from './room/room.component';

const routes: Routes = [
  {
    path: '',
    component: MasterComponent,
    children: [
      {
        path: 'item-card',
        component: ItemCardComponent,
      },
      {
        path: 'recipe',
        component: RecipeComponent,
      },
      {
        path: 'vendor',
        component: VendorComponent,
      },
      {
        path: 'room',
        component: RoomComponent,
      },
      {
        path: 'change-password',
        component: ChangePasswordComponent,
      },
      {
        path: 'customer',
        component: CustomerComponent,
      },
      {
        path: 'hotel',
        component: HotelComponent,
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
