import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-master',
  template: `
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class MasterComponent {
}

// export class MasterComponent implements OnInit {
//   constructor() { }
//   ngOnInit(): void {
//   }
// }
