import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
  NbTabsetModule
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MasterRoutingModule } from './master-routing.module';
import { MasterComponent } from '../master/master.component';
import { ItemCardComponent } from './item-card/item-card.component';
import { VendorComponent } from './vendor/vendor.component';
import { ImageDialogComponent } from './_dialog/image-dialog/image-dialog.component';
import { CustomerComponent } from './customer/customer.component';
import { HotelComponent } from './hotel/hotel.component';
import { ChangePasswordComponent } from './change-password/change-password.component';


import {BadgeModule} from 'primeng/badge';
import { RecipeComponent } from './recipe/recipe.component';

//For Table
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';

import {AutoCompleteModule} from 'primeng/autocomplete';

import {DropdownModule} from 'primeng/dropdown';
import { RoomComponent } from './room/room.component';

@NgModule({
  declarations: [
    MasterComponent,
    ItemCardComponent,
    VendorComponent,
    ImageDialogComponent,
    CustomerComponent,
    HotelComponent,
    ChangePasswordComponent,
    RecipeComponent,
    RoomComponent,
  ],
  imports: [
    CommonModule,
    MasterRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule,
    TableModule,
    NbTabsetModule,
    BadgeModule,
    DialogModule,
    ConfirmDialogModule,
    ToolbarModule,
    ButtonModule,
    AutoCompleteModule,
    DropdownModule
  ]
})
export class MasterModule { }
