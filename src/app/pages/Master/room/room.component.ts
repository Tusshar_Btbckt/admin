import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  Room,Room_Resp_Models,Room_Common_Models,Room_Dropdown_Models
} from '../../../pages/_models/Master/room.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { RoomService } from '../../../pages/_services/Master/room.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe,DatePipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
import { NbDateService } from '@nebular/theme';
// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';


@Component({
  selector: 'ngx-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})
export class RoomComponent implements OnInit {

  RoomForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  // Master
public RoomType_List: Room_Dropdown_Models[];

//For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private Room_Service: RoomService,
    private dialogService: NbDialogService,
    private toastrService: NbToastrService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     public datepipe: DatePipe,
     private authenticationService:AuthenticationService,
     protected dateService: NbDateService<Date> 
  ) { }

  currentUserData:any = {}

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.get_RoomType_List();

    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];

  }

   getFormControl(): void {
    this.RoomForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      RoomName: ['', Validators.required],
      TableNo: ['', Validators.required],
      RoomType: ['', Validators.required],
      InActive:[false]
    });
  }

  get_RoomType_List(): void {

    const objModel = { StrQuery:"Sp_Get_Room 'Get_RoomType_List','','','','','',1,1", };

    this.Room_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.RoomType_List = resp.row;
      }
      else
      {
        this.RoomType_List = [];
      }

    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  // convenience getter for easy access to form fields
  get f() { return this.RoomForm.controls; }

  onSubmit(Name){
   
   debugger;
   this.IsWait = true;
   // stop here if form is invalid
   if (this.RoomForm.invalid) {
     debugger;
     this.IsWait = false;
     return false;
   }
   debugger;

   const objModel: Room = {
     SrNo:this.f.SrNo.value,
     Date:this.datepipe.transform(this.f.Date.value, 'yyyy-MM-dd'),
     RoomName:this.f.RoomName.value,
     TableNo:this.f.TableNo.value,
     RoomType:this.f.RoomType.value,
     IpAddress:"::1",
     IsActive:this.f.InActive.value == false ? true:false,
     IsDelete:this.f.InActive.value == true ? true:false,
     CompanyId:this.currentUserData.Company,
     CreatedBy:this.currentUserData.UserId,
     Command:Name
 }


 debugger;
   this.Room_Service.Save_Room(objModel).subscribe((resp: any) => {
     console.log(resp);
     let _result = resp.ResponseCode;
     
     if(_result== "2")
     {
       this.IsWait = false;
       this.RoomForm.reset();
       this.RoomForm.setErrors(null);
       this.products = [];
       //setTimeout(() => this.formGroupDirective.resetForm(), 200);
       this.IsNew_Entry = true;
       setTimeout(() => {
        this.RoomForm.controls['InActive'].setValue(false);
        this.RoomForm.controls['Date'].setValue(this.dateService.today()); 
      }, 500);
       this.Show_Success_msg('Data Save Successfully!!!');
      
     }
     else if(_result== "4")
     {
       this.IsWait = false;
       this.RoomForm.reset();
       this.RoomForm.setErrors(null);
       this.products = [];
       this.IsNew_Entry = true;
       this.IsUpdate = false;
       setTimeout(() => {
        this.RoomForm.controls['InActive'].setValue(false);
        this.RoomForm.controls['Date'].setValue(this.dateService.today()); 
      }, 500);
       this.Show_Success_msg('Data updated Successfully!!!');
     }
     else  if(_result== "-6")
     {
       this.IsWait = false;
       this.Show_Error_msg('Something went wrong...Please try again later!!!');
     }
     else  if(_result== "-8")
     {
       this.IsWait = false;
       this.Show_Error_msg('Please check trx Data!!!');
     }
     else 
     {
       this.IsWait = false;
       this.Show_Error_msg('Something went wrong...Please try again later!!!');
     }
   },   
   (error: AppResponse) => {
     this.IsWait = false;
      if(error.status === 400)
            this.error = "Error 400...Bad Request!!!";
       else
            //this.error = error.message;
            this.Show_Error_msg('Something went wrong...Please try again later!!!');
   });
 };


 Btn_Reset(){
   this.IsUpdate = false;
   this.RoomForm.reset();
   this.RoomForm.setErrors(null);

   setTimeout(() => {
    this.RoomForm.controls['InActive'].setValue(false);
    this.RoomForm.controls['Date'].setValue(this.dateService.today()); 
  }, 500);

 };

 // Notification msg
Show_Success_msg(msg):void{
 this.IsSucessMsg = msg;
 this.IsSucess = true;
 setTimeout(()=>{                           //<<<---using ()=> syntax
   this.IsSucess = false;
   this.IsSucessMsg = "";
 }, 3000);
};

Show_Warning_msg(msg):void{
 this.IsWarningMsg = msg;
 this.IsWarning = true;
 setTimeout(()=>{                           //<<<---using ()=> syntax
   this.IsWarning = false;
   this.IsWarningMsg = "";
 }, 3000);
};

Show_Error_msg(msg):void{
 this.IsDangerMsg = msg;
 this.IsDanger = true;
 setTimeout(()=>{                           //<<<---using ()=> syntax
   this.IsDanger = false;
   this.IsDangerMsg = "";
 }, 3000);
};

// List View
selectTab(ev) {
  debugger;
    if(ev.tabId == "tb_lstVw"){
 
     this.tb_IsActive = false;
     
      if(this.IsNew_Entry){
       // fetch new entry
       this.tble_loading = true;
       this.get_ListView_List("0","10","");

       setTimeout(() => {
         if (this.datasource) {
             this.Prod_ListView = this.datasource.slice(0, (0 + 10));
             this.tble_loading = false;
             this.IsNew_Entry= false;
         }
       }, 1000);
      }
    }
 
   }

   get_ListView_List(first,rows,globalFilter): void {
    debugger;

    globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
     const objModel = { 
       StrQuery:`Sp_Get_Room 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
     };
     
    this.Room_Service.getData_JSON(objModel).subscribe((resp: any) => {
      //debugger;
      //console.log(resp.row);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.datasource = resp.row;
        //For List-View
        this.totalRecords =  resp.row[0].Row_Count;

        this.Prod_ListView = this.datasource.slice(first, (first + rows));
        this.tble_loading = false;

      }
      else
      {
        this.datasource = [];
        this.totalRecords =  0;
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  loadCustomers(event: LazyLoadEvent) {
    debugger;
    this.tble_loading = true;

    if(event.globalFilter!= null){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(this.datasource.length == 0){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(event.first > 500){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    
    if(this.datasource.length > 0)
    {
      this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      this.tble_loading = false;
    }
    // setTimeout(() => {
    //     if (this.datasource) {
    //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
    //         this.tble_loading = false;
    //     }
    // }, 2000);


  }

   // Edit
async edit_Room(parm){
  const str = `Sp_Get_Room 'Get_Room_Details_BySrNo_ForEdit',${parm},'','','','',1,1`;
  const objModel = { 
    StrQuery:str, 
  };

    await this.Room_Service.getData_JSON(objModel).subscribe((resp: any) => {
      debugger;
    //console.log(resp);
    let _result = resp.success;
    if(_result== "1")
    {
      let objResp = resp.row;
      this.IsUpdate = true;
      
      //this.get_State_List_ForEdit(objResp[0].Country,objResp[0].State);
      //this.get_City_List_ForEdit(objResp[0].Country,objResp[0].State);

      //setTimeout(() => {
      this.RoomForm.controls['SrNo'].setValue(objResp[0].SrNo);
      this.RoomForm.controls['Date'].setValue(objResp[0].Date);
      this.RoomForm.controls['RoomName'].setValue(objResp[0].RoomName);
      this.RoomForm.controls['TableNo'].setValue(objResp[0].TableNo);
      this.RoomForm.controls['RoomType'].setValue(objResp[0].RoomType);
      this.RoomForm.controls['InActive'].setValue(objResp[0].InActive == 1 ? true : false);
      //}, 1000);
     

      this.tb_IsActive = true;

    }
    else
    {
      this.submitted = false;
      this.Show_Error_msg('Something went wrong...Please try again later!!!');
    }
  },   
  (error: AppResponse) => {
     if(error.status === 400)
           this.error = "Error 400...Bad Request!!!";
      else
           this.error = error.message;
  });

 
  
};

// Delete From ListView
deleteProduct_Confirm(parm) {

  Swal.fire({
    title: 'Confirm',
    text: 'Are you sure you want to delete?',
    icon: 'warning',
    showCancelButton: true,
    confirmButtonText: 'Yes',
    cancelButtonText: 'No'
  }).then((result) => {
    if (result.isConfirmed) {

      this.Delete_Room(parm);

    } else if (result.dismiss === Swal.DismissReason.cancel) {

    }
  })


}

Delete_Room(parm) {
  debugger
  const objModel: any = {
    SrNo: parm,
    IpAddress: "::1",
    Company: 1,
    CreatedBy: 1,
    Command: "Delete"
  }

  this.Room_Service.Save_Room(objModel).subscribe((resp: any) => {
    debugger
    let _result = resp.ResponseCode;
    if (_result == "6") {
      this.Prod_ListView = this.Prod_ListView.filter(val => val.SrNo !== parm);
      this.datasource = this.datasource.filter(val => val.SrNo !== parm);
      this.showToast('success', 'Successful', 'Entry Deleted.');
    }
    else {
      this.showToast('error', 'Error', 'Entry Not Deleted.');
    }
  },
    (error: AppResponse) => {
      if (error.status === 400)
        this.error = "Error 400...Bad Request!!!";
      else
        this.error = error.message;
    });


}

private showToast(type: string, title: string, body: string) {
  const config = {
    status: type,
    destroyByClick: true,
    duration: 3000,
    hasIcon: true,
    position: NbGlobalPhysicalPosition.TOP_RIGHT,
    preventDuplicates: false,
  };
  const titleContent = title ? `${title}` : '';

  this.toastrService.show(
    body,
    `${titleContent}`,
    config);
}



}
