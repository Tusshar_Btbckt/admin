import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  Vendor,V_Resp_Models,V_Common_Models,V_Dropdown_Models
} from '../../../pages/_models/Master/vendor.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { VendorService } from '../../../pages/_services/Master/vendor.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';

import { AuthenticationService } from '../../Authentication/services/authentication.service';

interface Country {
  XCode: string;
  XName: string;
}

interface State {
  XCode: string;
  XName: string;
}

interface City {
  XCode: string;
  XName: string;
}

@Component({
  selector: 'ngx-vendor',
  templateUrl: './vendor.component.html',
  styleUrls: ['./vendor.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})

export class VendorComponent implements OnInit {

  VendorForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string;  

  // Master
  public Country_List: V_Dropdown_Models[];
  public State_List: V_Dropdown_Models[];
  public City_List: V_Dropdown_Models[];
  
  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor( 
    private formBuilder: FormBuilder,
    private Vendor_Service: VendorService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     private authenticationService:AuthenticationService 
    ) { }

    currentUserData:any = {}

    ngOnInit(): void {
      this.currentUserData =  this.authenticationService.getUserData();
      this.getFormControl();
      this.get_Country_List();

      //For List-View
      this.totalRecords = 0;
      this.tble_loading = true;
      this.datasource = [];
    }

  getFormControl(): void {
    this.VendorForm = this.formBuilder.group({
      SrNo: [''],
      Name: ['', Validators.required],
      Description: ['', Validators.required],
      Mobile:  ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Email: ['', [Validators.required, Validators.email]],
      Address: ['', Validators.required],
      Country: ['', Validators.required],
      State: ['', Validators.required],
      City: ['', Validators.required],
      InActive:[false]
    });
  }

  get_Country_List(): void {

    const objModel = { StrQuery:"Sp_Vendor_Mst 'Get_Country_List','','','','','',1,1", };

    this.Vendor_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Country_List = resp.row;
      }
      else
      {
        this.Country_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_State_List(param): void {
    this.State_List = [];
    this.City_List = [];
    this.VendorForm.controls['State'].setValue('');
    this.VendorForm.controls['City'].setValue('');

    const objModel = { StrQuery:`Sp_Vendor_Mst 'Get_State_List','${param}','','','','',1,1`, };

    this.Vendor_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.State_List = resp.row;
      }
      else
      {
        this.State_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_City_List(param2): void {
    this.City_List = [];
    this.VendorForm.controls['City'].setValue('');

    let param1 = this.f.Country.value;
    const objModel = { StrQuery:`Sp_Vendor_Mst 'Get_City_List','${param1}','${param2}','','','',1,1`, };

    this.Vendor_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.City_List = resp.row;
      }
      else
      {
        this.City_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  

   // convenience getter for easy access to form fields
   get f() { return this.VendorForm.controls; }

   onSubmit(Name){
    
    debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.VendorForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }
    debugger;
    const objModel: Vendor = {
      SrNo:this.f.SrNo.value,
      Name:this.f.Name.value,
      Description:this.f.Description.value,
      Mobile:this.f.Mobile.value,
      Email:this.f.Email.value,
      Address:this.f.Address.value,
      Country:this.f.Country.value,
      State:this.f.State.value,
      City:this.f.City.value,
      IpAddress:"::1",
      IsActive:this.f.InActive.value == false ? true:false,
      IsDelete:this.f.InActive.value == true ? true:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
  }


  debugger;
    this.Vendor_Service.Save_Vendor(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.VendorForm.reset();
        this.VendorForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.VendorForm.controls['InActive'].setValue(false);
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.VendorForm.reset();
        this.VendorForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        this.VendorForm.controls['InActive'].setValue(false);
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  };


  Btn_Reset(){
    this.IsUpdate = false;
    this.VendorForm.reset();
    this.VendorForm.setErrors(null);
    
    setTimeout(() => {
      this.VendorForm.controls['InActive'].setValue(false);
    }, 500);

  };

  // Notification msg
Show_Success_msg(msg):void{
  this.IsSucessMsg = msg;
  this.IsSucess = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsSucess = false;
    this.IsSucessMsg = "";
  }, 3000);
};

Show_Warning_msg(msg):void{
  this.IsWarningMsg = msg;
  this.IsWarning = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsWarning = false;
    this.IsWarningMsg = "";
  }, 3000);
};

Show_Error_msg(msg):void{
  this.IsDangerMsg = msg;
  this.IsDanger = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsDanger = false;
    this.IsDangerMsg = "";
  }, 3000);
};


   // List View
  selectTab(ev) {
    debugger;
      if(ev.tabId == "tb_lstVw"){
   
       this.tb_IsActive = false;
       
        if(this.IsNew_Entry){
         // fetch new entry
         this.tble_loading = true;
         this.get_ListView_List("0","10","");
  
         setTimeout(() => {
           if (this.datasource) {
               this.Prod_ListView = this.datasource.slice(0, (0 + 10));
               this.tble_loading = false;
               this.IsNew_Entry= false;
           }
         }, 1000);
        }
      }
   
     }
  
     get_ListView_List(first,rows,globalFilter): void {
      debugger;
  
      globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
       const objModel = { 
         StrQuery:`Sp_Vendor_Mst 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
       };
       
      this.Vendor_Service.getData_JSON(objModel).subscribe((resp: any) => {
        //debugger;
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          debugger;
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
  
          this.Prod_ListView = this.datasource.slice(first, (first + rows));
          this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
        }
      },   
      (error: AppResponse) => {
        debugger;
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
    };
  
    loadCustomers(event: LazyLoadEvent) {
      debugger;
      this.tble_loading = true;
  
      if(event.globalFilter!= null){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(this.datasource.length == 0){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(event.first > 500){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      
      if(this.datasource.length > 0)
      {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
      // setTimeout(() => {
      //     if (this.datasource) {
      //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      //         this.tble_loading = false;
      //     }
      // }, 2000);
  
  
    }

     // Edit
  async editPurchaseOrder(parm){
    const str = `Sp_Vendor_Mst 'Get_Vendor_Details_BySrNo',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.Vendor_Service.getData_JSON(objModel).subscribe((resp: any) => {
        debugger;
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        let objResp = resp.row;
        this.IsUpdate = true;
        
        this.get_State_List_ForEdit(objResp[0].Country,objResp[0].State);
        this.get_City_List_ForEdit(objResp[0].Country,objResp[0].State);

        setTimeout(() => {
        this.VendorForm.controls['SrNo'].setValue(objResp[0].SrNo);
        this.VendorForm.controls['Name'].setValue(objResp[0].Name);
        this.VendorForm.controls['Description'].setValue(objResp[0].Description);
        this.VendorForm.controls['Mobile'].setValue(objResp[0].Mobile);
        this.VendorForm.controls['Email'].setValue(objResp[0].Email);
        this.VendorForm.controls['Address'].setValue(objResp[0].Address);
        this.VendorForm.controls['Country'].setValue(objResp[0].Country);
        this.VendorForm.controls['State'].setValue(objResp[0].State);
        this.VendorForm.controls['City'].setValue(objResp[0].City);
        this.VendorForm.controls['InActive'].setValue(objResp[0].InActive == 1 ? true : false);
        }, 1000);
       

        this.tb_IsActive = true;

      }
      else
      {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    

    
  };

  async get_State_List_ForEdit(param,param2) {
    //this.State_List = [];
    //this.City_List = [];
    //this.VendorForm.controls['State'].setValue('');
    //this.VendorForm.controls['City'].setValue('');

    const objModel = { StrQuery:`Sp_Vendor_Mst 'Get_State_List','${param}','','','','',1,1`, };

    await this.Vendor_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.State_List = resp.row;
        this.VendorForm.controls['State'].setValue(param2);
      }
      else
      {
        this.State_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_City_List_ForEdit(param1,param2): void {
    //this.City_List = [];
    //this.VendorForm.controls['City'].setValue('');

    //let param1 = this.f.Country.value;
    const objModel = { StrQuery:`Sp_Vendor_Mst 'Get_City_List','${param1}','${param2}','','','',1,1`, };

    this.Vendor_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.City_List = resp.row;
      }
      else
      {
        this.City_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

}
