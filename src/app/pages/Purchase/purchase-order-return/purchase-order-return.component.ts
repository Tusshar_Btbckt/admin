import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { AdminEntries,AdminEntries_Models,AdminEntries_Resp_Models,AdminEntriesRes_test_Models} from '../../../pages/_models/admin/admin-entries.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { AdminEntriesService } from '../../../pages/_services/admin/admin-entries.service';
import { NbDialogService } from '@nebular/theme';

import { LocalDataSource } from 'ng2-smart-table';
import { SmartTableData } from '../../../@core/data/smart-table';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

interface Vendor {
  XCode: string;
  XName: string;
}

interface FSEntry {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  age: number;

}

@Component({
  selector: 'ngx-purchase-order-return',
  templateUrl: './purchase-order-return.component.html',
  styleUrls: ['./purchase-order-return.component.scss']
})
export class PurchaseOrderReturnComponent implements OnInit {

  POReturnForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string; 

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  Vendor_List: Vendor[] = [
    {XCode: '01', XName:'Risa Pearson'},
    {XCode: '02', XName:'Margaret D. Evans'},
    {XCode: '03', XName:'Bryan J. Luellen'},
    {XCode: '04', XName:'Kathryn S. Collier'}
  ]; 

  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      Sr: {
        title: 'Sr',
        type: 'number'
      },
      Item: {
        title: 'Item Name',
        width: '100%',
        editor: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              {value: '1', title:'Item 1'},
              {value: '2', title:'Item 2'},
              {value: '3', title:'Item 3'},
              {value: '4', title:'Item 4'},
            ],
          },
        },
        filter: {
          type: 'list',
          config: {
            selectText: 'Select',
            list: [
              {value: '1', title:'Item 1'},
              {value: '2', title:'Item 2'},
              {value: '3', title:'Item 3'},
              {value: '4', title:'Item 4'},
            ],
          },
        },
      },
      ActQty: {
        title: 'Act Qty',
        width: '100%',
        type: 'number',
      },
      Qty: {
        title: 'Qty',
        type: 'number',
      },
      Rate: {
        title: 'Rate',
        type: 'number',
      },
      Amount: {
        title: 'Amount',
        type: 'number',
      },
      Discount: {
        title: 'Discount',
        type: 'number',
      },
      NetAmount: {
        title: 'NetAmount',
        type: 'number',
      },
      
    },
  };

  source: LocalDataSource = new LocalDataSource();

  constructor(
    private formBuilder: FormBuilder,
    private dialogService: NbDialogService,
    private service: SmartTableData,
    private authenticationService:AuthenticationService 
  ) { }

  currentUserData:any = {}
  
  // convenience getter for easy access to form fields
  get f() { return this.POReturnForm.controls; }

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    // const data : FSEntry[]  = [
    //   {
    //     id: 1,firstName: 'FName 1',lastName: 'LName 1', username: 'test', email: 'asd@asd.com', age: 25
    //   },
    //   {
    //     id: 2,firstName: 'FName 2',lastName: 'LName 2', username: 'test 2', email: 'asd@asd.com', age: 30
    //   },
    // ];
    // this.source.load(data);
  }

  getFormControl(): void {
    this.POReturnForm = this.formBuilder.group({
      SrNo: ['', Validators.required],
      Date: ['', Validators.required],
      Vendor: ['', Validators.required],
      InActive:[false]
    });
  }
  
  

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }


}
