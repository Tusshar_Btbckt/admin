import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  PO,PO_Resp_Models,PORes_test_Models,PO_Common_Models,PO_Dropdown_Models
} from '../../../pages/_models/Purchase/po.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { PoService } from '../../../pages/_services/purchase/po.service';
import { NbDialogService } from '@nebular/theme';


import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
import { NbDateService } from '@nebular/theme';

interface FSEntry {
  id: number;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  age: number;
}

interface Product {
  Id: string;
  Sr: number;
  Item: any;
  PackUnit: any;
  Qty: number;
  Rate: number;
  Amount: number;
  Discount: number;
  NetAmount: number;
}

interface Item {
  label: string;
  value: string;
}

@Component({
  selector: 'ngx-purchase-order',
  templateUrl: './purchase-order.component.html',
  styleUrls: ['./purchase-order.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})
export class PurchaseOrderComponent implements OnInit {

  POForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  //products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string; 

  // Master
  public Vendor_List: PO_Dropdown_Models[];

  // Trx
  productDialog: boolean;
  products: Product[];
  product: any;
  selectedProducts: Product[];
  //submitted: boolean;
  statuses: any[];
  Item:Item;


  text: string;
  selectedItem: any;
  filteredItems: any[];
  items: any[];
  PackUnit_List: any[];
  filtered_PackUnit: any[];

  TrxForm: FormGroup;
  submitted_trx = false;

    //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

    // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

 

  constructor(
    private formBuilder: FormBuilder,
    private PO_Service: PoService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     private authenticationService:AuthenticationService,
     protected dateService: NbDateService<Date>
  ) { }

  currentUserData:any = {}

  // convenience getter for easy access to form fields
  get f() { return this.POForm.controls; }
  get t() { return this.TrxForm.controls; }

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.getFormControl();
    this.getTrx_FormControl();
    this.get_Vendor_List();
    this.get_Item_List();
    this.get_PackUnit_List();

    this.product = {};
    this.products = [];
    
    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
    

  }

  getFormControl(): void {
    this.POForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      Vendor: ['', Validators.required],
      TotalAmount: ['', Validators.required],
      TotalDiscount: ['', Validators.required],
      TotalNetAmount: ['', Validators.required],
      InActive:[false]
    });
  }

  getTrx_FormControl(): void {
    //const regexPattern = "/^-?\d*[.,]?\d{0,2}$/";
    
    this.TrxForm = this.formBuilder.group({
      Id: [''],
      Sr: [''],
      Item: ['',Validators.required],
      PackUnit: ['',Validators.required],
      Qty:  ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Rate: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Amount: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Discount:['',Validators.required],
      NetAmount:['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]]
      //InActive:[false]
    });
  }

  onSubmit(Name){
    
    debugger;
    this.IsWait = true;
    // stop here if form is invalid
    if (this.POForm.invalid) {
      debugger;
      this.IsWait = false;
      return false;
    }
    debugger;

    

    const objModel: PO = {
      SrNo:this.f.SrNo.value,
      Date:this.f.Date.value,
      Vendor:this.f.Vendor.value,
      objlist:this.products.map(element => ({ 
        Sr:element.Sr,
        ItemCode:element.Item.value,
        PackUnit:element.PackUnit.value,
        Qty:element.Qty,
        Rate:element.Rate,
        Amount:element.Amount,
        Discount:element.Discount,
        NetAmount:element.NetAmount
      })),
      Amount:this.f.TotalAmount.value,
      Discount:this.f.TotalDiscount.value,
      NetAmount:this.f.TotalNetAmount.value,
      IpAddress:"::1",
      //IsActive:this.f.InActive.value == false ? true:false,
      //IsDelete:this.f.InActive.value == true ? true:false,
      IsActive:true,
      IsDelete:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
  }


  debugger;
    this.PO_Service.Save_PurchaseOrder(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.POForm.reset();
        this.POForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        setTimeout(() => {
          this.POForm.controls['Date'].setValue(this.dateService.today());     
        }, 500); 
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.POForm.reset();
        this.POForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        setTimeout(() => {
          this.POForm.controls['Date'].setValue(this.dateService.today());     
        }, 500); 
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  }; 

  get_Vendor_List(): void {

    const objModel = { StrQuery:"Sp_PurchaseOrderMst 'Get_Vendor_List','','','','','',1,1", };

    this.PO_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Vendor_List = resp.row;
      }
      else
      {
        this.Vendor_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Item_List(): void {

    const objModel = { StrQuery:"Sp_PurchaseOrderMst 'Get_Item_List','','','','','',1,1", };

    this.PO_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.items = resp.row;
      }
      else
      {
        this.items = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_PackUnit_List(): void {

    const objModel = { StrQuery:"Sp_PurchaseOrderMst 'Get_PackUnit_List','','','','','',1,1", };

    this.PO_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.PackUnit_List = resp.row;
      }
      else
      {
        this.PackUnit_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  Btn_Reset() {
    this.IsUpdate = false;
    this.POForm.reset();
    this.POForm.setErrors(null);
    
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    this.product = {};
    this.products = [];

    
    setTimeout(() => {
      this.POForm.controls['Date'].setValue(this.dateService.today());     
    }, 500); 
  };

  // List View
  selectTab(ev) {
  debugger;
    if(ev.tabId == "tb_lstVw"){
 
     this.tb_IsActive = false;
     
      if(this.IsNew_Entry){
       // fetch new entry
       this.tble_loading = true;
       this.get_ListView_List("0","10","");

       setTimeout(() => {
         if (this.datasource) {
             this.Prod_ListView = this.datasource.slice(0, (0 + 10));
             this.tble_loading = false;
             this.IsNew_Entry= false;
         }
       }, 1000);
      }
    }
 
   }

   get_ListView_List(first,rows,globalFilter): void {
    debugger;

    globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
     const objModel = { 
       StrQuery:`Sp_PurchaseOrderMst 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
     };
     
    this.PO_Service.getData_JSON(objModel).subscribe((resp: any) => {
      //debugger;
      //console.log(resp.row);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.datasource = resp.row;
        //For List-View
        this.totalRecords =  resp.row[0].Row_Count;

        this.Prod_ListView = this.datasource.slice(first, (first + rows));
        this.tble_loading = false;

      }
      else
      {
        this.datasource = [];
        this.totalRecords =  0;
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  loadCustomers(event: LazyLoadEvent) {
    debugger;
    this.tble_loading = true;

    if(event.globalFilter!= null){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(this.datasource.length == 0){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    else if(event.first > 500){
      this.get_ListView_List(event.first,event.rows,event.globalFilter);
    }
    
    if(this.datasource.length > 0)
    {
      this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      this.tble_loading = false;
    }
    // setTimeout(() => {
    //     if (this.datasource) {
    //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
    //         this.tble_loading = false;
    //     }
    // }, 2000);


  }

  // Edit
  async editPurchaseOrder(parm){
    const str = `Sp_PurchaseOrderMst 'Get_PO_Master_Details_BySrNo',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.PO_Service.getData_JSON(objModel).subscribe((resp: any) => {
        debugger;
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        let objResp = resp.row;
        this.IsUpdate = true;

        //this.Change_XMaster_forEdit(objResp.XMaster);
        //setTimeout(() => {
          //var Amount = parseFloat(objResp[0].Amount).toFixed(2);

          this.POForm.controls['SrNo'].setValue(objResp[0].SrNo);
          this.POForm.controls['Date'].setValue(objResp[0].Date);
          this.POForm.controls['Vendor'].setValue(objResp[0].Vendor);
          this.POForm.controls['TotalAmount'].setValue(objResp[0].Amount.toFixed(2));
          this.POForm.controls['TotalDiscount'].setValue(objResp[0].Discount.toFixed(2));
          this.POForm.controls['TotalNetAmount'].setValue(objResp[0].NetAmount.toFixed(2));
          this.POForm.controls['InActive'].setValue(false);
        //}, 1000);

        // Trx
        const str = `Sp_PurchaseOrderMst 'Get_PO_Trx_Details_BySrNo',${parm},'','','','',1,1`;
        const objModel = { 
          StrQuery:str, 
        };

        this.PO_Service.getData_JSON(objModel).subscribe((resp_Trx: any) => {

          let _result_trx = resp_Trx.success;
          if(_result_trx== "1")
          {
            let objResp_trx = []
            objResp_trx = resp_Trx.row;

            this.products = [];

            for(let i = 0; i < objResp_trx.length; i++) {
                let item = objResp_trx[i];

                const Item: Item = {
                  label: item.ItemName,
                  value: item.ItemCode
                }

                const Item2: Item = {
                  label: item.PackUnitName,
                  value: item.PackUnit
                }

                const objModel: Product = {
                  Id: item.Sr,
                  Sr: item.Sr,
                  Item: Item,
                  PackUnit : Item2,
                  Qty: item.Qty,
                  Rate: item.Rate,
                  Amount:  item.Amount,
                  Discount: item.Discount,
                  NetAmount: item.NetAmount
                }

                this.products.push(objModel);
                
            }
            
            
        
              //objModel.Id = this.createId();
              //this.product.image = 'product-placeholder.svg';
              

          }
          else
          {
            this.submitted = false;
            this.Show_Error_msg('Something went wrong...Please try again later!!!');
          }
        },   
        (error: AppResponse) => {
           if(error.status === 400)
                 this.error = "Error 400...Bad Request!!!";
            else
                 this.error = error.message;
        });

        this.tb_IsActive = true;

      }
      else
      {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    

    
  };


  // Trx
  
  openNew() {
    //this.product = {};
    this.submitted = false;
    this.productDialog = true;
  }

deleteSelectedProducts() {
  this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.products = this.products.filter(val => !this.selectedProducts.includes(val));
          this.selectedProducts = null;
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
  });
}
  

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  search(event) {
    this.Vendor_List = [
      {XCode: '01', XName:'Risa Pearson'},
      {XCode: '02', XName:'Margaret D. Evans'},
      {XCode: '03', XName:'Bryan J. Luellen'},
      {XCode: '04', XName:'Kathryn S. Collier'}
    ]; 
}

filterItems(event) {
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  let filtered : any[] = [];
  let query = event.query;

  for(let i = 0; i < this.items.length; i++) {
      let item = this.items[i];
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(item);
      }
  }
debugger;
  this.filteredItems = filtered;
}

filter_PackUnit(event) {
  //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
  let filtered : any[] = [];
  let query = event.query;

  for(let i = 0; i < this.PackUnit_List.length; i++) {
      let item = this.PackUnit_List[i];
      if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
          filtered.push(item);
      }
  }
debugger;
  this.filtered_PackUnit = filtered;
}

saveProduct() {
  debugger;
  this.submitted_trx = true;  

  if (this.t.Id.value) {

    const objModel: Product = {
      Id: this.t.Id.value,
      Sr: this.t.Sr.value,
      Item:this.t.Item.value,
      PackUnit:this.t.PackUnit.value,
      Qty: this.t.Qty.value,
      Rate: this.t.Rate.value,
      Amount:  this.t.Amount.value,
      Discount: this.t.Discount.value,
      NetAmount: this.t.NetAmount.value
    }

    this.products[this.findIndexById(this.t.Id.value)] = objModel;
    //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
  }
  else {

    const objModel: Product = {
      Id: this.t.Id.value,
      Sr: this.products.length + 1,
      Item:this.t.Item.value,
      PackUnit:this.t.PackUnit.value,
      Qty: this.t.Qty.value,
      Rate: this.t.Rate.value,
      Amount:  this.t.Amount.value,
      Discount: this.t.Discount.value,
      NetAmount: this.t.NetAmount.value
    }

      objModel.Id = this.createId();
      //this.product.image = 'product-placeholder.svg';
      this.products.push(objModel);
      //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
  }

  this.TrxForm.reset();
  this.TrxForm.setErrors(null);
  //setTimeout(() => this.formGroupDirective.resetForm(), 200);
  this.submitted_trx = false;

  this.products = [...this.products];
  this.productDialog = false;
  //this.product = {};

  this.POForm.controls['TotalAmount'].setValue(this.sum_Of_Amount());
  this.POForm.controls['TotalDiscount'].setValue(this.sum_Of_Discount());
  this.POForm.controls['TotalNetAmount'].setValue(this.sum_Of_NetAmount());
  
}

// Notification msg
Show_Success_msg(msg):void{
  this.IsSucessMsg = msg;
  this.IsSucess = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsSucess = false;
    this.IsSucessMsg = "";
  }, 3000);
};

Show_Warning_msg(msg):void{
  this.IsWarningMsg = msg;
  this.IsWarning = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsWarning = false;
    this.IsWarningMsg = "";
  }, 3000);
};

Show_Error_msg(msg):void{
  this.IsDangerMsg = msg;
  this.IsDanger = true;
  setTimeout(()=>{                           //<<<---using ()=> syntax
    this.IsDanger = false;
    this.IsDangerMsg = "";
  }, 3000);
};

// Trx
findIndexById(Id: string): number {
  let index = -1;
  for (let i = 0; i < this.products.length; i++) {
      if (this.products[i].Id === Id) {
          index = i;
          break;
      }
  }

  return index;
}

createId(): string {
  let id = '';
  var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for ( var i = 0; i < 5; i++ ) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
  }
  return id;
}
hideDialog() {
  this.TrxForm.reset();
  this.TrxForm.setErrors(null);
  //setTimeout(() => this.formGroupDirective.resetForm(), 200);
  this.submitted_trx = false;
  this.productDialog = false;
}

editProduct(product: Product) {
  debugger;


  this.TrxForm.controls['Id'].setValue(product.Id);
  this.TrxForm.controls['Sr'].setValue(product.Sr);
  this.TrxForm.controls['Item'].setValue(product.Item);
  this.TrxForm.controls['PackUnit'].setValue(product.PackUnit);
  this.TrxForm.controls['Qty'].setValue(product.Qty);

  
  this.TrxForm.controls['Rate'].setValue(parseFloat(product.Rate.toString()).toFixed(2));
  this.TrxForm.controls['Amount'].setValue(parseFloat(product.Amount.toString()).toFixed(2));
  this.TrxForm.controls['Discount'].setValue(parseFloat(product.Discount.toString()).toFixed(2));
  this.TrxForm.controls['NetAmount'].setValue(parseFloat(product.NetAmount.toString()).toFixed(2));
  
  this.productDialog = true;
 
}

deleteProduct(product: Product) {
  debugger;
        this.confirmationService.confirm({
            message: 'Are you sure you want to delete ' + product.Item.label + '?',
            header: 'Confirm',
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
                this.products = this.products.filter(val => val.Id !== product.Id);
                debugger;
                this.product = {};

                this.POForm.controls['TotalAmount'].setValue(this.sum_Of_Amount());
                this.POForm.controls['TotalDiscount'].setValue(this.sum_Of_Discount());
                this.POForm.controls['TotalNetAmount'].setValue(this.sum_Of_NetAmount());
                //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
            }
        });
}

// Calculate Amount
// Cal_Amount(product: Product){

// }

sum_Of_Amount() {
  let sum: number = 0;
  this.products.forEach(a => sum += parseFloat(a.Amount.toString()));
  var sum1 = parseFloat(sum.toString()).toFixed(2);
  return sum1;
}
sum_Of_Discount() {
  let sum: number = 0;
  this.products.forEach(a => sum += parseFloat(a.Discount.toString()));
  var sum1 = parseFloat(sum.toString()).toFixed(2);
  return sum1;
}
sum_Of_NetAmount() {
  let sum: number = 0;
  this.products.forEach(a => sum += parseFloat(a.NetAmount.toString()));
  var sum1 = parseFloat(sum.toString()).toFixed(2);
  return sum1;
}

Qty_onKeyUp(x) {

  this.fun_Cal_NetAmount();
}

fun_Cal_NetAmount(){

  //let myDecimal: number = 17.5;
  let Amount: number = 0;
  let NetAmount: number;
  Amount = this.t.Qty.value * this.t.Rate.value;
  if(this.t.Discount.value){
    NetAmount = Amount - parseFloat(this.t.Discount.value);
  }
  else{
    NetAmount = Amount;
  }
  
  let Cnvrt_Amount = parseFloat(Amount.toString()).toFixed(2);
  let Cnvrt_NetAmount = parseFloat(NetAmount.toString()).toFixed(2);

  this.TrxForm.patchValue({
    //Rate : val.Rate,
    Amount : Cnvrt_Amount,
    NetAmount: Cnvrt_NetAmount,
  }, {emitEvent:false});

  //val.Amount = parseFloat(val.Amount).toFixed(2);
  

  // //let net_amount: string;
  // if (val.Discount) {
  //   val.NetAmount = parseFloat(val.Amount) - parseFloat(val.Discount);
  //   val.NetAmount = parseFloat(val.NetAmount);

  // }else{
  //   val.NetAmount = parseFloat(val.Amount);
  //   val.NetAmount = parseFloat(val.NetAmount);
  // }

  // val.Amount = parseFloat(val.Amount).toFixed(2);
  // val.NetAmount = parseFloat(val.NetAmount).toFixed(2);
  // //val.Rate = parseFloat(val.Rate).toFixed(2);

  // this.TrxForm.patchValue({
  //   //Rate : val.Rate,
  //   Amount : val.Amount,
  //   NetAmount: val.NetAmount,
  // }, {emitEvent:false});

  // this.TrxForm.patchValue({
  //   //Rate : val.Rate,
  //   Amount : val.Amount,
  //   NetAmount: val.NetAmount,
  // }, {emitEvent:false});
}


}
