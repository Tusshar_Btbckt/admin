import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PurchaseComponent } from './purchase.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';
import { PurchaseOrderAcceptanceComponent } from './purchase-order-acceptance/purchase-order-acceptance.component';
import { PurchaseOrderReturnComponent } from './purchase-order-return/purchase-order-return.component';



const routes: Routes = [
  {
    path: '',
    component: PurchaseComponent,
    children: [
      {
        path: 'purchase-order',
        component: PurchaseOrderComponent,
      },
      {
        path: 'purchase-order-acceptance',
        component: PurchaseOrderAcceptanceComponent,
      },
      {
        path: 'purchase-order-return',
        component: PurchaseOrderReturnComponent,
      }
    ],
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseRoutingModule { }
