import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-purchase',
  template:`
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class PurchaseComponent {
}
