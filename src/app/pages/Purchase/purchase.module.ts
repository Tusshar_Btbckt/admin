import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
  NbTabsetModule,
  NbBadgeModule
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { PurchaseRoutingModule } from './purchase-routing.module';
import { PurchaseComponent } from '../purchase/purchase.component';
import { PurchaseOrderComponent } from './purchase-order/purchase-order.component';

// For Table
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { PurchaseOrderReturnComponent } from './purchase-order-return/purchase-order-return.component';

//For Table
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';

import {AutoCompleteModule} from 'primeng/autocomplete';
import { PurchaseOrderAcceptanceComponent } from './purchase-order-acceptance/purchase-order-acceptance.component';

@NgModule({
  declarations: [
    PurchaseComponent,
    PurchaseOrderComponent,
    PurchaseOrderReturnComponent,
    PurchaseOrderAcceptanceComponent
  ],
  imports: [
    CommonModule,
    PurchaseRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule,
    Ng2SmartTableModule,
    TableModule,
    DialogModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    NbTabsetModule,
    NbBadgeModule,
    ToolbarModule,
    ButtonModule
  ]
})
export class PurchaseModule { }
