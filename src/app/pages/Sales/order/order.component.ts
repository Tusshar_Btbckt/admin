import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  Order,Order_Resp_Models,OrderRes_test_Models,Order_Common_Models,Order_Dropdown_Models,Order_Dropdown2_Models
} from '../../../pages/_models/Sales/order.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { OrderService } from '../../../pages/_services/Sales/order.service';
import { NbDialogService } from '@nebular/theme';


import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

import { NbDateService } from '@nebular/theme';

interface Product {
  Id: string;
  Sr: number;
  Item: any;
  Qty: number;
  Rate: number;
  Amount: number;
  Instruction: any;
  Remark: string;
}

interface Item {
  label: string;
  value: string;
}

@Component({
  selector: 'ngx-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService,CurrencyPipe]
})
export class OrderComponent implements OnInit {

  OrderForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  //products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string; 

  // Master
  public Room_List: Order_Dropdown_Models[];
  public Room_TableNo_List: Order_Dropdown_Models[];
  public Customer_List: Order_Dropdown_Models[];

  // Trx
  productDialog: boolean;
  products: Product[];
  product: any;
  selectedProducts: Product[];
  //submitted: boolean;
  statuses: any[];
  Item:Item;

  // Master
  public Instruction_List: Order_Dropdown2_Models[];



  text: string;
  selectedItem: any;
  filteredItems: any[];
  items: any[];

  filtered_Instruction_List: any[];

  TrxForm: FormGroup;
  submitted_trx = false;

    //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

    // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private Order_Service: OrderService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     private authenticationService:AuthenticationService,
     protected dateService: NbDateService<Date>
  ) { }

  currentUserData:any = {}

  // convenience getter for easy access to form fields
  get f() { return this.OrderForm.controls; }
  get t() { return this.TrxForm.controls; }

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    
    this.getFormControl();
    this.getTrx_FormControl();
    this.get_Room_List();
    this.get_Customer_List();
    this.get_Item_List();
    this.get_Instruction_List();

    this.product = {};
    this.products = [];
    
    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
    

  }
  
  getFormControl(): void {
    this.OrderForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      Room: ['', Validators.required],
      TableNo: ['', Validators.required],
      Customer: ['', Validators.required],
      Person: ['0', Validators.required],
      NetAmount: ['0.00', Validators.required],
      InActive:[false]
    });
  }

  getTrx_FormControl(): void {
    //const regexPattern = "/^-?\d*[.,]?\d{0,2}$/";
    
    this.TrxForm = this.formBuilder.group({
      Id: [''],
      Sr: [''],
      Item: ['',Validators.required],
      PackUnit: [''],
      Qty:  ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Rate: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Amount: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Instruction: [''],
      Remark:['']
    });
  }

  onSubmit(Name){
    
    
    this.IsWait = true;
    // stop here if form is invalid
    if (this.OrderForm.invalid) {
      
      this.IsWait = false;
      return false;
    }
    

    

    const objModel: Order = {
      SrNo:Name == "Save"?"1":this.f.SrNo.value,
      Date:this.f.Date.value,
      RoomSrNo:this.f.Room.value,
      TableNo:this.f.TableNo.value,
      Customer:this.f.Customer.value,
      objlist:this.products.map(element => ({ 
        Sr:element.Sr,
        ItemCode:element.Item.value,
        PackUnit:'',
        Qty:element.Qty,
        Rate:element.Rate,
        Amount:element.Amount,
        Instruction:element.Instruction.value,
        Remark : element.Remark
      })),
      NetAmount:this.f.NetAmount.value,
      Person :this.f.Person.value,
      IpAddress:"::1",
      IsActive:this.f.InActive.value == false ? true:false,
      IsDelete:this.f.InActive.value == true ? true:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
  }


  
    this.Order_Service.Save_PurchaseOrder(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.OrderForm.reset();
        this.OrderForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.OrderForm.reset();
        this.OrderForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        this.IsUpdate = false;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  };

  get_Room_List(): void {

    const objModel = { StrQuery:"Sp_Get_Order 'Get_Room_List','','','','','',1,1", };

    this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Room_List = resp.row;
      }
      else
      {
        this.Room_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Room_TableNo_List(param): void {
    
    this.Room_TableNo_List = [];
    this.OrderForm.controls['TableNo'].setValue('');

    const objModel = { StrQuery:`Sp_Get_Order 'Get_Room_TableNo_List','${param}','','','','',1,1`, };

    this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
      
      let _result = resp.success;
      if(_result== "1")
      {
        this.Room_TableNo_List = resp.row;
      }
      else
      {
        this.Room_TableNo_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Customer_List(): void {

    const objModel = { StrQuery:"Sp_Get_Order 'Get_Customer_List','','','','','',1,1", };

    this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Customer_List = resp.row;
      }
      else
      {
        this.Customer_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Item_List(): void {

    const objModel = { StrQuery:"Sp_Get_Order 'Get_Item_List','','','','','',1,1", };

    this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.items = resp.row;
      }
      else
      {
        this.items = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Instruction_List(): void {

    const objModel = { StrQuery:"Sp_Get_Order 'Get_Instruction_List','','','','','',1,1", };

    this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Instruction_List = resp.row;
      }
      else
      {
        this.Instruction_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  Get_ItemPrice_ByItem(param): void {
    
    this.TrxForm.controls['Rate'].setValue(parseFloat("0").toFixed(2));

    const objModel = { StrQuery:`Sp_Get_Order 'Get_ItemPrice_ByItem','${param.value}','','','','',1,1`, };

    this.Order_Service.getData_JSON(objModel).subscribe((resp: any) => {
      
      let _result = resp.success;
      if(_result== "1")
      {

        let Qty: number = 1;
        let Rate: number = parseFloat(resp.row[0].Price);
        let Amount:number = Qty * Rate;
        let Cnvrt_Amount = parseFloat(Amount.toString()).toFixed(2);

        this.TrxForm.controls['Rate'].setValue(parseFloat(Rate.toString()).toFixed(2));
        this.TrxForm.controls['Qty'].setValue(Qty);
        this.TrxForm.controls['Amount'].setValue(parseFloat(Cnvrt_Amount.toString()).toFixed(2));


      }
      else
      {
        let Qty: number = 1;
        let Rate: number = parseFloat("0");
        let Amount:number = Qty * Rate;
        let Cnvrt_Amount = parseFloat(Amount.toString()).toFixed(2);

        this.TrxForm.controls['Rate'].setValue(parseFloat(Rate.toString()).toFixed(2));
        this.TrxForm.controls['Qty'].setValue(Qty);
        this.TrxForm.controls['Amount'].setValue(parseFloat(Cnvrt_Amount.toString()).toFixed(2));
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  // Reset Form
  ResetForm() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);

    this.OrderForm.reset();
    this.OrderForm.setErrors(null);

    //setTimeout(() => this.formGroupDirective.resetForm(), 200);
    this.product = {};
    this.products = [];
    this.IsUpdate = false;

    setTimeout(() => {
      this.OrderForm.controls['Date'].setValue(this.dateService.today());
      this.OrderForm.controls['NetAmount'].setValue('0.00');      
    }, 500);   

    
  }

  // List View
  selectTab(ev) {
    
      if(ev.tabId == "tb_lstVw"){
   
       this.tb_IsActive = false;
       
        if(this.IsNew_Entry){
         // fetch new entry
         this.tble_loading = true;
         this.get_ListView_List("0","10","");
  
         setTimeout(() => {
           if (this.datasource) {
               this.Prod_ListView = this.datasource.slice(0, (0 + 10));
               this.tble_loading = false;
               this.IsNew_Entry= false;
           }
         }, 1000);
        }
      }
   
  }

  get_ListView_List(first,rows,globalFilter): void {
      
  
      globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
       const objModel = { 
         StrQuery:`Sp_Get_Order 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
       };
       
      this.Order_Service.getData_JSON(objModel).subscribe((resp: any) => {
        //
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
  
          this.Prod_ListView = this.datasource.slice(first, (first + rows));
          this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
        }
      },   
      (error: AppResponse) => {
        
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
  };
  
  loadCustomers(event: LazyLoadEvent) {
      
      this.tble_loading = true;
  
      if(event.globalFilter!= null){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(this.datasource.length == 0){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(event.first > 500){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      
      if(this.datasource.length > 0)
      {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
      // setTimeout(() => {
      //     if (this.datasource) {
      //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      //         this.tble_loading = false;
      //     }
      // }, 2000);
  
  
  }

  // Edit
  async editPurchaseOrder(parm){
    debugger;
    const str = `Sp_Get_Order 'Get_Master_Details_BySrNo_ForEdit',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.Order_Service.getData_JSON(objModel).subscribe((resp: any) => {
        
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        let objResp = resp.row;
        this.IsUpdate = true;

        //this.Change_XMaster_forEdit(objResp.XMaster);
        //setTimeout(() => {
          //var Amount = parseFloat(objResp[0].Amount).toFixed(2);

          

        //}, 1000);

        // Trx
        const str = `Sp_Get_Order 'Get_Trx_Details_BySrNo_ForEdit',${parm},'','','','',1,1`;
        const objModel = { 
          StrQuery:str, 
        };

        this.Order_Service.getData_JSON(objModel).subscribe((resp_Trx: any) => {

          let _result_trx = resp_Trx.success;
          if(_result_trx== "1")
          {

            // ********** Trx
            let objResp_trx = []
            objResp_trx = resp_Trx.row;

            this.products = [];

            for(let i = 0; i < objResp_trx.length; i++) {
                let item = objResp_trx[i];

                const Item: Item = {
                  label: item.ItemName,
                  value: item.ItemCode
                }

                const Item_Instruction: Item = {
                  label: item.InstructionName,
                  value: item.Instruction
                }

                const objModel: Product = {
                  Id: item.Sr,
                  Sr: item.Sr,
                  Item: Item,
                  Qty: item.Qty,
                  Rate: item.Rate,
                  Amount:  item.Amount,
                  Instruction: Item_Instruction,
                  Remark:  item.Remark
                }
                

                this.products.push(objModel);
                
            }
            
            // ******* Table No List
            this.Room_TableNo_List = [];
            this.OrderForm.controls['TableNo'].setValue('');

            const objModel = { StrQuery:`Sp_Get_Order 'Get_Room_TableNo_List','${objResp[0].RoomSrNo}','','','','',1,1`, };

            this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
              
              let _result = resp.success;
              if(_result== "1")
              {
                this.Room_TableNo_List = resp.row;
                debugger;
                // ************** Instruction List
                const objModel = { StrQuery:"Sp_Get_Order 'Get_Instruction_List','','','','','',1,1", };

                this.Order_Service.getDropdownList(objModel).subscribe((resp: any) => {
                  let _result = resp.success;
                  if(_result== "1")
                  {
                    this.Instruction_List = resp.row;
                    debugger;
                    // ****************** Master Data
                    setTimeout(() => {
                      this.OrderForm.controls['SrNo'].setValue(objResp[0].SrNo);
                      this.OrderForm.controls['Date'].setValue(objResp[0].Date);
                      this.OrderForm.controls['Room'].setValue(objResp[0].RoomSrNo);
                      this.OrderForm.controls['TableNo'].setValue(objResp[0].TableNo);
                      this.OrderForm.controls['Customer'].setValue(objResp[0].Customer);
                      this.OrderForm.controls['NetAmount'].setValue(objResp[0].NetAmount);
                      this.OrderForm.controls['Person'].setValue(objResp[0].Person);                
                      this.OrderForm.controls['InActive'].setValue(false);
                      }, 1500);

                  }
                  else
                  {
                    this.Instruction_List = [];
                  }
                },   
                (error: AppResponse) => {
                  if(error.status === 400)
                        this.error = "Error 400...Bad Request!!!";
                    else
                        this.error = error.message;
                });



                

              }
              else
              {
                this.Room_TableNo_List = [];
              }
            },   
            (error: AppResponse) => {
              if(error.status === 400)
                    this.error = "Error 400...Bad Request!!!";
                else
                    this.error = error.message;
            });
            
        
              

          }
          else
          {
            this.submitted = false;
            this.Show_Error_msg('Something went wrong...Please try again later!!!');
          }
        },   
        (error: AppResponse) => {
           if(error.status === 400)
                 this.error = "Error 400...Bad Request!!!";
            else
                 this.error = error.message;
        });

        this.tb_IsActive = true;

      }
      else
      {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    

    
  };

   // Trx
  
  openNew() {
    //this.product = {};
    this.submitted = false;
    this.productDialog = true;
  }

  deleteSelectedProducts() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.products = this.products.filter(val => !this.selectedProducts.includes(val));
          this.selectedProducts = null;
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
    });
  }
  

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  // search(event) {
  //   this.Vendor_List = [
  //     {XCode: '01', XName:'Risa Pearson'},
  //     {XCode: '02', XName:'Margaret D. Evans'},
  //     {XCode: '03', XName:'Bryan J. Luellen'},
  //     {XCode: '04', XName:'Kathryn S. Collier'}
  //   ]; 
  // }

  filterItems(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;

    for(let i = 0; i < this.items.length; i++) {
        let item = this.items[i];
        if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(item);
        }
    }
    this.filteredItems = filtered;
  }

  filterItems_Instruction(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;
  
    for(let i = 0; i < this.Instruction_List.length; i++) {
        let item = this.Instruction_List[i];
        if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(item);
        }
    }
  
    this.filtered_Instruction_List = filtered;
  }

  saveProduct() {
  
  this.submitted_trx = true;

  

  if (this.t.Id.value) {

    const objModel: Product = {
      Id: this.t.Id.value,
      Sr: this.t.Sr.value,
      Item:this.t.Item.value,
      Qty: this.t.Qty.value,
      Rate: this.t.Rate.value,
      Amount:  this.t.Amount.value,
      Instruction: this.t.Instruction.value,
      Remark: this.t.Remark.value,
    }

    this.products[this.findIndexById(this.t.Id.value)] = objModel;
    //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Updated', life: 3000});
  }
  else {

    const objModel: Product = {
      Id: this.t.Id.value,
      Sr: this.products.length + 1,
      Item:this.t.Item.value,
      Qty: this.t.Qty.value,
      Rate: this.t.Rate.value,
      Amount:  this.t.Amount.value,
      Instruction: this.t.Instruction.value,
      Remark: this.t.Remark.value,
    }

      objModel.Id = this.createId();
      //this.product.image = 'product-placeholder.svg';
      this.products.push(objModel);
      //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Created', life: 3000});
  }

  this.OrderForm.controls['NetAmount'].setValue(this.sum_Of_NetAmount());

  this.TrxForm.reset();
  this.TrxForm.setErrors(null);
  //setTimeout(() => this.formGroupDirective.resetForm(), 200);
  this.submitted_trx = false;

  this.products = [...this.products];
  this.productDialog = false;
  //this.product = {};

  
  }

  // Notification msg
  Show_Success_msg(msg):void{
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg):void{
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg):void{
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  // Trx
  findIndexById(Id: string): number {
    let index = -1;
    for (let i = 0; i < this.products.length; i++) {
        if (this.products[i].Id === Id) {
            index = i;
            break;
        }
    }

    return index;
  }

  createId(): string {
    let id = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for ( var i = 0; i < 5; i++ ) {
        id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }
  hideDialog() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    //setTimeout(() => this.formGroupDirective.resetForm(), 200);
    this.submitted_trx = false;
    this.productDialog = false;
  }

  editProduct(product: Product) {
    

    this.TrxForm.controls['Id'].setValue(product.Id);
    this.TrxForm.controls['Sr'].setValue(product.Sr);
    this.TrxForm.controls['Item'].setValue(product.Item);
    this.TrxForm.controls['Qty'].setValue(product.Qty);
    
    this.TrxForm.controls['Rate'].setValue(parseFloat(product.Rate.toString()).toFixed(2));
    this.TrxForm.controls['Amount'].setValue(parseFloat(product.Amount.toString()).toFixed(2));
    this.TrxForm.controls['Instruction'].setValue(product.Instruction);
    this.TrxForm.controls['Remark'].setValue(product.Remark);
    
    this.productDialog = true;
  
  }

  deleteProduct(product: Product) {
    
    this.confirmationService.confirm({
        message: 'Are you sure you want to delete ' + product.Item.label + '?',
        header: 'Confirm',
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
            this.products = this.products.filter(val => val.Id !== product.Id);
            
            this.product = {};
            this.OrderForm.controls['NetAmount'].setValue(this.sum_Of_NetAmount());
            //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
        }
    });
  }

  Qty_onKeyUp(x) {

    this.fun_Cal_NetAmount();
  }
  
  fun_Cal_NetAmount(){
  
    //let myDecimal: number = 17.5;
    let Amount: number = 0;
    let NetAmount: number;
    Amount = this.t.Qty.value * this.t.Rate.value;
    // if(this.t.Discount.value){
    //   NetAmount = Amount - parseFloat(this.t.Discount.value);
    // }
    // else{
    //   NetAmount = Amount;
    // }
    
    let Cnvrt_Amount = parseFloat(Amount.toString()).toFixed(2);
    //let Cnvrt_NetAmount = parseFloat(NetAmount.toString()).toFixed(2);
  
    this.TrxForm.patchValue({
      //Rate : val.Rate,
      Amount : Cnvrt_Amount,
      //NetAmount: Cnvrt_NetAmount,
    }, {emitEvent:false});
  
  }

  sum_Of_NetAmount() {
    let sum: number = 0;
    this.products.forEach(a => sum += parseFloat(a.Amount.toString()));
    var sum1 = parseFloat(sum.toString()).toFixed(2);
    return sum1;
  }


}
