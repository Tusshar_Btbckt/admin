import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import {FormBuilder,FormControl,FormGroup, Validators,FormGroupDirective} from '@angular/forms';
import { 
  POS,POS_Resp_Models,POSRes_test_Models,POS_Common_Models,POS_Dropdown_Models,POS_Trx_Models,POS_Dropdown2_Models
} from '../../../pages/_models/Sales/pos.model';
import { AppResponse} from '../../../pages/_models/app-response.model';
import { POSService } from '../../../pages/_services/Sales/pos.service';
import { NbDialogService } from '@nebular/theme';


import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

import{CurrencyPipe} from '@angular/common';

import { LazyLoadEvent } from 'primeng/api';
import Swal from 'sweetalert2/dist/sweetalert2.js';
import { AuthenticationService } from '../../Authentication/services/authentication.service';
import { DomSanitizer } from '@angular/platform-browser';

import { NbDateService } from '@nebular/theme';

interface Product {
  Id: number;
  Sr: number;
  Item: any;
  Qty: number;
  Rate: number;
  Amount: number;
  NetAmount: number;
}


@Component({
  selector: 'ngx-pos',
  templateUrl: './pos.component.html',
  styleUrls: ['./pos.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
    providers: [MessageService,ConfirmationService,CurrencyPipe]
})
export class POSComponent implements OnInit {

  POSForm: FormGroup;
  loading = false;
  submitted = false;
  error: string;
  SuccessMsg: string;  
  returnUrl: string; 
  //products = [];
  IsWait:boolean;

  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string; 

  // Master
  public Room_List: POS_Dropdown_Models[];
  public Room_TableNo_List: POS_Dropdown_Models[];
  public Customer_List: POS_Dropdown_Models[];
  public MOP_List: POS_Dropdown_Models[];
  

  // Trx
  productDialog: boolean;
  products: Product[];
  product: any;
  selectedProducts: Product[];
  //submitted: boolean;
  statuses: any[];
  Item:POS_Dropdown2_Models;


  text: string;
  selectedItem: any;
  filteredItems: any[];
  items: any[];

  TrxForm: FormGroup;
  submitted_trx = false;

    //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

    // For Update
  IsUpdate: boolean = false;

  // For Tab
  tb_IsActive: boolean = true;
  PrintString: any;

  @ViewChild(FormGroupDirective) formGroupDirective: FormGroupDirective;

  constructor(
    private formBuilder: FormBuilder,
    private POS_Service: POSService,
    private dialogService: NbDialogService,
    private messageService: MessageService,
     private confirmationService: ConfirmationService,
     private currencyPipe : CurrencyPipe,
     private authenticationService:AuthenticationService,
     private sanitizer: DomSanitizer,
     protected dateService: NbDateService<Date>
  ) { }

  currentUserData:any = {}

  // convenience getter for easy access to form fields
  get f() { return this.POSForm.controls; }
  get t() { return this.TrxForm.controls; }

  ngOnInit(): void {

    this.currentUserData =  this.authenticationService.getUserData();
    
    this.getFormControl();
    this.getTrx_FormControl();
    this.get_Room_List();
    this.get_Customer_List();
    this.get_Item_List();
    this.get_MOP_List();

    this.product = {};
    this.products = [];
    
    //For List-View
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  getFormControl(): void {
    this.POSForm = this.formBuilder.group({
      SrNo: [''],
      Date: [this.dateService.today(), Validators.required],
      Room: ['', Validators.required],
      TableNo: ['', Validators.required],
      Customer: ['', Validators.required],
      Discount:  ['0.00', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      TotalAmount:  ['0.00', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      MOP: ['', Validators.required],
      InActive:[false]
    });
  }

  getTrx_FormControl(): void {
    //const regexPattern = "/^-?\d*[.,]?\d{0,2}$/";
    
    this.TrxForm = this.formBuilder.group({
      Id: [''],
      Sr: [''],
      Item: ['',Validators.required],
      PackUnit: [''],
      Qty:  ['', [Validators.required, Validators.pattern("^[0-9]*$")]],
      Rate: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]],
      Amount: ['', [Validators.required, Validators.pattern("^[0-9]+(.[0-9]{0,2})?$")]]
    });
  }

  get_Room_List(): void {

    const objModel = { StrQuery:"Sp_Get_POS 'Get_Room_List','','','','','',1,1", };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Room_List = resp.row;
      }
      else
      {
        this.Room_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_MOP_List(): void {

    const objModel = { StrQuery:"Sp_Get_POS 'Get_MOP_List','','','','','',1,1", };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.MOP_List = resp.row;
      }
      else
      {
        this.MOP_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Room_TableNo_List(param): void {
    
    this.Room_TableNo_List = [];
    this.POSForm.controls['TableNo'].setValue('');

    const objModel = { StrQuery:`Sp_Get_POS 'Get_Room_TableNo_List','${param}','','','','',1,1`, };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      
      let _result = resp.success;
      if(_result== "1")
      {
        this.Room_TableNo_List = resp.row;
      }
      else
      {
        this.Room_TableNo_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  async get_Room_TableNo_List_Edit(param,param2) {
    
    this.Room_TableNo_List = [];
    

    const objModel = { StrQuery:`Sp_Get_POS 'Get_Room_TableNo_List','${param}','','','','',1,1`, };

    await this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.Room_TableNo_List = resp.row;
        this.POSForm.controls['TableNo'].setValue(param2);
      }
      else
      {
        this.Room_TableNo_List = [];
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Order_Item_List_ByRoom_TableNo(param): void {
    
    let param2 = this.f.Room.value;

    const objModel = { StrQuery:`Sp_Get_POS 'Get_Order_List_ByRoom_TableNo','${param2}','${param}','','','',1,1`, };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
     
      let _result = resp.success;
      if(_result== "1")
      {
        //this.products = resp.row;        

        this.products = resp.row.map(element => ({ 
          Id: element.Rnk,
          Sr: element.Sr,
          Item: {
            label: element.ItemName,
            value: element.ItemCode
          },
          Qty: element.Qty,
          Rate: element.Rate,
          Amount: element.Amount,
          NetAmount: element.NetAmount
        }));

        this.Get_Customer_ByRoom_TableNo(param);
      }
      else
      {
        this.products =[];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  Get_Customer_ByRoom_TableNo(param): void {

    
    //this.Room_TableNo_List = [];

    let param2 = this.f.Room.value;

    const objModel = { StrQuery:`Sp_Get_POS 'Get_Customer_ByRoom_TableNo','${param2}','${param}','','','',1,1`, };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        let Customer = resp.row[0].Customer;
        this.POSForm.controls['TotalAmount'].setValue(this.sum_Of_NetAmount());
        this.POSForm.controls['Customer'].setValue(Customer);
      }
      else
      {
        this.POSForm.controls['TotalAmount'].setValue(0);
        this.POSForm.controls['Customer'].setValue('');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  async Get_Customer_ByRoom_TableNo_Edit(param,param2){

    
    //this.Room_TableNo_List = [];

    //let param2 = this.f.Room.value;

    const objModel = { StrQuery:`Sp_Get_POS 'Get_Customer_ByRoom_TableNo','${param2}','${param}','','','',1,1`, };

    await this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      debugger;
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        let Customer = resp.row[0].Customer;
        //this.POSForm.controls['TotalAmount'].setValue(this.sum_Of_NetAmount());
        this.POSForm.controls['Customer'].setValue(Customer);
      }
      else
      {
        this.POSForm.controls['TotalAmount'].setValue(0);
        this.POSForm.controls['Customer'].setValue('');
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Customer_List(): void {

    const objModel = { StrQuery:"Sp_Get_POS 'Get_Customer_List','','','','','',1,1", };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Customer_List = resp.row;
      }
      else
      {
        this.Customer_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  get_Item_List(): void {

    const objModel = { StrQuery:"Sp_Get_POS 'Get_Item_List','','','','','',1,1", };

    this.POS_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.items = resp.row;
      }
      else
      {
        this.items = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  onSubmit(Name){
    
    
    this.IsWait = true;
    // stop here if form is invalid
    if (this.POSForm.invalid) {
      
      this.IsWait = false;
      return false;
    }
    

    

    const objModel: POS = {
      SrNo:Name == "Save"?"1":this.f.SrNo.value,
      Date:this.f.Date.value,
      RoomSrNo:this.f.Room.value,
      TableNo:this.f.TableNo.value,
      Customer:this.f.Customer.value,
      objlist:this.products.map(element => ({ 
        Sr:element.Id,
        ItemCode:element.Item.value,
        Qty:element.Qty,
        Rate:element.Rate,
        Amount:element.Amount,
        NetAmount :element.NetAmount
      })),
      Discount:this.f.Discount.value,
      TotalAmount:this.f.TotalAmount.value,
      MOP:this.f.MOP.value,
      Status:4,
      IpAddress:"::1",
      IsActive:this.f.InActive.value == false ? true:false,
      IsDelete:this.f.InActive.value == true ? true:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:Name
    }


    debugger;
    this.POS_Service.Save_POSMst(objModel).subscribe((resp: any) => {
      console.log(resp);
      let _result = resp.ResponseCode;
      
      if(_result== "2")
      {
        this.IsWait = false;
        this.POSForm.reset();
        this.POSForm.setErrors(null);
        this.products = [];
        //setTimeout(() => this.formGroupDirective.resetForm(), 200);
        this.IsNew_Entry = true;
        this.Show_Success_msg('Data Save Successfully!!!');
       
      }
      else if(_result== "4")
      {
        this.IsWait = false;
        this.POSForm.reset();
        this.POSForm.setErrors(null);
        this.products = [];
        this.IsNew_Entry = true;
        this.IsUpdate = false;
        this.Show_Success_msg('Data updated Successfully!!!');
      }
      else  if(_result== "-6")
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
      else  if(_result== "-8")
      {
        this.IsWait = false;
        this.Show_Error_msg('Please check trx Data!!!');
      }
      else 
      {
        this.IsWait = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      debugger;
      this.IsWait = false;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.Show_Error_msg('Something went wrong...Please try again later!!!');
    });
  };

  // List View
  selectTab(ev) {
    
      if(ev.tabId == "tb_lstVw"){
   
       this.tb_IsActive = false;
       
        if(this.IsNew_Entry){
         // fetch new entry
         this.tble_loading = true;
         this.get_ListView_List("0","10","");
  
         setTimeout(() => {
           if (this.datasource) {
               this.Prod_ListView = this.datasource.slice(0, (0 + 10));
               this.tble_loading = false;
               this.IsNew_Entry= false;
           }
         }, 1000);
        }
      }
   
  }

  get_ListView_List(first,rows,globalFilter): void {
      
  
      globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
       const objModel = { 
         StrQuery:`Sp_Get_POS 'Get_ListView',${first},${rows},'${globalFilter != "" ? (first + rows):rows}','','',1,1`
       };
       
      this.POS_Service.getData_JSON(objModel).subscribe((resp: any) => {
        //
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
  
          this.Prod_ListView = globalFilter != "" ? this.datasource :this.datasource.slice(first, (first + rows));
          //this.Prod_ListView = this.datasource.slice(first, (first + rows));
          this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
        }
      },   
      (error: AppResponse) => {
        
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
  };
  
  loadCustomers(event: LazyLoadEvent) {
      
      this.tble_loading = true;
  
      if(event.globalFilter!= null){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(this.datasource.length == 0){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(event.first > 500){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      
      if(this.datasource.length > 0)
      {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
      // setTimeout(() => {
      //     if (this.datasource) {
      //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      //         this.tble_loading = false;
      //     }
      // }, 2000);
  
  
  }

  // Edit
  async editPurchaseOrder(parm){
    const str = `Sp_Get_POS 'Get_Master_Details_BySrNo_ForEdit',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

      await this.POS_Service.getData_JSON(objModel).subscribe((resp: any) => {
        debugger;
      //console.log(resp);
      let _result = resp.success;
      if(_result== "1")
      {
        let objResp = resp.row;
        this.IsUpdate = true;
        debugger;
        //this.Change_XMaster_forEdit(objResp.XMaster);
        //setTimeout(() => {
          //var Amount = parseFloat(objResp[0].Amount).toFixed(2);

          

          this.get_Room_TableNo_List_Edit(objResp[0].RoomSrNo,objResp[0].TableNo);
          
          this.Get_Customer_ByRoom_TableNo_Edit(objResp[0].TableNo,objResp[0].TableNo);
          

          this.editPurchaseOrder_Trx(objResp[0].SrNo);

          setTimeout(() => {
          this.POSForm.controls['SrNo'].setValue(objResp[0].SrNo);
          this.POSForm.controls['Date'].setValue(objResp[0].Date);
          this.POSForm.controls['Room'].setValue(objResp[0].RoomSrNo);
          this.POSForm.controls['TableNo'].setValue(objResp[0].TableNo);
          this.POSForm.controls['Customer'].setValue(objResp[0].Customer);
          this.POSForm.controls['Discount'].setValue(objResp[0].Discount);
          this.POSForm.controls['TotalAmount'].setValue(objResp[0].TotalAmount);
          this.POSForm.controls['MOP'].setValue(objResp[0].MOP);
          this.POSForm.controls['InActive'].setValue(false);
          this.POSForm.controls['TotalAmount'].setValue(this.sum_Of_NetAmount());
          }, 1500);

        //}, 1000);

        // Trx
        

      }
      else
      {
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

    

    
  };

  async editPurchaseOrder_Trx(parm){

    const str = `Sp_Get_POS 'Get_Trx_Details_BySrNo_ForEdit',${parm},'','','','',1,1`;
    const objModel = { 
      StrQuery:str, 
    };

    await this.POS_Service.getData_JSON(objModel).subscribe((resp_Trx: any) => {
      let _result_trx = resp_Trx.success;
      if(_result_trx== "1")
      {
        let objResp_trx = []
        objResp_trx = resp_Trx.row;
        this.products = [];
        this.products = objResp_trx.map(item => ({ 
          Id: item.Sr,
          Sr: item.Sr,
          Item: {
            label: item.ItemName,
            value: item.ItemCode
          },
          Qty: item.Qty,
          Rate: item.Rate,
          Amount: item.Amount,
          NetAmount: item.NetAmount
        }));
        // for(let i = 0; i < objResp_trx.length; i++) {
        //     let item = objResp_trx[i];
            // const Item: Item = {
            //   label: item.ItemName,
            //   value: item.ItemCode
            // }
            // const objModel: POS_Trx_Models = {
            //   Sr: item.Sr,
            //   ItemCode: item.ItemCode,
            //   Qty: item.Qty,
            //   Rate: item.Rate,
            //   Amount:  item.Amount,
            //   NetAmount:item.NetAmount
            // }
            //this.products.push(objModel);
        //}
          //objModel.Id = this.createId();
          //this.product.image = 'product-placeholder.svg';
          
      }
      else
      {
        this.products = [];
        this.submitted = false;
        this.Show_Error_msg('Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
    this.tb_IsActive = true;

  };

  Get_Print(parm){

    const objModel: POS_Common_Models = {
      Type:"",
      Value:parm,
      Value1:"",
      Value2:"",
      Value3:"",
      Value4:"",
      UserId:"",
      Company:this.currentUserData.Company
    }
    //const objModel = { StrQuery:"Sp_Get_POS 'Get_Item_List','','','','','',1,1", };

    this.POS_Service.Get_POS_Print(objModel).subscribe((resp: any) => {
      debugger;
      //let _result = resp.success;
      //this.items = resp.row;
      //const pdfAsDataUri = `data:application/pdf;base64,${resp.RedirectUrl}`;

      //var pdfAsDataUri = "data:application/pdf;base64," + escape(response);


      //************************* */
      var object = `<embed src="data:application/pdf;base64,${resp.Response}" type="application/pdf"`;
      object += " frameborder=\"0\" width=\"100%\" height=\"557.48px\">";
      this.PrintString = this.sanitizer.bypassSecurityTrustHtml(object);

      //this.submitted = false;
      this.productDialog = true;
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });

  };

  // Reset Form
  ResetForm() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);

    this.POSForm.reset();
    this.POSForm.setErrors(null);

    //setTimeout(() => this.formGroupDirective.resetForm(), 200);
    this.product = {};
    this.products = [];
    this.IsUpdate = false;

    setTimeout(() => {
      this.POSForm.controls['Date'].setValue(this.dateService.today());
      this.POSForm.controls['Discount'].setValue('0.00');
      this.POSForm.controls['TotalAmount'].setValue('0.00');
    }, 500);   

    
  }


  

   // Trx
  
  openNew() {
    //this.product = {};
    this.submitted = false;
    this.productDialog = true;
  }

  deleteSelectedProducts() {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
          this.products = this.products.filter(val => !this.selectedProducts.includes(val));
          this.selectedProducts = null;
          this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
      }
    });
  }
  

  onDeleteConfirm(event): void {
    if (window.confirm('Are you sure you want to delete?')) {
      event.confirm.resolve();
    } else {
      event.confirm.reject();
    }
  }

  // search(event) {
  //   this.Vendor_List = [
  //     {XCode: '01', XName:'Risa Pearson'},
  //     {XCode: '02', XName:'Margaret D. Evans'},
  //     {XCode: '03', XName:'Bryan J. Luellen'},
  //     {XCode: '04', XName:'Kathryn S. Collier'}
  //   ]; 
  // }

  filterItems(event) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered : any[] = [];
    let query = event.query;

    for(let i = 0; i < this.items.length; i++) {
        let item = this.items[i];
        if (item.label.toLowerCase().indexOf(query.toLowerCase()) == 0) {
            filtered.push(item);
        }
    }
    
    this.filteredItems = filtered;
  }


  // Notification msg
  Show_Success_msg(msg):void{
    this.IsSucessMsg = msg;
    this.IsSucess = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsSucess = false;
      this.IsSucessMsg = "";
    }, 3000);
  };

  Show_Warning_msg(msg):void{
    this.IsWarningMsg = msg;
    this.IsWarning = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsWarning = false;
      this.IsWarningMsg = "";
    }, 3000);
  };

  Show_Error_msg(msg):void{
    this.IsDangerMsg = msg;
    this.IsDanger = true;
    setTimeout(()=>{                           //<<<---using ()=> syntax
      this.IsDanger = false;
      this.IsDangerMsg = "";
    }, 3000);
  };

  // // Trx
  // findIndexById(Id: string): number {
  //   let index = -1;
  //   for (let i = 0; i < this.products.length; i++) {
  //       if (this.products[i].Id === Id) {
  //           index = i;
  //           break;
  //       }
  //   }
  //   return index;
  // }

  createId(): string {
    let id = '';
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for ( var i = 0; i < 5; i++ ) {
        id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }
  hideDialog() {
    this.TrxForm.reset();
    this.TrxForm.setErrors(null);
    this.submitted_trx = false;
    this.productDialog = false;
    
  }

  // editProduct(product: Product) {
  //   
  //   this.TrxForm.controls['Id'].setValue(product.Id);
  //   this.TrxForm.controls['Sr'].setValue(product.Sr);
  //   this.TrxForm.controls['Item'].setValue(product.Item);
  //   this.TrxForm.controls['Qty'].setValue(product.Qty);    
  //   this.TrxForm.controls['Rate'].setValue(parseFloat(product.Rate.toString()).toFixed(2));
  //   this.TrxForm.controls['Amount'].setValue(parseFloat(product.Amount.toString()).toFixed(2));    
  //   this.productDialog = true;  
  // }

  // deleteProduct(product: Product) {
  //   
  //   this.confirmationService.confirm({
  //       message: 'Are you sure you want to delete ' + product.Item.label + '?',
  //       header: 'Confirm',
  //       icon: 'pi pi-exclamation-triangle',
  //       accept: () => {
  //           this.products = this.products.filter(val => val.Id !== product.Id);
  //           
  //           this.product = {};
  //           this.POSForm.controls['NetAmount'].setValue(this.sum_Of_NetAmount());
  //           //this.messageService.add({severity:'success', summary: 'Successful', detail: 'Product Deleted', life: 3000});
  //       }
  //   });
  // }

  Qty_onKeyUp(x) {

    this.fun_Cal_NetAmount();
  }
  
  fun_Cal_NetAmount(){
  
    this.POSForm.controls['TotalAmount'].setValue(this.sum_Of_NetAmount());
  
  }

  sum_Of_NetAmount() {
    let sum: number = 0;
    let discount: number = 0;
    discount = this.f.Discount.value;

    this.products.forEach(a => sum += parseFloat(a.NetAmount.toString()));
    sum = sum - discount;
    var sum1 = parseFloat(sum.toString()).toFixed(2);
    
    return sum1;
  }

}
