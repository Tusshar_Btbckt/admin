import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-sales',
  template:`
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})
export class SalesComponent {
}


// export class SalesComponent implements OnInit {
//   constructor() { }
//   ngOnInit(): void {
//   }
// }
