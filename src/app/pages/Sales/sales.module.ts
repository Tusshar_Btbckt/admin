import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbActionsModule,
  NbAlertModule,
  NbButtonModule,
  NbCardModule,
  NbCheckboxModule,
  NbDatepickerModule, NbIconModule,
  NbInputModule,
  NbRadioModule,
  NbSelectModule,
  NbToggleModule,
  NbUserModule,
  NbTabsetModule,
  NbBadgeModule
} from '@nebular/theme';

import { ThemeModule } from '../../@theme/theme.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SalesRoutingModule } from './sales-routing.module';
import { SalesComponent } from '../sales/sales.component';

// For Table
import { Ng2SmartTableModule } from 'ng2-smart-table';

//For Table
import {TableModule} from 'primeng/table';
import {DialogModule} from 'primeng/dialog';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ToolbarModule} from 'primeng/toolbar';
import {ButtonModule} from 'primeng/button';

import {AutoCompleteModule} from 'primeng/autocomplete';
import { OrderComponent } from './order/order.component';
import { POSComponent } from './pos/pos.component';


@NgModule({
  declarations: [
    SalesComponent,
    OrderComponent,
    POSComponent
  ],
  imports: [
    CommonModule,
    SalesRoutingModule,
    ThemeModule,
    NbInputModule,
    NbCardModule,
    NbAlertModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbCheckboxModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    NbToggleModule,
    Ng2SmartTableModule,
    TableModule,
    DialogModule,
    ConfirmDialogModule,
    AutoCompleteModule,
    NbTabsetModule,
    NbBadgeModule,
    ToolbarModule,
    ButtonModule
  ]
})
export class SalesModule { }
