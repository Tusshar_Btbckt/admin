export interface AddStock {
    SrNo:string;
    Date:string;
    ItemName:string;
    PackUnit:string;
    Qty:number;
    Kitchen:string;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface AS_Resp_Models {
    success:string;
    row:any[];
}

export interface AS_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface AS_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}