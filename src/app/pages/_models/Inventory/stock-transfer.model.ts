export interface StockTransfer {
    SrNo:string;
    Date:string;
    From_Kitchen:string;
    To_Kitchen:string;
    objlist:ST_Trx_Models[];
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface ST_Trx_Models {
    Sr:number;
    ItemCode:string;
    PackUnit:string;
    Qty:number;
}

export interface Product {
    Id: string;
    Sr: number;
    Item: any;
    Qty: number;
    PackQty: any;
  }
  
export interface Item {
    label: string;
    value: string;
}
  

export interface ST_Resp_Models {
    success:string;
    row:any[];
}

export interface ST_Dropdown_Models {
    label:string;
    value:string;
}

export interface ST_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}