export interface Customer {
    SrNo:string;
    FirstName:string;
    LastName:string;
    Mobile:number;
    Email:string;
    DOB:string;
    Age:number;
    Gender:string;
    MaritalStatus:string;
    SchoolName:string;
    CollageName:string;
    Address:string;
    Country:string;
    State:string;
    City:string;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}



export interface C_Resp_Models {
    success:string;
    row:any[];
}

export interface C_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface C_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}