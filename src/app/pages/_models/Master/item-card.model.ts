export interface ItemCard {
    SrNo: string;
    Name: string;
    Description: string;
    ItemType: string;
    Marks: string;
    Category: string;
    Ordering: number;
    Price: number;
    Base64String_Image1:string;
    Image1_Name:string;
    Recipe:string;
    PackUnit:string;
    RecipeQty:number;
    Cuisines:string;
    objlist:CF_Trx_Models[];
    IpAddress: string;
    IsActive: boolean;
    IsDelete: boolean;
    Company: number;
    CreatedBy: number;
    Command: string;
}

export interface ItemCard_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    XCode: string;
    XName: string;
    StrQuery: string;
    success: string;
    row: any[];
}

export interface ItemCard_Resp_Models {
    success: string;
    row: any[];
}

export interface ItemCard_Common_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    UserId: string;
    Company: string;
}

export interface CF_Trx_Models {
    Sr:number;
    ConversionUnit:string;
    ConversionFactor:number;
}

export interface ConversionTrx {
    Id: string;
    Sr: number;
    ConversionUnit: any;
    ConversionFactor: number;
}
  
export  interface ConversionUnit {
    label: string;
    value: string;
}