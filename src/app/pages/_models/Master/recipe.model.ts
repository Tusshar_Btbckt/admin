
export interface Recipe {
    SrNo:string;
    Date:string;
    RecipeName:string;
    Qty:number;
    PackQty:string;
    TotalAmount:number;
    objlist:Recipe_Trx_Models[];
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface Recipe_Trx_Models {
    Sr:number;
    ItemXCode:string;
    Qty:number;
    PackQty:string;
    Amount: number;
}

export interface Product {
    Id: string;
    Sr: number;
    Item: any;
    Qty: number;
    PackQty: any;
    Amount: number;
  }
  
export interface Item {
    label: string;
    value: string;
}
  

export interface Recipe_Resp_Models {
    success:string;
    row:any[];
}

export interface Recipe_Dropdown_Models {
    label:string;
    value:string;
}


export interface Recipe_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}