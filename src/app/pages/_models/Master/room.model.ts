export interface Room {
    SrNo:string;
    Date:string;
    RoomName:string;
    TableNo:number;
    RoomType:string;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}



export interface Room_Resp_Models {
    success:string;
    row:any[];
}

export interface Room_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface Room_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}
