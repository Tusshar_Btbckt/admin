export interface Vendor {
    SrNo:string;
    Name:string;
    Description:string;
    Mobile:number;
    Email:string;
    Address:string;
    Country:number;
    State:number;
    City:number;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}



export interface V_Resp_Models {
    success:string;
    row:any[];
}

export interface V_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface V_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}