export interface PO {
    SrNo:string;
    Date:string;
    Vendor:string;
    objlist:PO_Trx_Models[];
    Amount:number;
    Discount:number;
    NetAmount:number;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface PO_Resp_Models {
    success:string;
    row:any[];
}

export interface PORes_test_Models {
    value:string;
    Message:string;
}

export interface PO_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface PO_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}

export interface PO_Trx_Models {
    Sr:number;
    ItemCode:string;
    PackUnit:string;
    Qty:number;
    Rate:number;
    Amount:number;
    Discount:number;
    NetAmount:number;
}



