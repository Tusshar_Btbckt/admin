export interface POA {
    SrNo:string;
    Date:string;
    Vendor:string;
    PONo:string;
    objlist:POA_Trx_Models[];
    Amount:number;
    Discount:number;
    NetAmount:number;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface POA_Resp_Models {
    success:string;
    row:any[];
}

export interface POARes_test_Models {
    value:string;
    Message:string;
}

export interface POA_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface POA_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}

export interface POA_Trx_Models {
    Sr:number;
    ItemCode:string;
    PackUnit:string;
    Qty:number;
    Rate:number;
    Amount:number;
    Discount:number;
    NetAmount:number;
}



