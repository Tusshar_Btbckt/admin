export interface Report {
    SrNo:string;
    Sr:number;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface Report_Resp_Models {
    success:string;
    row:any[];
}

export interface Report_Res_test_Models {
    value:string;
    Message:string;
}

export interface Report_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface Report_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}


