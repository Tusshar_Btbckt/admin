export interface Order {
    SrNo:string;
    Date:string;
    RoomSrNo:string;
    TableNo:string;
    Customer:string;
    objlist:Order_Trx_Models[];
    NetAmount:number;
    Person:number;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface Order_Resp_Models {
    success:string;
    row:any[];
}

export interface OrderRes_test_Models {
    value:string;
    Message:string;
}

export interface Order_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface Order_Dropdown2_Models {
    label:string;
    value:string;
}

export interface Order_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}

export interface Order_Trx_Models {
    Sr:number;
    ItemCode:string;
    PackUnit:string;
    Qty:number;
    Rate:number;
    Amount:number;
    Instruction:string;
    Remark:string;
}



