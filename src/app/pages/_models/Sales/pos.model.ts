export interface POS {
    SrNo:string;
    Date:string;
    RoomSrNo:string;
    TableNo:string;
    Customer:string;
    Discount:number;
    TotalAmount:number;
    MOP:string;
    Status:number;
    objlist:POS_Trx_Models[];
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    CompanyId:number;
    CreatedBy:number;
    Command:string;
}

export interface POS_Resp_Models {
    success:string;
    row:any[];
}

export interface POSRes_test_Models {
    value:string;
    Message:string;
}

export interface POS_Dropdown_Models {
    XCode:string;
    XName:string;
}

export interface POS_Dropdown2_Models {
    label: string;
    value: string;
}

export interface POS_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}

export interface POS_Trx_Models {
    Sr:number;
    ItemCode:string;
    Qty:number;
    Rate:number;
    Amount:number;
    NetAmount:number;
}
