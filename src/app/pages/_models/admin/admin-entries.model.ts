export interface AdminEntries {
    XMaster:string;
    XCode:string;
    XName:string;
    XLink:string;
    Ordering:number;
    Description:string;
    Base64String_Image1:string;
    Image1_Name:string;
    Base64String_Image2:string;
    Image2_Name:string;
    IpAddress:string;
    IsActive:boolean;
    IsDelete:boolean;
    Company:number;
    CreatedBy:number;
    Command:string;
}

export interface AdminEntries_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    XCode:string;
    XName:string;
    StrQuery:string;
    success:string;
    row:any[];
}

export interface AdminEntries_Resp_Models {
    success:string;
    row:any[];
}

export interface AdminEntriesRes_test_Models {
    value:string;
    Message:string;
}

export interface AdminEntries_Common_Models {
    Type:string;
    Value:string;
    Value1:string;
    Value2:string;
    Value3:string;
    Value4:string;
    UserId:string;
    Company:string;
}


