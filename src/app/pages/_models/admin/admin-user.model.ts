export interface AdminUser {
    UserId: number;
    HotelId: number;
    UserFName: string;
    UserLName: string;
    UserName: string;
    Password: string;
    Mobile: string;
    Email: string;
    UserTypeId: number;
    IpAddress: string;
    IsActive: boolean;
    IsDelete: boolean;
    Company: number;
    CreatedBy: number;
    Command: string;
}

export interface AdminUser_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    XCode: string;
    XName: string;
    StrQuery: string;
    success: string;
    row: any[];
}

export interface AdminUser_Resp_Models {
    success: string;
    row: any[];
}

export interface AdminUser_Common_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    UserId: string;
    Company: string;
}