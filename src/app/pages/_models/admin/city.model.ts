export interface City {
    CityId: number;
    State: string;
    City: string;
    IpAddress: string;
    IsActive: boolean;
    IsDelete: boolean;
    Company: number;
    CreatedBy: number;
    Command: string;
}

export interface City_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    XCode: string;
    XName: string;
    StrQuery: string;
    success: string;
    row: any[];
}

export interface City_Resp_Models {
    success: string;
    row: any[];
}

export interface City_Common_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    UserId: string;
    Company: string;
}