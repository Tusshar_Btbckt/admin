export interface Country {
    CountryId: number;
    Code: string;
    Name: string;
    PhoneCode: string;
    Currency: string;
    Flag: string;
    IpAddress: string;
    IsActive: boolean;
    IsDelete: boolean;
    Company: number;
    CreatedBy: number;
    Command: string;
}

export interface Country_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    XCode: string;
    XName: string;
    StrQuery: string;
    success: string;
    row: any[];
}

export interface Country_Resp_Models {
    success: string;
    row: any[];
}

export interface Country_Common_Models {
    Type: string;
    Value: string;
    Value1: string;
    Value2: string;
    Value3: string;
    Value4: string;
    UserId: string;
    Company: string;
}