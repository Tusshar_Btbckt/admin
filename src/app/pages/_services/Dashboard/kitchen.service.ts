import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Kitchen,Kitchen_Resp_Models,KitchenRes_test_Models,Kitchen_Common_Models} from '../../_models/Dashboard/kitchen.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Origin':'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})
export class KitchenService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };

  public Update_Order_Dashboard(Order: Kitchen):Observable<any>{   
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Update_Order_Dashboard/`, 
    Order,httpOptions)
  };

  public Update_Order_Dashboard_Ready(Order: Kitchen):Observable<any>{   
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Update_Order_Dashboard_Ready/`, 
    Order,httpOptions)
  };

  // public get_ListView(admin_Body: any):Observable<any>{
  //   return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Get_Purchase_Order_List/`,admin_Body,httpOptions)
  // };

  public getData_JSON(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };
  
}
