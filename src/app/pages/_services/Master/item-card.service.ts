import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ItemCard } from '../../_models/Master/item-card.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': 'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})

export class ItemCardService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(itemcard_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, itemcard_Body, httpOptions)
  };

  public Save_ItemCard(ItemCard: ItemCard): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_ItemCard/`, ItemCard, httpOptions)
  };

  public get_ListView(itemcard_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, itemcard_Body, httpOptions)
  };

  public getData_JSON(itemcard_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, itemcard_Body, httpOptions)
  };

}