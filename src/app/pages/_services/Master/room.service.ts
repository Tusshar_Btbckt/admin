import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Room,Room_Resp_Models,Room_Common_Models} from '../../_models/Master/room.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Origin':'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };

  public Save_Room(Vendor: Room):Observable<any>{   
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_Room/`, 
    Vendor,httpOptions)
  };

  public getData_JSON(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };
}
