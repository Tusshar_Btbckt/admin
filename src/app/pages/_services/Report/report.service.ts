import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Report,Report_Resp_Models,Report_Res_test_Models,Report_Common_Models} from '../../_models/Report/report.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Origin':'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private httpClient: HttpClient) { }

  public getData_JSON(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };
}
