import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { POS,POS_Resp_Models,POSRes_test_Models,POS_Common_Models} from '../../_models/Sales/pos.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Origin':'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})
export class POSService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };

  public Save_POSMst(Order: POS):Observable<any>{   
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_POS/`, 
    Order,httpOptions)
  };

  // public get_ListView(admin_Body: any):Observable<any>{
  //   return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Get_Purchase_Order_List/`,admin_Body,httpOptions)
  // };

  public getData_JSON(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };

  public Get_POS_Print(admin_Body: POS_Common_Models):Observable<any>{
    //return this.httpClient.get<any>(`${environment.webapiUrl}/api/SvnElvn/Get_POS_Print/`,admin_Body,httpOptions)
    let params = new HttpParams();
    params = params.append('SrNo', admin_Body.Value);
    params = params.append('CompanyId', admin_Body.Company);
    return this.httpClient.get(`${environment.webapiUrl}/api/SvnElvn/Get_POS_Print/`, { params: params,responseType: 'json'})
    
  };
  
}
