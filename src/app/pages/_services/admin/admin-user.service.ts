import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AdminUser } from '../../_models/admin/admin-user.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': 'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})

export class AdminUserService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(adminuser_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, adminuser_Body, httpOptions)
  };

  public Save_AdminUser(AdminUser: AdminUser): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_AdminUser/`, AdminUser, httpOptions)
  };

  public get_ListView(adminuser_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, adminuser_Body, httpOptions)
  };

  public getData_JSON(adminuser_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, adminuser_Body, httpOptions)
  };

}