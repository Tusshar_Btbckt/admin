import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { City } from '../../_models/admin/city.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': 'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})

export class CityService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(city_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, city_Body, httpOptions)
  };

  public Save_City(City: City): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_City/`, City, httpOptions)
  };

  public get_ListView(city_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, city_Body, httpOptions)
  };

  public getData_JSON(city_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, city_Body, httpOptions)
  };

}