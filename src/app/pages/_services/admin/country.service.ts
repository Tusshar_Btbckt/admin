import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Country } from '../../_models/admin/country.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': 'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})

export class CountryService {

  constructor(private httpClient: HttpClient) { }

  public Save_Country(Country: Country): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_Country/`, Country, httpOptions)
  };

  public get_ListView(country_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, country_Body, httpOptions)
  };

  public getData_JSON(country_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, country_Body, httpOptions)
  };

}