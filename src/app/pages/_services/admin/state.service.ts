import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { State } from '../../_models/admin/state.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8',
    'Access-Control-Allow-Origin': 'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})

export class StateService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(state_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, state_Body, httpOptions)
  };

  public Save_State(State: State): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_State/`, State, httpOptions)
  };

  public get_ListView(state_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, state_Body, httpOptions)
  };

  public getData_JSON(state_Body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`, state_Body, httpOptions)
  };

}