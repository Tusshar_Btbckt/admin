import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { POA,POA_Resp_Models,POARes_test_Models,POA_Common_Models} from '../../_models/Purchase/poa.model';
import { environment } from '../../../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json; charset=utf-8',
    'Access-Control-Allow-Origin':'https://icorrect.in'
  })
};

@Injectable({
  providedIn: 'root'
})
export class POAService {

  constructor(private httpClient: HttpClient) { }

  public getDropdownList(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };

  public Save_PO_Acceptance(PurcahseOrder: POA):Observable<any>{   
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Save_PO_Acceptance/`, 
    PurcahseOrder,httpOptions)
  };

  public get_ListView(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/Get_Purchase_Order_List/`,admin_Body,httpOptions)
  };

  public getData_JSON(admin_Body: any):Observable<any>{
    return this.httpClient.post<any>(`${environment.webapiUrl}/api/SvnElvn/getData_JSON/`,admin_Body,httpOptions)
  };
}
