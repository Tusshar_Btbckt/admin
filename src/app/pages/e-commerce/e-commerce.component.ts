import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { PermissionCodes } from "../Authentication/permissionConstant";
import { AuthenticationService } from "../Authentication/services/authentication.service";

@Component({
  selector: "ngx-ecommerce",
  templateUrl: "./e-commerce.component.html",
})
export class ECommerceComponent implements OnInit {
  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) {}
  ngOnInit(): void {
    let dashBoardPermission = this.authenticationService.getPermission(
      PermissionCodes.Dashboard
    );
    if (!dashBoardPermission) this.router.navigate(["/pages/no-access"]);
  }
}
