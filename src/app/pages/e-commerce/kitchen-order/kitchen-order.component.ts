import { Component, OnInit } from '@angular/core';
import { KitchenService } from '../../../pages/_services/Dashboard/kitchen.service';
import { AppResponse} from '../../../pages/_models/app-response.model';

import { LazyLoadEvent } from 'primeng/api';
import { Kitchen } from '../../_models/Dashboard/kitchen.model';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

// Toster
import {
  NbComponentStatus,
  NbGlobalLogicalPosition,
  NbGlobalPhysicalPosition,
  NbGlobalPosition,
  NbToastrService,
  NbToastrConfig,
} from '@nebular/theme';

@Component({
  selector: 'ngx-kitchen-order',
  templateUrl: './kitchen-order.component.html',
  styleUrls: ['./kitchen-order.component.scss'],
  providers: [MessageService,ConfirmationService]
})
export class KitchenOrderComponent implements OnInit {

  public Order_List: any[];
  error: string;

  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  //For Accepted Order
  AcceptedOrder_List: any[];
  AcceptedOrder_TotalRecords: number;

  UserTypeId: number;

  interval;
  interval2;

  constructor(
    private Kitchen_Service: KitchenService,
    private authenticationService:AuthenticationService,
    private toastrService: NbToastrService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  currentUserData:any = {}

  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();
    this.UserTypeId = this.currentUserData.UserTypeId;
    debugger;
    this.get_ListView_List();
    this.get_AcceptedOrder_List();
    // this.interval = setInterval(() => {
    //   this.get_ListView_List();
    // }, 3000);

     //For List-View
     this.totalRecords = 0;
     this.tble_loading = true;
     this.datasource = [];
     //For Accepted Order
     this.AcceptedOrder_List = [];
     this.AcceptedOrder_TotalRecords =  0;
     
  }

  stop_Interval() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  Resume_Interval() {
    this.interval = setInterval(() => {
      this.get_ListView_List();
    }, 3000);
  }

  //For Accepted Order
  stop_Interval2() {
    if (this.interval2) {
      clearInterval(this.interval2);
    }
  }

  Resume_Interval2() {
    this.interval2 = setInterval(() => {
      this.get_AcceptedOrder_List();
    }, 3000);
  }

  get_Order_List(): void {

    const objModel = { StrQuery:"Sp_Get_Dashboard 'Get_Kitchen_Order_List','','','','','',1,1", };

    this.Kitchen_Service.getDropdownList(objModel).subscribe((resp: any) => {
      let _result = resp.success;
      if(_result== "1")
      {
        this.Order_List = resp.row;
      }
      else
      {
        this.Order_List = [];
      }
    },   
    (error: AppResponse) => {
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
  };

  // List View
  
  get_ListView_List(): void {
      debugger;
  
      const objModel = { 
        StrQuery:`Sp_Get_Dashboard 'Get_Kitchen_Order_List','','','','','',1,1`
      };

      this.Kitchen_Service.getData_JSON(objModel).subscribe((resp: any) => {
        debugger;
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          debugger;
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
          this.Prod_ListView = this.datasource;
  
          //this.Prod_ListView = this.datasource.slice(first, (first + rows));
          //this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
          this.Prod_ListView = this.datasource;
        }
      },   
      (error: AppResponse) => {
        debugger;
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
  };

  get_AcceptedOrder_List(): void {
    debugger;

    const objModel = { 
      StrQuery:`Sp_Get_Dashboard 'Get_Kitchen_Accepted_Order_List','','','','','',1,1`
    };

    this.Kitchen_Service.getData_JSON(objModel).subscribe((resp: any) => {
      debugger;
      //console.log(resp.row);
      let _result = resp.success;
      if(_result== "1")
      {
        debugger;
        this.AcceptedOrder_List = resp.row;
        this.AcceptedOrder_TotalRecords =  resp.row[0].Row_Count;

      }
      else
      {
        this.AcceptedOrder_List = [];
        this.AcceptedOrder_TotalRecords =  0;
      }
    },   
    (error: AppResponse) => {
      debugger;
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             this.error = error.message;
    });
};
  
  

  
  Update_Order_Dashboard(SrNo,Sr,Rnk){

    debugger;

    //this.stop_Interval();

    const objModel: Kitchen = {
      SrNo:SrNo,
      Sr:Sr,
      IpAddress:"::1",
      IsActive:true,
      IsDelete:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:"Update"
    }
    debugger;
    this.Kitchen_Service.Update_Order_Dashboard(objModel).subscribe((resp: any) => {
      //console.log(resp);
      let _result = resp.ResponseCode;
     
      //this.Resume_Interval();
      if(_result== "2")
      {
        this.deleteSelectedProducts(Rnk);
        this.get_AcceptedOrder_List();
        this.showToast('success','Successful','Order Accepted');
      }
      else if(_result== "-4")
      {
        this.deleteSelectedProducts(Rnk);
        this.showToast('warning','Warning','Already order is accepted');
      }
      else  if(_result== "-2")
      {
        this.showToast('danger','Error','Something went wrong...Please try again later!!!');
      }
      else 
      {
        this.showToast('danger','Error','Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      //this.Resume_Interval();
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.showToast('danger','Error','Something went wrong...Please try again later!!!');
    });
  };

  Update_Order_ForReady_Dashboard(SrNo,Sr,Rnk){

    debugger;

    //this.stop_Interval();

    const objModel: Kitchen = {
      SrNo:SrNo,
      Sr:Sr,
      IpAddress:"::1",
      IsActive:true,
      IsDelete:false,
      CompanyId:this.currentUserData.Company,
      CreatedBy:this.currentUserData.UserId,
      Command:"Update"
    }
    debugger;
    this.Kitchen_Service.Update_Order_Dashboard_Ready(objModel).subscribe((resp: any) => {
      //console.log(resp);
      let _result = resp.ResponseCode;
     
      //this.Resume_Interval();
      if(_result== "2")
      {
        this.deleteSelectedProducts_FromAccepted_List(Rnk);
        this.showToast('success','Successful','Order Ready');
      }
      else if(_result== "-4")
      {
        this.deleteSelectedProducts_FromAccepted_List(Rnk);
        this.showToast('warning','Warning','Already order is accepted');
      }
      else  if(_result== "-2")
      {
        this.showToast('danger','Error','Something went wrong...Please try again later!!!');
      }
      else 
      {
        this.showToast('danger','Error','Something went wrong...Please try again later!!!');
      }
    },   
    (error: AppResponse) => {
      //this.Resume_Interval();
       if(error.status === 400)
             this.error = "Error 400...Bad Request!!!";
        else
             //this.error = error.message;
             this.showToast('danger','Error','Something went wrong...Please try again later!!!');
    });
  };

  private showToast(type: string, title: string, body: string) {
    const config = {
      status: type,
      destroyByClick: true,
      duration: 3000,
      hasIcon: true,
      position: NbGlobalPhysicalPosition.TOP_RIGHT,
      preventDuplicates: false,
    };
    const titleContent = title ? `${title}` : '';
    
    this.toastrService.show(
      body,
      `${titleContent}`,
      config);
  }

  deleteSelectedProducts(Rnk) {
    this.Prod_ListView = this.Prod_ListView.filter(val => val.Rnk != Rnk);

    // this.confirmationService.confirm({
    //     message: 'Are you sure you want to delete the selected products?',
    //     header: 'Confirm',
    //     icon: 'pi pi-exclamation-triangle',
    //     accept: () => {
    //         this.Prod_ListView = this.Prod_ListView.filter(val => val.Rnk == Rnk);
    //         //this.products = this.products.filter(val => !this.selectedProducts.includes(val));
    //         //this.selectedProducts = null;
    //         this.messageService.add({severity:'success', summary: 'Successful', detail: 'Products Deleted', life: 3000});
    //     }
    // });
  }

  deleteSelectedProducts_FromAccepted_List(Rnk) {
    this.AcceptedOrder_List = this.AcceptedOrder_List.filter(val => val.Rnk != Rnk);
    
  }


}
