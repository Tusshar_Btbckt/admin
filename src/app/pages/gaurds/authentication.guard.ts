import { templateJitUrl } from "@angular/compiler";
import { Injectable, Inject, Injector } from "@angular/core";
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
} from "@angular/router";
@Injectable({
  providedIn: "root",
})
export class AuthGaurd implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    const currentUser = localStorage.getItem("_cred_dt");

    if (!currentUser) {
      localStorage.clear();
      this.router.navigate(["/"]);
      return false;
    }

    return true;
  }
}
