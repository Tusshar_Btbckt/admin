import { NbMenuItem } from '@nebular/theme';
import { PermissionCodes } from './Authentication/permissionConstant';

export const MENU_ITEMS: any[] = [
  {
    title: 'E-commerce',
    icon: 'shopping-cart-outline',
    link: '/pages/dashboard',
    home: true,
    key:PermissionCodes.Dashboard
  },
  {
    title: 'IoT Dashboard',
    icon: 'home-outline',
    link: '/pages/iot-dashboard',
    key:PermissionCodes.Dashboard
  },
  {
    title: 'FEATURES',
    group: true,
    key:""
  },
  {
    title: 'Admin',
    icon: 'people-outline',
    key:PermissionCodes.Admin,
    children: [
      {
        title: 'Admin Entries',
        key:PermissionCodes.AdmnEntrs,
        link: '/pages/admin/admin-entries',
      },
      {
        title: 'Admin User',
        key:PermissionCodes.AdmnUsr,
        link: '/pages/admin/admin-user',
      },
      {
        title: 'Country',
        key:PermissionCodes.Cntry,
        link: '/pages/admin/country',
      },
      {
        title: 'State',
        key:PermissionCodes.Stt,
        link: '/pages/admin/state',
      },
      {
        title: 'City',
        key:PermissionCodes.ct,
        link: '/pages/admin/city',
      },
    ],
  },
  {
    title: 'Master',
    icon: 'file-text-outline',
    key:PermissionCodes.Master,
    children: [
      {
        title: 'Item-Card',
        key:PermissionCodes.ItmCrd,
        link: '/pages/master/item-card',
      },
      {
        title: 'Recipe',
        key:PermissionCodes.Rcp,
        link: '/pages/master/recipe',
      },
      {
        title: 'Vendor',
        key:PermissionCodes.Vndr,
        link: '/pages/master/vendor',
      },
      {
        title: 'Room',
        key:PermissionCodes.Rm,
        link: '/pages/master/room',
      },
      {
        title: 'Change-Password',
        key:PermissionCodes.CngPswrd,
        link: '/pages/master/change-password',
      },
      {
        title: 'Customer',
        key:PermissionCodes.Cstmr,
        link: '/pages/master/customer',
      },
      {
        title: 'Hotel',
        key:PermissionCodes.Htl,
        link: '/pages/master/hotel',
      }
    ],
  },
  {
    title: 'Purchase',
    key:PermissionCodes.Purchase,
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'Purchase Order',
        key:PermissionCodes.PrchsOrdr,
        link: '/pages/purchase/purchase-order',
      },
      {
        title: 'Purchase Order Acceptance',
        key:PermissionCodes.PrchsOrdrAcptnc,
        link: '/pages/purchase/purchase-order-acceptance',
      },
      {
        title: 'Purchase-Order Return',
        key:PermissionCodes.PrchsOrdrRtrn,
        link: '/pages/purchase/purchase-order-return',
      }
    ],
  },
  {
    title: 'Inventory',
    key:PermissionCodes.Inventory,
    icon: 'monitor-outline',
    children: [
      {
        title: 'Add Stock',
        key:PermissionCodes.AdStck,
        link: '/pages/inventory/Add-Stock',
      },
      {
        title: 'Stock Transfer',
        key:PermissionCodes.Stt,
        link: '/pages/inventory/Stock-Transfer',
      }
    ],
  },
  {
    title: 'Sales',
    key:PermissionCodes.Sales,
    icon: 'shopping-cart-outline',
    children: [
      {
        title: 'Order',
        key:PermissionCodes.Ordr,
        link: '/pages/sales/Order',
      },
      {
        title: 'POS',
        key:PermissionCodes.POS,
        link: '/pages/sales/POS',
      }
    ],
  },
  {
    title: 'Report',
    key:PermissionCodes.Report,
    icon: 'file-outline',
    children: [
      {
        title: 'Stock Report',
        key:PermissionCodes.rptStck,
        link: '/pages/Report/Stock-Report',
      }
    ],
  },
  {
    title: 'HRM',
    key:PermissionCodes.HRM,
    icon: 'person-add-outline',
    children: [
      {
        title: 'Calender',
        key:PermissionCodes.HRM,
        link: '/pages/hrm/calender',
      },
      {
        title: 'Calender-Open-Close',
        key:"",
        link: '/pages/hrm/calender-open-close',
      },
      {
        title: 'Payroll-Calender',
        key:"",
        link: '/pages/hrm/payroll-calender',
      },
      {
        title: 'Employee-Profile',
        key:"",
        link: '/pages/hrm/employee-profile',
      },
      {
        title: 'Daily-Attendance',
        key:"",
        link: '/pages/hrm/daily-attendance',
      },
      {
        title: 'Monthly-Attendance',
        key:"",
        link: '/pages/hrm/monthly-attendance',
      },
      {
        title: 'Salary',
        key:"",
        link: '/pages/hrm/salary',
      },
      {
        title: 'Salary-Calculation',
        key:"",
        link: '/pages/hrm/salary-calculation',
      }
    ],
  },
  
  // {
  //   title: 'Layout',
  //   icon: 'layout-outline',
  //   children: [
  //     {
  //       title: 'Stepper',
  //       link: '/pages/layout/stepper',
  //     },
  //     {
  //       title: 'List',
  //       link: '/pages/layout/list',
  //     },
  //     {
  //       title: 'Infinite List',
  //       link: '/pages/layout/infinite-list',
  //     },
  //     {
  //       title: 'Accordion',
  //       link: '/pages/layout/accordion',
  //     },
  //     {
  //       title: 'Tabs',
  //       pathMatch: 'prefix',
  //       link: '/pages/layout/tabs',
  //     },
  //   ],
  // },
  // {
  //   title: 'Forms',
  //   icon: 'edit-2-outline',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/pages/forms/inputs',
  //     },
  //     {
  //       title: 'Form Layouts',
  //       link: '/pages/forms/layouts',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/pages/forms/buttons',
  //     },
  //     {
  //       title: 'Datepicker',
  //       link: '/pages/forms/datepicker',
  //     },
  //   ],
  // },
  // {
  //   title: 'UI Features',
  //   icon: 'keypad-outline',
  //   link: '/pages/ui-features',
  //   children: [
  //     {
  //       title: 'Grid',
  //       link: '/pages/ui-features/grid',
  //     },
  //     {
  //       title: 'Icons',
  //       link: '/pages/ui-features/icons',
  //     },
  //     {
  //       title: 'Typography',
  //       link: '/pages/ui-features/typography',
  //     },
  //     {
  //       title: 'Animated Searches',
  //       link: '/pages/ui-features/search-fields',
  //     },
  //   ],
  // },
  // {
  //   title: 'Modal & Overlays',
  //   icon: 'browser-outline',
  //   children: [
  //     {
  //       title: 'Dialog',
  //       link: '/pages/modal-overlays/dialog',
  //     },
  //     {
  //       title: 'Window',
  //       link: '/pages/modal-overlays/window',
  //     },
  //     {
  //       title: 'Popover',
  //       link: '/pages/modal-overlays/popover',
  //     },
  //     {
  //       title: 'Toastr',
  //       link: '/pages/modal-overlays/toastr',
  //     },
  //     {
  //       title: 'Tooltip',
  //       link: '/pages/modal-overlays/tooltip',
  //     },
  //   ],
  // },
  // {
  //   title: 'Extra Components',
  //   icon: 'message-circle-outline',
  //   children: [
  //     {
  //       title: 'Calendar',
  //       link: '/pages/extra-components/calendar',
  //     },
  //     {
  //       title: 'Progress Bar',
  //       link: '/pages/extra-components/progress-bar',
  //     },
  //     {
  //       title: 'Spinner',
  //       link: '/pages/extra-components/spinner',
  //     },
  //     {
  //       title: 'Alert',
  //       link: '/pages/extra-components/alert',
  //     },
  //     {
  //       title: 'Calendar Kit',
  //       link: '/pages/extra-components/calendar-kit',
  //     },
  //     {
  //       title: 'Chat',
  //       link: '/pages/extra-components/chat',
  //     },
  //   ],
  // },
  // {
  //   title: 'Maps',
  //   icon: 'map-outline',
  //   children: [
  //     {
  //       title: 'Google Maps',
  //       link: '/pages/maps/gmaps',
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       link: '/pages/maps/leaflet',
  //     },
  //     {
  //       title: 'Bubble Maps',
  //       link: '/pages/maps/bubble',
  //     },
  //     {
  //       title: 'Search Maps',
  //       link: '/pages/maps/searchmap',
  //     },
  //   ],
  // },
  // {
  //   title: 'Charts',
  //   icon: 'pie-chart-outline',
  //   children: [
  //     {
  //       title: 'Echarts',
  //       link: '/pages/charts/echarts',
  //     },
  //     {
  //       title: 'Charts.js',
  //       link: '/pages/charts/chartjs',
  //     },
  //     {
  //       title: 'D3',
  //       link: '/pages/charts/d3',
  //     },
  //   ],
  // },
  // {
  //   title: 'Editors',
  //   icon: 'text-outline',
  //   children: [
  //     {
  //       title: 'TinyMCE',
  //       link: '/pages/editors/tinymce',
  //     },
  //     {
  //       title: 'CKEditor',
  //       link: '/pages/editors/ckeditor',
  //     },
  //   ],
  // },
  // {
  //   title: 'Tables & Data',
  //   icon: 'grid-outline',
  //   children: [
  //     {
  //       title: 'Smart Table',
  //       link: '/pages/tables/smart-table',
  //     },
  //     {
  //       title: 'Tree Grid',
  //       link: '/pages/tables/tree-grid',
  //     },
  //   ],
  // },
  // {
  //   title: 'Miscellaneous',
  //   icon: 'shuffle-2-outline',
  //   children: [
  //     {
  //       title: '404',
  //       link: '/pages/miscellaneous/404',
  //     },
  //   ],
  // },
  {
    title: 'Auth',
    key:"",
    icon: 'lock-outline',
    children: [
      {
        title: 'Login',
        key:"",
        link: '/auth/login',
      },
      {
        title: 'Register',
        key:"",
        link: '/auth/register',
      },
      {
        title: 'Request Password',
        key:"",
        link: '/auth/request-password',
      },
      {
        title: 'Reset Password',
        key:"",
        link: '/auth/reset-password',
      },
    ],
  },
];
