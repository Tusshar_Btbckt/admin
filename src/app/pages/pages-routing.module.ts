import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { AuthGaurd } from './gaurds/authentication.guard';
import { NoAccessComponent } from './Authentication/components/no-access/no-access.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
      canActivate: [AuthGaurd]
    },
    {
      path: 'iot-dashboard',
      component: DashboardComponent,
      canActivate: [AuthGaurd]
    },
    {
      path: 'admin',
      loadChildren: () => import('./Admin/admin.module')
        .then(m => m.AdminModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'master',
      loadChildren: () => import('./Master/master.module')
        .then(m => m.MasterModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'purchase',
      loadChildren: () => import('./Purchase/purchase.module')
        .then(m => m.PurchaseModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'inventory',
      loadChildren: () => import('./Inventory/inventory.module')
        .then(m => m.InventoryModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'sales',
      loadChildren: () => import('./Sales/sales.module')
        .then(m => m.SalesModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'Report',
      loadChildren: () => import('./report/report.module')
        .then(m => m.ReportModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'hrm',
      loadChildren: () => import('./HRM/hrm.module')
        .then(m => m.HRMModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'layout',
      loadChildren: () => import('./layout/layout.module')
        .then(m => m.LayoutModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'forms',
      loadChildren: () => import('./forms/forms.module')
        .then(m => m.FormsModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'ui-features',
      loadChildren: () => import('./ui-features/ui-features.module')
        .then(m => m.UiFeaturesModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'modal-overlays',
      loadChildren: () => import('./modal-overlays/modal-overlays.module')
        .then(m => m.ModalOverlaysModule),
    },
    {
      path: 'extra-components',
      loadChildren: () => import('./extra-components/extra-components.module')
        .then(m => m.ExtraComponentsModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'maps',
      loadChildren: () => import('./maps/maps.module')
        .then(m => m.MapsModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'charts',
      loadChildren: () => import('./charts/charts.module')
        .then(m => m.ChartsModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'editors',
      loadChildren: () => import('./editors/editors.module')
        .then(m => m.EditorsModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'tables',
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
        canActivate: [AuthGaurd]
    },
    {
      path: 'miscellaneous',
      loadChildren: () => import('./miscellaneous/miscellaneous.module')
        .then(m => m.MiscellaneousModule),
        canActivate: [AuthGaurd]
    },
    {
      path:"no-access",
      component:NoAccessComponent,
      canActivate: [AuthGaurd]
    },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
