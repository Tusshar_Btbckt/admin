import { Component } from "@angular/core";
import { NbMenuItem } from "@nebular/theme";
import { AuthenticationService } from "./Authentication/services/authentication.service";

import { MENU_ITEMS } from "./pages-menu";

@Component({
  selector: "ngx-pages",
  styleUrls: ["pages.component.scss"],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent {
  FinalMenu: NbMenuItem[] = [];
  constructor(private authenticationService: AuthenticationService) {
    MENU_ITEMS.forEach((element) => {
      let permission = this.authenticationService.getPermission(element.key);
      if (permission) {
        var child = [];
        if (element.children && element.children.length > 0) {
          element.children.forEach((ele) => {
            let childPermission = this.authenticationService.getPermission(
              ele.key
            );
            if (childPermission) {
              child.push(ele);
            }
          });
        }
        if (child.length > 0) element.children = child;
        this.FinalMenu.push(element);
      }
    });
    console.log(this.FinalMenu);
  }

  menu = this.FinalMenu;
}
