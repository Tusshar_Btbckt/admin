import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ngx-report',
  template: `
  <router-outlet></router-outlet>
  `,
  styles: [
  ]
})

export class ReportComponent {
}
