import { Component, OnInit } from '@angular/core';

import { ReportService } from '../../../pages/_services/Report/report.service';
import { AppResponse} from '../../../pages/_models/app-response.model';

import { LazyLoadEvent } from 'primeng/api';
import { Kitchen } from '../../_models/Dashboard/kitchen.model';
import { AuthenticationService } from '../../Authentication/services/authentication.service';

import { NbDialogService } from '@nebular/theme';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'ngx-stock-report',
  templateUrl: './stock-report.component.html',
  styleUrls: ['./stock-report.component.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService,ConfirmationService]
})
export class StockReportComponent implements OnInit {

  error: string;
  tb_IsActive: boolean = true;
  
  IsSucess= false;
  IsWarning= false;
  IsDanger= false;
  IsSucessMsg: string;  
  IsWarningMsg: string;  
  IsDangerMsg: string; 
  
  //For List-View
  IsNew_Entry = false;
  datasource: any[];
  Prod_ListView: any[];
  totalRecords: number;
  cols: any[];
  tble_loading: boolean;

  constructor(
    private Report_Service: ReportService,
    private authenticationService:AuthenticationService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  currentUserData:any = {}


  ngOnInit(): void {
    this.currentUserData =  this.authenticationService.getUserData();

    //For List-View
    this.Prod_ListView = [];
    this.totalRecords = 0;
    this.tble_loading = true;
    this.datasource = [];
  }

  // List View
  // selectTab(ev) {
  //   debugger;
  //     if(ev.tabId == "tb_lstVw"){
   
  //      this.tb_IsActive = false;
       
  //       if(this.IsNew_Entry){
  //        // fetch new entry
  //        this.tble_loading = true;
  //        this.get_ListView_List("0","10","");
  
  //        setTimeout(() => {
  //          if (this.datasource) {
  //              this.Prod_ListView = this.datasource.slice(0, (0 + 10));
  //              this.tble_loading = false;
  //              this.IsNew_Entry= false;
  //          }
  //        }, 1000);
  //       }
  //     }
   
  //    }
  
     get_ListView_List(first,rows,globalFilter): void {
      debugger;
  
      globalFilter = globalFilter != "" && globalFilter != null ? globalFilter : "";
       const objModel = { 
         StrQuery:`Sp_StockReport 'Get_ListView',${first},${rows},'${globalFilter}','','',1,1`
       };
       
      this.Report_Service.getData_JSON(objModel).subscribe((resp: any) => {
        //debugger;
        //console.log(resp.row);
        let _result = resp.success;
        if(_result== "1")
        {
          debugger;
          this.datasource = resp.row;
          //For List-View
          this.totalRecords =  resp.row[0].Row_Count;
  
          this.Prod_ListView = this.datasource.slice(first, (first + rows));
          this.tble_loading = false;
  
        }
        else
        {
          this.datasource = [];
          this.totalRecords =  0;
        }
      },   
      (error: AppResponse) => {
        debugger;
         if(error.status === 400)
               this.error = "Error 400...Bad Request!!!";
          else
               this.error = error.message;
      });
    };
  
    loadCustomers(event: LazyLoadEvent) {
      debugger;
      this.tble_loading = true;
  
      if(event.globalFilter!= null){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(this.datasource.length == 0){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      else if(event.first > 500){
        this.get_ListView_List(event.first,event.rows,event.globalFilter);
      }
      
      if(this.datasource.length > 0)
      {
        this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
        this.tble_loading = false;
      }
      // setTimeout(() => {
      //     if (this.datasource) {
      //         this.Prod_ListView = this.datasource.slice(event.first, (event.first + event.rows));
      //         this.tble_loading = false;
      //     }
      // }, 2000);
  
  
    }


}
